﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Models
{
	public class Field : IField
	{
		public Field(IDeclaredType declaredType, string name)
		{
			DeclaredType = declaredType;
			Name = name;
			Readonly = false;
		}

		public Field(ITypeModel type, string name)
		{
			DeclaredType = new DeclaredType(type);
			Name = name;
			Readonly = false;
		}

		public IDeclaredType DeclaredType { get; set; }
		public string Name { get; set; }
		public bool Readonly { get; set; }
		public string Signature
		{
			get { return Name; }
		}
	}
}