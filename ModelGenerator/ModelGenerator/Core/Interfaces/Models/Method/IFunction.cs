﻿using System.Collections.Generic;

namespace ModelGenerator.Core.Interfaces.Models.Method
{
	public interface IFunction
	{
		IList<IMethodArgument> Inputs { get; set; }
		IMethodOutput Output { get; set; }
		IMethodImplementation Implementation { get; set; }
	}
}