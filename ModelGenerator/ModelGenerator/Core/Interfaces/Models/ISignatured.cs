﻿namespace ModelGenerator.Core.Interfaces.Models
{
	public interface ISignatured
	{
		string Signature { get;}
	}
}