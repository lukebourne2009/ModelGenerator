﻿using System;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models.Method;
using IDeclaredTypeMapper
	= Mapper.Core.IMapper<ModelGenerator.Core.Interfaces.DesignModels.IDeclaredDesignModel, ModelGenerator.Core.Interfaces.Models.IDeclaredType, ModelGenerator.Core.Interfaces.Scopes.IScope>;

namespace ModelGenerator.Common.Mappers
{
	public class MethodArgumentMapper : BaseMapper<IMethodArgumentDesignModel, IMethodArgument, IScope>
    {
		private readonly IDeclaredTypeMapper _declaredTypeMapper;

	    public MethodArgumentMapper(IDeclaredTypeMapper declaredTypeMapper)
	    {
		    _declaredTypeMapper = declaredTypeMapper;
	    }

		public override IMethodArgument Map(IMethodArgumentDesignModel source, IScope model)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (model  == null) throw new ArgumentNullException("model");

			return new MethodArgument(_declaredTypeMapper.Map(source, model), source.Name);
        }
    }
}