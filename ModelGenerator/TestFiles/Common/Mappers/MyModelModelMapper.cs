public class MyModelModelMapper : IModelMapper<MyModelEntity, MyModelModel, MyModelViewModel>
{
	public MyModelEntity MapToEntity(MyModelModel source)
	{
		return new MyModelEntity()
		{
			MyProperty = source.MyProperty
		};
	}
	public MyModelEntity MapToEntity(MyModelViewModel source)
	{
		var myModelModel = MapToModel(source);
		var myModelEntity = MapToEntity(myModelModel);
		return myModelEntity;
	}
	public MyModelModel MapToModel(MyModelEntity source)
	{
		return new MyModelModel()
		{
			MyProperty = source.MyProperty
		};
	}
	public MyModelModel MapToModel(MyModelViewModel source)
	{
		return new MyModelModel()
		{
			MyProperty = source.MyProperty
		};
	}
	public MyModelViewModel MapToViewModel(MyModelEntity source)
	{
		var myModelModel = MapToModel(source);
		var myModelViewModel = MapToViewModel(myModelModel);
		return myModelViewModel;
	}
	public MyModelViewModel MapToViewModel(MyModelModel source)
	{
		return new MyModelViewModel()
		{
			MyProperty = source.MyProperty
		};
	}
	
}
