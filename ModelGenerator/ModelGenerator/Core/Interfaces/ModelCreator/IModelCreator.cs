﻿using System;
using ModelGenerator.Core.Interfaces.Settings;

namespace ModelGenerator.Core.Interfaces.ModelCreator
{
    public interface IModelCreator
    {
		string ModelName { get; }
        void Build(Uri outPathUri);
        void Build(string outPathUri);
        IModelCreator CreateProperty(string propertyName, string typeName);
        IModelCreator CreateProperty(string propertyName, string typeName, IPropertyDesignSettings propertyDesignSettings);
        IModelCreator EditGlobalSettings(Action<IGlobalSettings> action);
        IViewModelPropertyMapperContext AndMapToViewModelProperty(string named);
	    IModelCreator Start(string modelName);
    }
}