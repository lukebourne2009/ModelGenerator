﻿using System.IO;

namespace ModelGenerator.Core.Interfaces.ModelCreator
{
    public interface IFileCreator
    {
        Stream CreateFile(string fileName);
    }
}