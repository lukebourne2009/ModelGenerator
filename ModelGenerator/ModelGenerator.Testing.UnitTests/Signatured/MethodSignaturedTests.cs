﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.Signatures;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Signatured
{
	public class MethodSignaturedTests : BaseTest<MethodSignaturedTests.TestMethodSignatured>
	{
		public class TestMethodSignatured : MethodSignatured<object>
		{
			public TestMethodSignatured(Func<object, string> nameSelector,
				Func<object, IEnumerable<ISignatured>> genericSelector, 
				Func<object, IEnumerable<ISignatured>> inputSelector) 
				: base(nameSelector, genericSelector, inputSelector)
			{}
		}

		[Test]
		[TestCaseSource("Map_GivenAMethodWith_N_NumberOfInputs_ReturnThemPeriodSeperatedAtTheEndOfTheString_TestData")]
		public string Map_GivenAMethodWith_N_NumberOfInputs_ReturnThemPeriodSeperatedAtTheEndOfTheString(IList<ISignatured> inputs)
		{
			// Arrange
			var sut = new TestMethodSignatured(x => "Name", x => new List<ISignatured>(), x => inputs);

			// Act
			var result = sut.Signature;

			// Assert
			return result;
		}

		private static IEnumerable Map_GivenAMethodWith_N_NumberOfInputs_ReturnThemPeriodSeperatedAtTheEndOfTheString_TestData()
		{
			var inputs = new List<ISignatured>();
			yield return new TestCaseData(inputs)
			{
				TestName = "Map: Given 0 inputs, no inputs are appended",
				ExpectedResult = "Name"
			};

			inputs = new List<ISignatured>();
			inputs.Add(new DeclaredType(new ClassModel("Input1")));
			yield return new TestCaseData(inputs)
			{
				TestName = "Map: Given 1 input, result ends with its typename",
				ExpectedResult = "Name.Input1"
			};

			inputs = new List<ISignatured>();
			inputs.Add(new DeclaredType(new ClassModel("Input1")));
			inputs.Add(new DeclaredType(new ClassModel("Input2")));
			yield return new TestCaseData(inputs)
			{
				TestName = "Map: Given 2 inputs, result ends with thier typename, period seperated",
				ExpectedResult = "Name.Input1.Input2"
			};
		}
	}
}