﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using MoreLinq;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core.Exceptions;

namespace TesterHelper.Core
{
	[TestFixture]
	public abstract class BaseTest<TSut> 
		where TSut : class
	{
		protected TSut Sut { get; private set; }
		private static readonly IDictionary<string, object> Properties = new Dictionary<string, object>();
		private static readonly IDictionary<Type, object> DefaultObjects = new Dictionary<Type, object>
		{
			{typeof(string), ""}
		};

		protected static TType Mock<TType>()
			where TType : class
		{
			return Mock(typeof(TType)) as TType;
		}
		protected static TType Mock<TType>(string argumentName)
			where TType : class
		{
			return Mock(typeof(TType), argumentName) as TType;
		}
		protected static object Mock(Type type)
		{
			return Mock(type,"");
		}
		protected static object Mock(Type type, string argumentName)
		{
			var key = type.ToString() + '.' + argumentName;
			var value = Properties.SingleOrDefault(x => x.Key.Contains(key)).Value;
			if (value != null)
				return value;

			if (type.IsValueType || type == typeof(string))
				value = DefaultObjects[type];
			else
				value = Substitute.For(new[] { type }, new object[0]);

			Properties.Add(key, value);
			return value;
		}

		protected static TType Create<TType>() 
			where TType : class
		{
			return Create(typeof(TType)) as TType;
		}
		protected static object Create(Type type)
		{
			if (type.IsAbstract || type.IsInterface)
				throw new UnableToInstantiateException(type);

			var constructor = type.GetConstructors().MaxBy(x => x.GetParameters().Length);

			List<object> parameters = new List<object>();
			foreach (var parameterInfo in constructor.GetParameters())
			{
				parameters.Add(Mock(parameterInfo.ParameterType, parameterInfo.Name));
			}

			var result = constructor.Invoke(parameters.ToArray());

			return result;
		}
		[SetUp]
		public void BaseSetup()
		{
			Properties.Clear();
			Sut = Create<TSut>();
		}

		[Test]
		[TestCaseSource("NullParameterChecks_TestData")]
		public void NullParameterChecks(Action test)
		{
			test();
		}

		protected static IEnumerable NullParameterChecks_TestData()
		{
			List<TestCaseData> tests = new List<TestCaseData>();
			var methods = typeof(TSut).GetMethods().Where(x => x.DeclaringType == typeof(TSut));

			foreach (var method in methods)
			{
				if (method.DeclaringType.IsAbstract 
					|| method.DeclaringType.GetProperties().Any(x => x.GetSetMethod() == method))
					continue;

				var parameterInfos = method.GetParameters();
				for (var index = 0; index < parameterInfos.Length; index++)
				{
					var parameterInfo = parameterInfos[index];
					if (parameterInfo.ParameterType.IsValueType)
						continue;

					Action test = () =>
					{
						var sut = CreateSystemUnderTest(method);
						Assert.Throws<ArgumentNullException>(() =>
						{
							try
							{
								method.Invoke(sut, GenerateParameters(parameterInfos, parameterInfo));
							}
							catch (TargetInvocationException e)
							{
								if (e.InnerException != null) throw e.InnerException;
							}
						});
					};
					tests.Add(new TestCaseData(test)
					{
						TestName = string.Format("{2}: passing null for parameter {0} of type {1}, throws an exception.",
							index + 1, parameterInfo.ParameterType.Name, method.Name)
					});
				}
			}
			return tests;
		}
		private static object CreateSystemUnderTest(MethodInfo method)
		{
			object[] sutParameters = { };
			var constructorInfos = method.DeclaringType.GetConstructors();

			if (constructorInfos.Any())
			{
				var sutConstructor = constructorInfos[0];
				var sutConstructorParameters = sutConstructor.GetParameters();

				sutParameters = GenerateParameters(sutConstructorParameters);
			}

			var classInstance = Activator.CreateInstance(method.DeclaringType, sutParameters);
			return classInstance;
		}
		private static object[] GenerateParameters(ParameterInfo[] sutConstructorParameters, ParameterInfo excludedParameter = null)
		{
			object[] sutParameters = new object[sutConstructorParameters.Length];
			for (int i = 0; i < sutConstructorParameters.Length; i++)
			{
				if (excludedParameter != null && sutConstructorParameters[i].Equals(excludedParameter))
				{
					sutParameters[i] = null;
				}
				else
				{
					if (sutConstructorParameters[i].ParameterType.IsSealed)
					{
						sutParameters[i] = DefaultObjects[sutConstructorParameters[i].ParameterType];
					}
					else
					{
						var mockParameter = Substitute.For(new[] {sutConstructorParameters[i].ParameterType}, new object[0]);
						sutParameters[i] = mockParameter;
					}
				}
			}

			return sutParameters;
		}
	}
}