﻿using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels
{
    public class AttributeDesignModel : IAttributeDesignModel
    {
        public AttributeDesignModel(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}