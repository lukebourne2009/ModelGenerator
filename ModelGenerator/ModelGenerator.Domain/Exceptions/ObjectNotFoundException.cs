﻿using System;

namespace ModelGenerator.Domain.Exceptions
{
	public class ObjectNotFoundException : Exception
	{
		public ObjectNotFoundException(string objectSignature)
			: base(string.Format(
				"Method with signature: '{0}' could not be found. Have you registered the object?\r\n" +
				"Have you made sure the signature is in format: Object<1,2,3,...n>\r\n" +
				"Where 1,2,3 ect are 'n' amount of generics?", objectSignature))
		{
			
		}
		
	}
}