﻿using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Mapping;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using NUnit.Framework;

namespace ModelGenerator.Testing.UnitTests.Implementations
{
    [TestFixture]
    public class MapperCreateImplementationTests
    {
        [Test]
        [TestCaseSource("Implementation_GivenAModel_ReturnCorrectString_TestData")]
        public string Implementation_GivenAModel_ReturnCorrectString(Method method, ClassModel model, ConceptModel conceptModel)
        {
            // Arrange
			var sut = new MapperCreateMethodImplementation(new DeclaredType(model), conceptModel, new Scope(new DeclaredType(new ClassModel("BigType")), method));

            // Act
            var result = sut.Implementation;

            // Assert
            return result;
        }

        public static IEnumerable Implementation_GivenAModel_ReturnCorrectString_TestData()
        {
			//////////////////////////////////////////////
	        ConceptModel arg3 = new ConceptModel("Model");
	        yield return new TestCaseData(new Method("Method")
			{
				Inputs = new List<IMethodArgument> { new MethodArgument(arg3.Model, "source") },
				Output = new MethodOutput(new ClassModel("int"))
			}, new ClassModel("int"), arg3)
            {
                ExpectedResult = "return new int();",
                TestName = "Given model with no properties"
            };

			///////////////////////////////////////////////////////
            var model = new ClassModel("int");
            model.Properties.Add(new Property("arg", new DeclaredType(new ClassModel("Prop"))));
            yield return new TestCaseData(new Method("Method")
            {
                Output = new MethodOutput(model),
                Inputs = new List<IMethodArgument> { new MethodArgument(model, "source") }
			}, model, new ConceptModel("Model"))
            {
                ExpectedResult = "return new int()\r\n" +
                                 "{\r\n" +
                                 "arg = source.arg\r\n" +
                                 "};",
                TestName = "Given model with 1 property"
            };

			///////////////////////////////////////////////////////
            model = new ClassModel("int");
            model.Properties.Add(new Property("arg", new DeclaredType(new ClassModel("Prop"))));
            model.Properties.Add(new Property("arg2", new DeclaredType(new ClassModel("Prop2"))));
            yield return new TestCaseData(new Method("Method")
            {
                Output = new MethodOutput(model),
                Inputs = new List<IMethodArgument> { new MethodArgument(model, "source") }
			}, model, new ConceptModel("Model"))
            {
                ExpectedResult = "return new int()\r\n" +
                                 "{\r\n" +
                                 "arg = source.arg,\r\n" +
                                 "arg2 = source.arg2\r\n" +
                                 "};",
                TestName = "Given model with 2 properties"
            };

			/////////////////////////////////////////////////////
	        var conceptModel = new ConceptModel("Model");
	        conceptModel.ViewModel.Properties.Add(new Property("arg", new DeclaredType(new ClassModel("ViewModelProp"))));
	        conceptModel.Model.Properties.Add(new Property("arg", new DeclaredType(new ClassModel("ModelProp"))));
			conceptModel.ViewModel.Properties[0].PropertyMapping = new PropertyMapping(new DeclaredType(conceptModel.Model));
			conceptModel.ViewModel.Properties[0].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[0]);

			yield return new TestCaseData(new Method("Method")
			{
				Output = new MethodOutput(conceptModel.ViewModel),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.Model, "source") }
			}, conceptModel.ViewModel, conceptModel)
			{
				ExpectedResult = "return new ModelViewModel()\r\n" +
				                 "{\r\n" +
								 "arg = _modelViewModelargMapper.Map(source.arg)\r\n" +
				                 "};",
				TestName = "Given model with 1 property and 1 property mapping from Model to ViewModel"
			};

			/////////////////////////////////////////////////////
	        conceptModel = new ConceptModel("Model");
	        conceptModel.ViewModel.Properties.Add(new Property("arg1", new DeclaredType(new ClassModel("ViewModelProp"))));
	        conceptModel.Model.Properties.Add(new Property("arg2", new DeclaredType(new ClassModel("ModelProp"))));
	        conceptModel.Model.Properties.Add(new Property("arg3", new DeclaredType(new ClassModel("ModelProp"))));
			conceptModel.ViewModel.Properties[0].PropertyMapping = new PropertyMapping(new DeclaredType(conceptModel.Model));
			conceptModel.ViewModel.Properties[0].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[0]);
			conceptModel.ViewModel.Properties[0].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[1]);

			yield return new TestCaseData(new Method("Method")
			{
				Output = new MethodOutput(conceptModel.ViewModel),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.Model, "source") }
			}, conceptModel.ViewModel, conceptModel)
			{
				ExpectedResult = "return new ModelViewModel()\r\n" +
				                 "{\r\n" +
								 "arg1 = _modelViewModelarg1Mapper.Map(source.arg2, source.arg3)\r\n" +
				                 "};",
				TestName = "Given model 1 property mapping that uses 2 properties"
			};			
			
			/////////////////////////////////////////////////////
	        conceptModel = new ConceptModel("Model");
	        conceptModel.ViewModel.Properties.Add(new Property("arg1", new DeclaredType(new ClassModel("ViewModelProp"))));
	        conceptModel.ViewModel.Properties.Add(new Property("arg2", new DeclaredType(new ClassModel("ViewModelProp"))));
	        conceptModel.Model.Properties.Add(new Property("arg2", new DeclaredType(new ClassModel("ModelProp"))));
	        conceptModel.Model.Properties.Add(new Property("arg3", new DeclaredType(new ClassModel("ModelProp"))));
			conceptModel.ViewModel.Properties[0].PropertyMapping = new PropertyMapping(new DeclaredType(conceptModel.Model));
			conceptModel.ViewModel.Properties[0].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[0]);
			conceptModel.ViewModel.Properties[0].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[1]);
	        conceptModel.ViewModel.Properties[1].PropertyMapping = new PropertyMapping(new DeclaredType(conceptModel.Model));
	        conceptModel.ViewModel.Properties[1].PropertyMapping.UsingProperties.Add(conceptModel.Model.Properties[0]);

			yield return new TestCaseData(new Method("Method")
			{
				Output = new MethodOutput(conceptModel.ViewModel),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.Model, "source") }
			}, conceptModel.ViewModel, conceptModel)
			{
				ExpectedResult = "return new ModelViewModel()\r\n" +
				                 "{\r\n" +
								 "arg1 = _modelViewModelarg1Mapper.Map(source.arg2, source.arg3),\r\n" +
								 "arg2 = _modelViewModelarg2Mapper.Map(source.arg2)\r\n" +
				                 "};",
				TestName = "Given model 2 property mappings that use different variations of properties"
			};
			
			/////////////////////////////////////////////////////
	        conceptModel = new ConceptModel("Model");
	        conceptModel.ViewModel.Properties.Add(new Property("arg1", new DeclaredType(new ClassModel("ViewModelProp"))));
			conceptModel.ViewModel.Properties[0].PropertyMapping = new PropertyMapping(new DeclaredType(conceptModel.Model));

			yield return new TestCaseData(new Method("Method")
			{
				Output = new MethodOutput(conceptModel.ViewModel),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.Model, "source") }
			}, conceptModel.ViewModel, conceptModel)
			{
				ExpectedResult = "return new ModelViewModel()\r\n" +
				                 "{\r\n" +
								 "arg1 = _modelViewModelarg1Factory.Create()\r\n" +
				                 "};",
				TestName = "Given model 1 mapping with no using properties, create a prop factory"
			};

	        /////////////////////////////////////////////////////
	        conceptModel = new ConceptModel("Model");

	        yield return new TestCaseData(new Method("Method")
	        {
		        Output = new MethodOutput(conceptModel.ViewModel),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.Entity, "source") }
	        }, conceptModel.ViewModel, conceptModel)
	        {
		        ExpectedResult = "var modelModel = MapToModel(source);\r\n" +
		                         "var modelViewModel = MapToViewModel(modelModel);\r\n" +
		                         "return modelViewModel;",
		        TestName = "Given a mapping from Entity to ViewModel simply call the 2 working mappers"
	        };

	        /////////////////////////////////////////////////////
	        conceptModel = new ConceptModel("Model");

	        yield return new TestCaseData(new Method("Method")
	        {
		        Output = new MethodOutput(conceptModel.Entity),
				Inputs = new List<IMethodArgument> { new MethodArgument(conceptModel.ViewModel, "source") }
			}, conceptModel.Entity, conceptModel)
	        {
		        ExpectedResult = "var modelModel = MapToModel(source);\r\n" +
								 "var modelEntity = MapToEntity(modelModel);\r\n" +
								 "return modelEntity;",
				TestName = "Given a mapping from ViewModel to Entity simply call the 2 working mappers"
	        };
        }
    }
}