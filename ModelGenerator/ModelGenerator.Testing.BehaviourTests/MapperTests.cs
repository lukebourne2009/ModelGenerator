﻿using System;
using ModelGenerator.Common;
using ModelGenerator.Common.Factories;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Domain;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.ObjectCreator;
using ModelGenerator.Domain.Settings;
using ModelGenerator.Domain.Validators;
using ModelGenerator.Helpers;
using NUnit.Framework;

namespace ModelGenerator.Testing.BehaviourTests
{
    [TestFixture]
    public class MapperTests
    {
        private string interfaceTemplate = "public interface IModelMapper<TEntity, TModel, TViewModel>\r\n" +
                                           "{\r\n" +
										   "\tTEntity MapToEntity(TModel source);\r\n" +
										   "\tTEntity MapToEntity(TViewModel source);\r\n" +
										   "\tTModel MapToModel(TEntity source);\r\n" +
										   "\tTModel MapToModel(TViewModel source);\r\n" +
										   "\tTViewModel MapToViewModel(TEntity source);\r\n" +
										   "\tTViewModel MapToViewModel(TModel source);\r\n" +
                                           "\t\r\n" +
                                           "}\r\n";

        private string concreteTemplate =
@"public class MyModelModelMapper : IModelMapper<MyModelEntity, MyModelModel, MyModelViewModel>
{
	public MyModelEntity MapToEntity(MyModelModel source)
	{
		return new MyModelEntity()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyModelEntity MapToEntity(MyModelViewModel source)
	{
		var myModelModel = MapToModel(source);
		var myModelEntity = MapToEntity(myModelModel);
		return myModelEntity;
	}
	public MyModelModel MapToModel(MyModelEntity source)
	{
		return new MyModelModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyModelModel MapToModel(MyModelViewModel source)
	{
		return new MyModelModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyModelViewModel MapToViewModel(MyModelEntity source)
	{
		var myModelModel = MapToModel(source);
		var myModelViewModel = MapToViewModel(myModelModel);
		return myModelViewModel;
	}
	public MyModelViewModel MapToViewModel(MyModelModel source)
	{
		return new MyModelViewModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	
}
";

        [Test]
        public void Build_CreatesAMapperInterface()
        {
            // Arrange
            Uri path = new Uri(TestHelper.TestDirectory);
            IModelCreator creator = CreateCreator();

            // Act
			creator.Start("MyModel")
				.CreateProperty("PropertyEntity", "int")
                .CreateProperty("OtherPropertyEntity", "int")
                .CreateProperty("Property", "int")
                .Build(path);

            // Assert
            TestHelper.AssertFileMatchesTemplate(
                TestHelper.TestDirectory + "\\Core\\Interfaces\\IModelMapper.cs", 
                interfaceTemplate);
        }

        [Test]
        public void Build_CreatesOnly1MapperInterfaceWhenCalledTwice()
        {
            // Arrange
            Uri path = new Uri(TestHelper.TestDirectory);
            var man = new GlobalObjectManager();
            IModelCreator creator = CreateCreator(man);

            // Act
			creator.Start("MyModel")
				.CreateProperty("PropertyEntity", "int")
                .Build(path);

			CreateCreator(man)
				.Start("MyOtherModel")
				.CreateProperty("PropertyEntity", "int")
                .Build(path);

            // Assert
            TestHelper.AssertFileMatchesTemplate(
                TestHelper.TestDirectory + "\\Core\\Interfaces\\IModelMapper.cs",
                interfaceTemplate);
        }

        [Test]
        public void Build_CreatesTheConcreteMapperClass()
        {
            // Arrange
            Uri path = new Uri(TestHelper.TestDirectory);
            var man = new GlobalObjectManager();
            IModelCreator creator = CreateCreator(man);

            // Act
			creator.Start("MyModel")
				.CreateProperty("PropertyEntity", "int")
                .Build(path);

            // Assert
            TestHelper.AssertFileMatchesTemplate(
                TestHelper.TestDirectory + "\\Common\\Mappers\\MyModelModelMapper.cs",
                concreteTemplate);
        }

        private static ModelCreator CreateCreator(IGlobalObjectManager man = null)
        {
	        man = man ?? new GlobalObjectManager();
	        var declaredTypeMapper = new DeclaredTypeMapper(man);

	        var methodImplementationMapper = new MethodImplementationMapper(man, new ScopeMapper(man, declaredTypeMapper), declaredTypeMapper);
	        var implementationDictionaryMapper = new MethodImplementationDictionaryMapper(man,
		        methodImplementationMapper);

	        var classCreator = new ClassCreator(new FileCreator(), man,
		        new FieldFormatter(),
		        new ConstructorFormatter(),
		        new ClassMethodFormatter(), new ClassPropertyFormatter(),
		        new ObjectDeclerationFormatter(),
		        new BraceFormatter(), new GenericReplaceFormatter(), new OveridableValidator(),
		        new MethodArgumentMapper(declaredTypeMapper),
		        new MethodOutputMapper(declaredTypeMapper),
		        new ImplementationMapper(implementationDictionaryMapper, declaredTypeMapper),
		        new ConstructorMapper(new MethodArgumentMapper(declaredTypeMapper),
			        new MethodImplementationMapper(man, new ScopeMapper(man, declaredTypeMapper), declaredTypeMapper)),
		        new PropertyMapper(declaredTypeMapper),
		        new FieldMapper(declaredTypeMapper),
		        declaredTypeMapper,
		        implementationDictionaryMapper, methodImplementationMapper);

	        var interfaceCreator = new InterfaceCreator(new FileCreator(), new BraceFormatter(), man,
		        new MethodArgumentMapper(declaredTypeMapper), new MethodOutputMapper(declaredTypeMapper));

	        return new ModelCreator(classCreator,
		        interfaceCreator,
		        new ObjectTemplateBuilder(man, classCreator, interfaceCreator),
		        new PropertyDesignModelFactory(),
		        new GlobalSettings(),
		        man, new PropertyMappingMapper(man, declaredTypeMapper));
        }
    }
}