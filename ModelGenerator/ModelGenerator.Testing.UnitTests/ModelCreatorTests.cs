﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Core.Enums;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Settings;
using ModelGenerator.Core.Interfaces.Factories;
using ModelGenerator.Core.Interfaces.Helpers;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.ObjectCreator;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests
{
    [TestFixture]
    public class ModelCreatorTests : BaseTest<ModelCreator>
    {
        private static IModelCreator CreateModelCreator()
        {
            IObjectCreator objectCreator = Substitute.For<IObjectCreator>();
            IObjectCreator interfaceCreator = Substitute.For<IObjectCreator>();
            IGlobalSettings globalSettings = Substitute.For<IGlobalSettings>();
            IGlobalObjectManager globalObjectManager = Substitute.For<IGlobalObjectManager>();
            IPropertyDesignModelFactory factory = Substitute.For<IPropertyDesignModelFactory>();
			IModelCreator creator = new ModelCreator(objectCreator, 
				interfaceCreator, 
				Substitute.For<IObjectTemplateBuilder>(), 
				factory, 
				globalSettings, 
				globalObjectManager,
				Substitute.For<IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope>>());
            return creator;
        }
        private static IModelCreator CreateModelCreator(out IGlobalSettings globalSettings)
        {
            IObjectCreator classCreator = Substitute.For<IObjectCreator>();
            IObjectCreator interfaceCreator = Substitute.For<IObjectCreator>();
            globalSettings = Substitute.For<IGlobalSettings>();
            IGlobalObjectManager globalObjectManager = Substitute.For<IGlobalObjectManager>();
            IPropertyDesignModelFactory factory = Substitute.For<IPropertyDesignModelFactory>();
			IModelCreator creator = new ModelCreator(classCreator, 
				interfaceCreator, 
				Substitute.For<IObjectTemplateBuilder>(), 
				factory, 
				globalSettings, 
				globalObjectManager, 
				Substitute.For<IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope>>());
            return creator;
        }


		#region Build

		[Test]
		[TestCaseSource("Build_CreateObjectIsCalledForEachModel_TestData")]
	    public void Build_CreateObjectIsCalledForEachModel(Func<ModelCreator, IObjectModel> modelSelector)
	    {
		    // Arrange
		    var sut = Create<ModelCreator>();

			// Act
			sut.Start("Model").Build("C:\\path");

			// Assert
		    Mock<IObjectCreator>("classCreator").Received(1).CreateObject(modelSelector(sut));
	    }
	    private static IEnumerable Build_CreateObjectIsCalledForEachModel_TestData()
	    {
		    yield return new TestCaseData(new Func<ModelCreator, IObjectModel>(x => x.Model.Entity))
		    {
			    TestName = "Build: Entity Object is created"
		    };
		    yield return new TestCaseData(new Func<ModelCreator, IObjectModel>(x => x.Model.Model))
			{
			    TestName = "Build: Model Object is created"
		    };
		    yield return new TestCaseData(new Func<ModelCreator, IObjectModel>(x => x.Model.ViewModel))
			{
			    TestName = "Build: ViewModel Object is created"
		    };
	    }
		[Test]
	    public void Build_CreateObjectIsCalledForIModelMapper()
	    {
			// Arrange
		    var sut = Create<ModelCreator>();

		    // Act
		    sut.Start("Model").Build("C:\\path");

		    // Assert
		    Mock<IObjectCreator>("interfaceCreator").Received(1).CreateObject(Mock<IObjectTemplateBuilder>().BuildIModelMapper());
		}
		[Test]
	    public void Build_IModelMapperIsBuiltAtCorrectFileLocation()
	    {
			// Arrange
		    var sut = Create<ModelCreator>();

		    // Act
		    sut.Start("Model").Build("C:\\path");

		    // Assert
		    Mock<IObjectCreator>("interfaceCreator").Received(1)
				.CreateObject(Arg.Is<IObjectModel>(x => x.FilePath.LocalPath == "C:\\path\\Core\\Interfaces"));
		}
		[Test]
		public void Build_CreateObjectIsCalledForTheConcreteModelMapper()
	    {
			// Arrange
		    var sut = Create<ModelCreator>();

		    // Act
		    sut.Start("Model").Build("C:\\path");

		    // Assert
		    Mock<IObjectCreator>("classCreator").CreateObject(Mock<IObjectTemplateBuilder>().BuildModelMapper("Model"));
		}
	    [Test]
	    public void Build_ConcreteModelMapperIsBuiltAtCorrectFileLocation()
	    {
		    // Arrange
		    var sut = Create<ModelCreator>();

		    // Act
		    sut.Start("Model").Build("C:\\path");

		    // Assert
		    Mock<IObjectCreator>("classCreator").Received(1)
			    .CreateObject(Arg.Is<IObjectModel>(x => x.FilePath.LocalPath == "C:\\path\\Common\\Mappers"));
		}

		#endregion

        [Test]
        public void CreateProperty_GivenAProperty_AddThatPropertyToEachModel()
        {
            // Arrange
	        var sut = Create<ModelCreator>();

			// Act
	        sut.Start("Model").CreateProperty("Prop", "int");

            // Assert
            Mock<IObjectCreator>("classCreator").Received(3).SetProperty(Arg.Any<IObjectModel>(), 
                Arg.Any<IPropertyDesignModel>());
        }

		#region Edit Global settings

		[Test]
        public void EditGlobalSettings_GivenNullInput_ThrowException()
        {
            // Arrange
	        var sut = Create<ModelCreator>();

            // Act
            TestDelegate test = () => sut.EditGlobalSettings(null);

            // Assert
            Assert.Throws<ArgumentNullException>(test);
        }
        [TestCase(EntityIdType.Int)]
        [TestCase(EntityIdType.String)]
        public void EditGlobalSettings_GivenAnActionThatChangesASetting_SettingIsChanged(EntityIdType type)
        {
            // Arrange
	        var sut = Create<ModelCreator>();

			// Act
	        sut.EditGlobalSettings(x => x.DefaultEntityIdType = type);

            // Assert
            Assert.AreEqual(type, Mock<IGlobalSettings>().DefaultEntityIdType);
		}

		#endregion

		[Test]
		public void AndMapToViewModelProperty_ReturnsNonNull()
	    {
			// Arrange
		    var sut = Create<ModelCreator>();

			// Act
			var result = sut.AndMapToViewModelProperty("Prop");

		    // Assert
			Assert.IsNotNull(result);
	    }

        [Test]
        [TestCase("Model.Prop")]
        [TestCase(".Prop")]
        [TestCase("Model-Prop")]
        [TestCase("")]
        public void AndMapToViewModelProperty_CertainFormatsOfInputAreRejected_AndThrow(string property)
        {
			// Arrange
			var sut = Create<ModelCreator>();

			// Act
			TestDelegate del = () => sut.AndMapToViewModelProperty(property);

            // Assert
            Assert.Throws<ArgumentException>(del);
        }
    }
}
