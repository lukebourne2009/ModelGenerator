﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces
{
    public interface IGlobalObjectManager
    {
        void RegisterObject(IObjectModel model);
	    void RegisterModel(IModel model);
	    IModel TryGetModel(string modelName);
        IObjectModel GetObject(string name);
	    IProperty TryGetProperty(string prop);
	    IProperty GetProperty(string prop);
	    IMethod TryGetMethod(string methodSig);
	    IMethod GetMethod(string methodSig);
	    IModel GetModel(string modelSig);
    }
}