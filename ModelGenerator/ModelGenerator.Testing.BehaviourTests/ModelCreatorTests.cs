﻿using System.IO;
using System.Linq;
using System.Text;
using ModelGenerator.Common;
using ModelGenerator.Common.Factories;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Enums;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Settings;
using ModelGenerator.Domain;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.ObjectCreator;
using ModelGenerator.Domain.Settings;
using ModelGenerator.Domain.Validators;
using ModelGenerator.Helpers;
using NUnit.Framework;

namespace ModelGenerator.Testing.BehaviourTests
{
    [TestFixture]
    public class ModelCreatorTests
    {
        [Test]
        public void Build_Creates3CSharpFilesWithTheCorrectNamesAtLocationProvided()
        {
            // Arrange
            string objectName = "MyModel";
            IModelCreator creator = CreateModelCreator();

            // Act
            creator.Start(objectName).Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);
            Assert.IsTrue(files.Any(x => x.EndsWith("MyModelViewModel.cs")));
            Assert.IsTrue(files.Any(x => x.EndsWith("MyModelModel.cs" )));
            Assert.IsTrue(files.Any(x => x.EndsWith("MyModelEntity.cs" )));
        }

        [Test]
        public void Build_Creates3CSharpFilesWithTheCorrectClassDefinitions()
        {
            // Arrange
            string entityWithId =
                "public class {0}Entity\r\n" +
                "{{\r\n" +
                //"\t[Key]\r\n" +
                //"\tpublic {1} Id {{ get; set; }}\r\n"+
                "\t\r\n" +
                "}}\r\n";
            string objectName = "MyModel";
            var creator = CreateModelCreator();

            // Act
			creator.Start(objectName).Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);

            AssertFileMatchesTemplate(files[0], string.Format(entityWithId,"MyModel","int"));
            AssertFileContents(files[1], "Model");
            AssertFileContents(files[2], "ViewModel");
        }

        [Test]
        public void CreateProperty_CreatingAPropertyCreatesAPropertyInEachFile()
        {
            // Arrange
            string classWithProperty =
                "public class {0}{1}\r\n" +
                "{{\r\n" +
                "\tpublic {2} {3} {{ get; set; }}\r\n" +
                "\t\r\n" +
                "}}\r\n";
            string objectName = "MyModel";
            IModelCreator creator = CreateModelCreator();

            // Act
			creator.Start(objectName)
				.CreateProperty("MyProperty", "int")
                .Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);
            AssertFileMatchesTemplate(files[0], string.Format(entityWithIdAndProperty, "MyModel", "int", "int", "MyProperty"));
            AssertFileMatchesTemplate(files[1], string.Format(classWithProperty, "MyModel", "Model", "int", "MyProperty"));
            AssertFileMatchesTemplate(files[2], string.Format(classWithProperty, "MyModel", "ViewModel", "int", "MyProperty"));
        }

        [Test]
        public void CreateProperty_CreatingMultiplePropertiesCreatesEachProperty()
        {
            // Arrange
            string classWithTwoProperties =
                "public class {0}{1}\r\n" +
                "{{\r\n" +
                "\tpublic {2} {3} {{ get; set; }}\r\n" +
                "\tpublic {4} {5} {{ get; set; }}\r\n" +
                "\t\r\n" +
                "}}\r\n";
            string objectName = "MyModel";
            IModelCreator creator = CreateModelCreator();

            // Act
			creator.Start(objectName)
				   .CreateProperty("MyProperty", "int")
                   .CreateProperty("str", "string")
                   .Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);
            AssertFileMatchesTemplate(files[0], string.Format(entityWithIdAndTwoProperties, "MyModel","int", "int", "MyProperty", "string", "str"));
            AssertFileMatchesTemplate(files[1], string.Format(classWithTwoProperties, "MyModel", "Model", "int", "MyProperty", "string", "str"));
            AssertFileMatchesTemplate(files[2], string.Format(classWithTwoProperties, "MyModel", "ViewModel", "int", "MyProperty", "string", "str"));
        }

        [Test]
        public void CreateProperty_CanProvideRequiredSettingForProperty()
        {
            // Arrange
            string classWithRequired =
                "public class {0}{1}\r\n" +
                "{{\r\n" +
                "\t[Required]\r\n" +
                "\tpublic {2} {3} {{ get; set; }}\r\n" +
                "\t\r\n" +
                "}}\r\n";
            string objectName = "MyModel";
            var creator = CreateModelCreator();

            // Act
			creator.Start(objectName)
				.CreateProperty("MyProperty", "int", new PropertyDesignSettings { Required = true })
                .Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);
            AssertFileMatchesTemplate(files[0], string.Format(entityWithIdAndPropertyAndRequired, "MyModel", "int", "int", "MyProperty"));
            AssertFileMatchesTemplate(files[1], string.Format(classWithProperty, "MyModel", "Model", "int", "MyProperty"));
            AssertFileMatchesTemplate(files[2], string.Format(classWithRequired, "MyModel", "ViewModel", "int", "MyProperty"));
        }

        [TestCase(EntityIdType.Int)]
        [TestCase(EntityIdType.String)]
        public void EditGlobalSettings_DefaultIdTypeCanBeSetToChangeTheEntityIdType(EntityIdType type)
        {
            // Arrange
            IModelCreator creator = CreateModelCreator();

            // Act
			creator.Start("MyModel")
				.EditGlobalSettings(x => x.DefaultEntityIdType = type)
                .CreateProperty("MyProperty", "int")
                .Build(TestHelper.TestDirectory);

            // Assert
            string[] files = Directory.GetFiles(TestHelper.TestDirectory);
            AssertFileMatchesTemplate(files[0], string.Format(entityWithIdAndProperty, "MyModel", type.ToString().ToLower(),"int" , "MyProperty"));
            AssertFileMatchesTemplate(files[1], string.Format(classWithProperty, "MyModel", "Model", "int", "MyProperty"));
            AssertFileMatchesTemplate(files[2], string.Format(classWithProperty, "MyModel", "ViewModel", "int", "MyProperty"));
        }

        private static ModelCreator CreateModelCreator()
        {
	        var man = new GlobalObjectManager();

	        var typeMapper = new DeclaredTypeMapper(man);

	        var methodImplementationMapper = new MethodImplementationMapper(man, new ScopeMapper(man, typeMapper), typeMapper);
	        var implementationDictionaryMapper =
		        new MethodImplementationDictionaryMapper(man,
			        methodImplementationMapper);

	        var classCreator = new ClassCreator(new FileCreator(), man,
		        new FieldFormatter(),
		        new ConstructorFormatter(),
		        new ClassMethodFormatter(), new ClassPropertyFormatter(), new ObjectDeclerationFormatter(),
		        new BraceFormatter(), new GenericReplaceFormatter(), new OveridableValidator(),
		        new MethodArgumentMapper(new DeclaredTypeMapper(man)),
		        new MethodOutputMapper(typeMapper), new ImplementationMapper(implementationDictionaryMapper, typeMapper),
		        new ConstructorMapper(new MethodArgumentMapper(new DeclaredTypeMapper(man)),
			        new MethodImplementationMapper(man, new ScopeMapper(man, typeMapper), typeMapper)),
		        new PropertyMapper(typeMapper),
		        new FieldMapper(new DeclaredTypeMapper(man)),
		        typeMapper,
		        implementationDictionaryMapper, methodImplementationMapper);

	        var interfaceCreator = new InterfaceCreator(new FileCreator(), new BraceFormatter(), man,
		        new MethodArgumentMapper(new DeclaredTypeMapper(man)), new MethodOutputMapper(typeMapper));

	        return new ModelCreator(classCreator,
		        interfaceCreator,
		        new ObjectTemplateBuilder(man, classCreator, interfaceCreator),
		        new PropertyDesignModelFactory(),
		        new GlobalSettings(), man, new PropertyMappingMapper(man, typeMapper));
        }
        private static void AssertFileContents(string file, string name)
        {
            using (FileStream stream = File.Open(file, FileMode.Open))
            {
                byte[] text = new byte[(int) stream.Length];
                stream.Read(text, 0, (int) stream.Length);
                string fileString = Encoding.ASCII.GetString(text);
                Assert.AreEqual("public class MyModel" + name + "\r\n{\r\n\t\r\n}\r\n", fileString);
            }
        }
        private static void AssertFileMatchesTemplate(string file, string template)
        {
            using (FileStream stream = File.Open(file, FileMode.Open))
            {
                byte[] text = new byte[(int) stream.Length];
                stream.Read(text, 0, (int) stream.Length);
                string fileString = Encoding.ASCII.GetString(text);
                Assert.AreEqual(template, fileString);
            }
        }

        private const string classWithProperty =
            "public class {0}{1}\r\n" +
            "{{\r\n" +
            "\tpublic {2} {3} {{ get; set; }}\r\n" +
            "\t\r\n" +
            "}}\r\n";
        private const string entityWithIdAndProperty =
            "public class {0}Entity\r\n" +
            "{{\r\n" +
            //"\t[Key]\r\n" +
            //"\tpublic {1} Id {{ get; set; }}\r\n" +
            "\tpublic {2} {3} {{ get; set; }}\r\n" +
            "\t\r\n" +
            "}}\r\n";
        private const string entityWithIdAndPropertyAndRequired =
            "public class {0}Entity\r\n" +
            "{{\r\n" +
            //"\t[Key]\r\n" +
            //"\tpublic {1} Id {{ get; set; }}\r\n" +
            "\t[Required]\r\n" +
            "\tpublic {2} {3} {{ get; set; }}\r\n" +
            "\t\r\n" +
            "}}\r\n";
        private const string entityWithIdAndTwoProperties =
            "public class {0}Entity\r\n" +
            "{{\r\n" +
            //"\t[Key]\r\n" +
            //"\tpublic {1} Id {{ get; set; }}\r\n" +
            "\tpublic {2} {3} {{ get; set; }}\r\n" +
            "\tpublic {4} {5} {{ get; set; }}\r\n" +
            "\t\r\n" +
            "}}\r\n";
    }
}