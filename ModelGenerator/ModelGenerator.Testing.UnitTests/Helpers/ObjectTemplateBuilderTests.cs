﻿using System;
using System.Collections;
using System.Linq;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Helpers;
using ModelGenerator.Testing.BehaviourTests;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Helpers
{
	[TestFixture]
	public class ObjectTemplateBuilderTests : BaseTest<ObjectTemplateBuilder>
	{
		private static ObjectTemplateBuilder _sut;
		private static IGlobalObjectManager _manager;
		private static IObjectCreator _classCreator;
		private static IObjectCreator _interfaceCreator;

		[SetUp]
		public static void Setup()
		{
			_manager = Substitute.For<IGlobalObjectManager>();
			_classCreator = Substitute.For<IObjectCreator>();
			_interfaceCreator = Substitute.For<IObjectCreator>();
			_sut = new ObjectTemplateBuilder(_manager, _classCreator, _interfaceCreator);
		}

		#region BuildModelMapper

		[Test]
		[TestCase("Name")]
		[TestCase("OtherName")]
		public void BuildModelMapper_GivenAModelMatchingTheSignatureAlreadyExists_DontBuildModel(string name)
		{
			// Arrange
			ClassModel model = new ClassModel(name + "ModelMapper");
			_manager.GetObject(name + "ModelMapper").Returns(model);

			// Act
			var result = _sut.BuildModelMapper(name);

			// Assert
			Assert.AreEqual(model, result);
		}

		[Test]
		public void BuildModelMapper_GivenAModelDoesntExists_RegisterModel()
		{
			// Arrange
			_manager.GetObject("NameModelMapper").Returns((IObjectModel)null);
			_classCreator.Implement(Arg.Any<IObjectModel>(), Arg.Any<IImplementationDesignModel>())
				.Returns(x => x.ArgAt<IObjectModel>(0));

			// Act
			var result = _sut.BuildModelMapper("Name");

			// Assert
			_manager.Received(1).RegisterObject(Arg.Any<ClassModel>());
		}

		[Test]
		[TestCaseSource("BuildModelMapper_RegisterAModelWithSpecificAttributes_TestData")]
		public void BuildModelMapper_ImplementAModelWithSpecificAttributes(Func<ImplementationDesignModel, bool> comparison)
		{
			// Arrange
			_manager.GetObject("NameModelMapper").Returns((IObjectModel)null);
			_manager.GetObject("IModelMapper<0,1,2>").Returns(new InterfaceModel("IModelMapper"));

			// Act
			var result = _sut.BuildModelMapper("Name");

			// Assert
			_classCreator.Received(1).Implement(Arg.Any<ClassModel>(), Arg.Is<ImplementationDesignModel>(x => comparison(x)));
		}
		private static IEnumerable BuildModelMapper_RegisterAModelWithSpecificAttributes_TestData()
		{
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => true)) { TestName = "BuildModelMapper: has 1 implementation" };
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => x.Model == _manager.GetObject("IModelMapper<0,1,2>"))) { TestName = "BuildModelMapper: has implements 'IModelMapper'" };
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => x.GenericArguments.Count == 3 )) { TestName = "BuildModelMapper: implemented 3 generic arguments" };
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => x.GenericArguments.ContainsKey("TEntity") &&
			                                                                           x.GenericArguments.ContainsKey("TModel") &&
			                                                                           x.GenericArguments.ContainsKey("TViewModel"))) { TestName = "BuildModelMapper: implements all 3 generic arguments of 'IModelMapper'" };
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => x.MethodImplementations.Values.All(y => y.GetType() == typeof(MapperCreateMethodImplementationDesignModel)))) 
			{ TestName = "BuildModelMapper: All Method Implementations are Mapper Creates" };			
			yield return new TestCaseData((Func<ImplementationDesignModel, bool>)(x => x.MethodImplementations.Values.All(y => y.Scope != null))) 
			{ TestName = "BuildModelMapper: Scope is injected into the implementation design model" };
		}

		[Test]
		public void BuildModelMapper_RegisterAModelAfterImplementing()
		{
			// Arrange
			_manager.GetObject("NameModelMapper").Returns((IObjectModel)null);
			_manager.GetObject("IModelMapper<0,1,2>").Returns(new InterfaceModel("IModelMapper"));
			ClassModel model = new ClassModel("Model");
			_classCreator.Implement(Arg.Any<IObjectModel>(), Arg.Any<IImplementationDesignModel>())
				.Returns(model);

			// Act
			var result = _sut.BuildModelMapper("Name");

			// Assert
			_manager.Received(1).RegisterObject(Arg.Is<ClassModel>(x => x.Name == "NameModelMapper"));
		}

		#endregion New Region


		#region BuildIFactory

		[Test]
		public void BuildIFactory_GivenModelExists_ReturnExistingModel()
		{
			// Arrange
			InterfaceModel model = new InterfaceModel("IFactory");
			_manager.GetObject("IFactory<0>").Returns(model);

			// Act
			var result = _sut.BuildIFactory();

			// Assert
			Assert.AreEqual(model, result);
		}

		[Test]
		public void BuildIFactory_GivenModelDoesntExist_ReturnNewInterfaceModel()
		{
			// Arrange
			_manager.GetObject("IFactory<0>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory();

			// Assert
			Assert.IsInstanceOf<InterfaceModel>(result);
		}

		[Test]
		public void BuildIFactory_GivenModelDoesntExist_RegisterNewModel()
		{
			// Arrange
			_manager.GetObject("IFactory<0>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory();

			// Assert
			_manager.Received(1).RegisterObject(Arg.Any<InterfaceModel>());
		}

		[Test]
		[TestCase("IFactory<0>", 0, TestName = "BuildIFactory: 1 output Generic, 0 inputs")]
		[TestCase("IFactory<0,1>", 1, TestName = "BuildIFactory: 1 output Generic, 1 input")]
		[TestCase("IFactory<0,1,2>", 2, TestName = "BuildIFactory: 1 output Generic, 2 inputs")]
		public void BuildIFactory_GenericsAreSetBasedOnOutputsAndInputs(string template, int nInputs)
		{
			// Arrange
			_manager.GetObject(template).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory(nInputs);

			// Assert
			_interfaceCreator.Received(1).SetGeneric(Arg.Any<InterfaceModel>(), "TObject");
			for (int i = 0; i < nInputs; i++)
			{
				_interfaceCreator.Received(1).SetGeneric(Arg.Any<InterfaceModel>(), "TSource" + (i + 1));
			}
		}

		[Test]
		public void BuildIFactory_OutputIsSet()
		{
			// Arrange
			_manager.GetObject("IFactory<0>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory();

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(),
				Arg.Is<MethodDesignModel>(x => x.Output.Type == "TObject"));
		}

		[Test]
		[TestCase("IFactory<0>", 0, TestName = "BuildIFactory: 0 inputs")]
		[TestCase("IFactory<0,1>", 1, TestName = "BuildIFactory: 1 input")]
		[TestCase("IFactory<0,1,2>", 2, TestName = "BuildIFactory: 2 inputs")]
		public void BuildIFactory_InputsAreSet(string template, int nInputs)
		{
			// Arrange
			_manager.GetObject(template).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory(nInputs);

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(), Arg.Is<MethodDesignModel>(x => x.Inputs.Count == nInputs));
		}

		[Test]		
		[TestCase("IFactory<0,1>", 1, TestName = "BuildIFactory: inputs named: source1")]
		[TestCase("IFactory<0,1,2>", 2, TestName = "BuildIFactory: inputs named: source1, source2")]
		public void BuildIFactory_InputsAreNamedCorrectly(string template, int nInputs)
		{
			// Arrange
			_manager.GetObject(template).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory(nInputs);

			// Assert
			for (int i = 0; i < nInputs; i++)
			{
				int index = i;
				_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(),
					Arg.Is<MethodDesignModel>(x => x.Inputs.Any(y => y.Name == "source" + (index + 1))));
			}
		}

		[Test]
		[TestCase("IFactory<0,1>", 1, TestName = "BuildIFactory: inputs Typed: TSource1")]
		[TestCase("IFactory<0,1,2>", 2, TestName = "BuildIFactory: inputs Typed: TSource1, TSource2")]
		public void BuildIFactory_InputsAreGivenTheCorrectGenericType(string template, int nInputs)
		{
			// Arrange
			_manager.GetObject(template).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIFactory(nInputs);

			// Assert
			for (int i = 0; i < nInputs; i++)
			{
				int index = i;
				_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(),
					Arg.Is<MethodDesignModel>(x => x.Inputs.Any(y => y.Type == "TSource" + (index + 1))));
			}
		}

		#endregion New Region


		#region BuildIModelMapper

		[Test]
		public void BuildIModelMapper_GivenAModelDoesntExist_RegisterModel()
		{
			// Arrange
			_manager.GetObject("NameModelMapper").Returns((IObjectModel)null);
			_classCreator.Implement(Arg.Any<IObjectModel>(), Arg.Any<IImplementationDesignModel>())
				.Returns(x => x.ArgAt<IObjectModel>(0));

			// Act
			var result = _sut.BuildModelMapper("Name");

			// Assert
			_manager.Received(1).RegisterObject(Arg.Any<ClassModel>());
		}

		[Test]
		public void BuildIModelMapper_GivenAModelExists_ReturnIt()
		{
			// Arrange
			InterfaceModel model = new InterfaceModel("IModelMapper");
			_manager.GetObject("IModelMapper<0,1,2>").Returns(model);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			Assert.AreEqual(model, result);
		}

		[Test]
		public void BuildIModelMapper_3GenericsAreCreated()
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(3).SetGeneric(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"),Arg.Any<string>());
		}

		[Test]
		[TestCase("TEntity", TestName = "BuildIModelMapper: A Generic with type 'TEntity' is set")]
		[TestCase("TModel", TestName = "BuildIModelMapper: A Generic with type 'TModel' is set")]
		[TestCase("TViewModel", TestName = "BuildIModelMapper: A Generic with type 'TViewModel' is set")]
		public void BuildIModelMapper_3GenericsAreCreated(string genericName)
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(1).SetGeneric(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"), genericName);
		}

		[Test]
		public void BuildIModelMapper_6MethodsAreCreated()
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(6).SetMethod(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"), Arg.Any<IMethodDesignModel>());
		}

		[Test]
		[TestCase("MapToEntity", TestName = "BuildIModelMapper: 2 methods with names MapToEntity are set")]
		[TestCase("MapToModel", TestName = "BuildIModelMapper: 2 methods with names 'MapToModel' are set")]
		[TestCase("MapToViewModel", TestName = "BuildIModelMapper: 2 methods with names 'MapToViewModel' are set")]
		public void BuildIModelMapper_6MethodsAreCreated(string method)
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(2).SetMethod(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"), 
				Arg.Is<IMethodDesignModel>(x => x.Name == method));
		}

		[Test]
		[TestCase("MapToEntity", "TEntity", TestName = "BuildIModelMapper: 'MapToEntity' returns a 'TEntity'")]
		[TestCase("MapToModel", "TModel", TestName = "BuildIModelMapper: 'MapToModel' returns a 'TModel'")]
		[TestCase("MapToViewModel", "TViewModel", TestName = "BuildIModelMapper: 'MapToViewModel' returns a 'TViewModel'")]
		public void BuildIModelMapper_EachMethodReturnsTheCorrectType(string method, string output)
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(2).SetMethod(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"),
				Arg.Is<IMethodDesignModel>(x => x.Output.Signature == output && x.Name == method));
		}

		[Test]
		[TestCase("MapToEntity", "TModel", TestName = "BuildIModelMapper: 'MapToEntity' takes a 'TModel' as input")]
		[TestCase("MapToEntity", "TViewModel", TestName = "BuildIModelMapper: 'MapToEntity' takes a 'TViewModel' as input")]
		[TestCase("MapToModel", "TEntity", TestName = "BuildIModelMapper: 'MapToModel' takes a 'TEntity' as input")]
		[TestCase("MapToModel", "TViewModel", TestName = "BuildIModelMapper: 'MapToModel' takes a 'TViewModel' as input")]
		[TestCase("MapToViewModel", "TEntity", TestName = "BuildIModelMapper: 'MapToViewModel' takes a 'TEntity' as input")]
		[TestCase("MapToViewModel", "TModel", TestName = "BuildIModelMapper: 'MapToViewModel' takes a 'TModel' as input")]
		public void BuildIModelMapper_EachMethodHasTheCorrectReleventInputs(string method, string input)
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"),
				Arg.Is<IMethodDesignModel>(x => x.Inputs.Count == 1 && x.Inputs[0].Type == input && x.Name == method));
		}

		[Test]
		public void BuildIModelMapper_EachMethodInputIsNamedCorrectly()
		{
			// Arrange
			_manager.GetObject("IModelMapper<0,1,2>").Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIModelMapper();

			// Assert
			_interfaceCreator.Received(6).SetMethod(Arg.Is<InterfaceModel>(x => x.Name == "IModelMapper"),
				Arg.Is<IMethodDesignModel>(x => x.Inputs.Count == 1 && x.Inputs[0].Name == "source"));
		}

		#endregion New Region


		#region BuildIMapper

		[Test]
		public void BuildIMapper_GivenAModelDoesntExists_RegisterModel()
		{
			// Arrange
			_manager.GetObject("IMapper<0>").Returns((IObjectModel)null);
			_classCreator.Implement(Arg.Any<IObjectModel>(), Arg.Any<IImplementationDesignModel>())
				.Returns(x => x.ArgAt<IObjectModel>(0));

			// Act
			var result = _sut.BuildIMapper();

			// Assert
			_manager.Received(1).RegisterObject(Arg.Any<InterfaceModel>());
		}

		[Test]
		public void BuildIMapper_GivenAModelExists_ReturnModelWithoutRegistering()
		{
			// Arrange
			InterfaceModel model = new InterfaceModel("IMapper");
			_manager.GetObject("IMapper<0>").Returns(model);

			// Act
			var result = _sut.BuildIMapper();

			// Assert
			Assert.AreEqual(model, result);
			_manager.Received(0).RegisterObject(Arg.Any<IObjectModel>());
		}

		[Test]
		[TestCase(0, TestName = "BuildIMapper: 1 generic is created")]
		[TestCase(1, TestName = "BuildIMapper: 2 generics are created")]
		[TestCase(2, TestName = "BuildIMapper: 3 generics are created")]
		public void BuildIMapper_AnOutputAndReleventInputGenericsAreCreated(int nInputs)
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(nInputs);

			// Assert
			_interfaceCreator.Received(nInputs+1).SetGeneric(Arg.Any<InterfaceModel>(), Arg.Any<string>());
		}

		[Test] 
		[TestCase(0, "TTarget", TestName = "BuildIMapper: 1st generic is called 'TTarget'")]
		[TestCase(1, "TSource1", TestName = "BuildIMapper: 2nd generic is called 'TSource1'")]
		[TestCase(2, "TSource2", TestName = "BuildIMapper: 3rd generic is called 'TSource2'")]
		public void BuildIMapper_GenericTypesAreNamedCorrectly(int nInputs, string name)
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(nInputs);

			// Assert
			_interfaceCreator.Received(1).SetGeneric(Arg.Any<InterfaceModel>(), name);
		}

		[Test]
		public void BuildIMapper_1MethodIsCreated()
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(3);

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(), Arg.Any<IMethodDesignModel>());
		}

		[Test]
		public void BuildIMapper_MethodIsCalledMap()
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(3);

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(), Arg.Is<IMethodDesignModel>(x => x.Name == "Map"));
		}

		[Test]
		public void BuildIMapper_MethodOutputsTTarget()
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(3);

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(), 
				Arg.Is<IMethodDesignModel>(x => x.Output.Signature == "TTarget"));
		}

		[Test]
		[TestCase(0, TestName = "BuildIMapper: no inputs are created")]
		[TestCase(1, TestName = "BuildIMapper: 1 input is created")]
		[TestCase(2, TestName = "BuildIMapper: 2 inputs are created")]
		public void BuildIMapper_AddsCorrectNumberOfInputs(int nInputs)
		{
			// Arrange
			_manager.GetObject(Arg.Any<string>()).Returns((IObjectModel)null);

			// Act
			var result = _sut.BuildIMapper(nInputs);

			// Assert
			_interfaceCreator.Received(1).SetMethod(Arg.Any<InterfaceModel>(),
				Arg.Is<IMethodDesignModel>(x => x.Inputs.Count == nInputs));
		}

		#endregion New Region
	}
}