﻿using System.IO;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Formatters;

namespace ModelGenerator.Domain.Formatter
{
    public class BraceFormatter : IFormatter
    {
        public string Format(string classString)
        {
            StringReader reader = new StringReader(classString);
            StringBuilder builder = new StringBuilder();

            string read = null;
            int tabs = 0;

            while ((read = reader.ReadLine()) != null)
            {
                var openbrackCount = read.Count(x => x == '{');
                var closebrackCount = read.Count(x => x == '}');
                if (openbrackCount - closebrackCount > 0 || !read.StartsWith("}"))
                {
                    builder.AppendLine(new string('\t', tabs) + read);
                    tabs += openbrackCount - closebrackCount;
                }
                else
                {
                    tabs += openbrackCount - closebrackCount;
                    builder.AppendLine(new string('\t', tabs) + read);
                }
            }

            return builder.ToString();
        }
    }
}