﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;

namespace ModelGenerator.Domain
{
	public class Scope : IScope
	{
		public Scope(IDeclaredType declaredType)
		{
			Model = declaredType;
		}

		public Scope(IDeclaredType declaredType, IMethod method)
		{
			Model = declaredType;
			Method = method;
		}

		public IDeclaredType Model { get; private set; }
		public IMethod Method { get; private set; }
		public IList<IVariable> GetVariables()
		{
			return (Model.Type as IObjectModel).Fields.Cast<IVariable>().ToList();
		}
		public IGeneric GetGeneric(string name)
		{
			return Model.Type.Generics.FirstOrDefault(x => x.Signature == name);
		}
		public IDeclaredType GetGenericArgument(string genericType)
		{
			var generic = GetGeneric(genericType);
			if (generic == null)
				throw new InvalidOperationException(string.Format("No generic with signature: '{0}' exists in scope", genericType));

			return GetGenericArgument(generic);
		}

		public IDeclaredType GetGenericArgument(IGeneric genericType)
		{
			if (!Model.GenericArguments.ContainsKey(genericType))
			{
				var genArg = (Model.Type as IObjectModel).Implementations.SelectMany(x => x.DeclaredType.GenericArguments)
					.FirstOrDefault(x => x.Key == genericType);
				if(genArg.Equals(default(KeyValuePair<IGeneric, IDeclaredType>)))
					throw new InvalidOperationException(
						string.Format("No declared type could be found for generic '{0}' in scope", genericType.Signature));

				return genArg.Value;
			}

			return Model.GenericArguments[genericType];
		}

		public IMethod GetMethod(string methodSig)
		{
			var model = (Model.Type as IObjectModel);
			var methods = model.Methods.Concat(model.Implementations.SelectMany(x => (x.DeclaredType.Type as IObjectModel).Methods));

			return methods.FirstOrDefault(x => x.Signature == methodSig);
		}
	}
}