﻿using System;

namespace ModelGenerator.Domain.Exceptions
{
	public class ModelNotFoundException : Exception
	{
		public ModelNotFoundException(string modelName)
			: base(string.Format("Model with signature: '{0}' could not be found. Have you registered it?", modelName))
		{
			
		}
	}
}