﻿using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests
{
	[TestFixture]
	public class PropertyMapperTests : BaseTest<PropertyMapper>
	{
		// TODO: This could also be included as a base test
		[Test]
		public void Map_ValidDesignModel_ReturnsNonNull()
		{
			// Arrange
			var sut = Create<PropertyMapper>();

			// Act
			var result = sut.Map(new PropertyDesignModel("Name", "Type"), Mock<IScope>());

			// Assert
			Assert.IsNotNull(result);
		}

		[Test]
		public void Map_InjectTheDeclaredMapperResult()
		{
			// Arrange
			Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>()
				.Map(Arg.Any<IDeclaredDesignModel>(), Mock<IScope>())
				.Returns(new DeclaredType(new ClassModel("Model")));

			// Act
			var result = Sut.Map(new PropertyDesignModel("Name", "Model"), Mock<IScope>());

			// Assert
			Assert.AreEqual("Model", result.DeclaredType.Signature);
		}

		[Test]
		public void Map_GivenAPropertyName_ReturnPropertyWithName()
		{
			// Arrange
			var sut = Create<PropertyMapper>();

			// Act
			var result = sut.Map(new PropertyDesignModel("Name", "Type"), Mock<IScope>());

			// Assert
			Assert.AreEqual("Name", result.Name);
		}

		[Test]
		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		public void Map_GivenAttributeModels_CreateAttributes(int nAttributes)
		{
			// Arrange
			var sut = Create<PropertyMapper>();
			var model = new PropertyDesignModel("Name", "Type");
			for (int i = 0; i < nAttributes; i++)
			{
				model.Attributes.Add(new AttributeDesignModel("Attr" + i));
			}

			// Act
			var result = sut.Map(model, Mock<IScope>());

			// Assert
			for (int i = 0; i < nAttributes; i++)
			{
				Assert.AreEqual("Attr" + i, result.Attributes[i].Name);
			}
		}
	}
}