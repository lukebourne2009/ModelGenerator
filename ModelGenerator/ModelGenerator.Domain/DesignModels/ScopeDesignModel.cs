﻿namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public class ScopeDesignModel : IScopeDesignModel
	{
		public ScopeDesignModel(IDeclaredDesignModel modelSig)
			: this(modelSig, null)
		{}
		public ScopeDesignModel(IDeclaredDesignModel modelSig, string methodSig)
		{
			Model = modelSig;
			Method = Model.Signature + '.' + methodSig;
		}

		public IDeclaredDesignModel Model { get; private set; }
		public string Method { get; set; }
	}
}