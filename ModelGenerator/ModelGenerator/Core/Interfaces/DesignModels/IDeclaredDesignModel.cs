﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface IDeclaredDesignModel : ISignatured
	{
		string Type { get; set; }
		// TODO this should really be a private setter
		IDictionary<string, IDeclaredDesignModel> GenericArguments { get; set; }
	}
}