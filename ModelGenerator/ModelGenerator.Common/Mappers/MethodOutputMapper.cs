﻿using System;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models.Method;

namespace ModelGenerator.Common.Mappers
{
    public class MethodOutputMapper : BaseMapper<IMethodOutputDesignModel, IMethodOutput, IScope>
    {
	    private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredMapper;

		public MethodOutputMapper(IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredMapper)
		{
			_declaredMapper = declaredMapper;
		}

		public override IMethodOutput Map(IMethodOutputDesignModel source, IScope scope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (scope == null) throw new ArgumentNullException("scope");

			return new MethodOutput(_declaredMapper.Map(source, scope));
		}
    }
}