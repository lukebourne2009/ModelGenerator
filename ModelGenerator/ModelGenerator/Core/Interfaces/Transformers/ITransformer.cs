﻿using System;
using System.Collections.Generic;

namespace ModelGenerator.Core.Interfaces.Transformers
{
    public interface ITransformer<in TSource, out TTarget>
    {
        TTarget Transform(TSource source);
    }
}