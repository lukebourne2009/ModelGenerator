﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.Models.Method
{
    public class MethodArgument : IMethodArgument
    {
        public MethodArgument(ITypeModel model, string name)
        {
			DeclaredType = new DeclaredType(model);
            Name = name;
        }        
		public MethodArgument(IDeclaredType declaredType, string name)
		{
			DeclaredType = declaredType;
			Name = name;
        }

	    public IDeclaredType DeclaredType { get; set; }
		public string Name { get; set; }
	    public string Signature { get { return Name; } }
    }
}