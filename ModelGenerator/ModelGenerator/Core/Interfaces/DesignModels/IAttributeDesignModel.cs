﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
    public interface IAttributeDesignModel : INamed
    {}
}