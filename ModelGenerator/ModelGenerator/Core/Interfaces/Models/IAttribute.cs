﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Models
{
    public interface IAttribute : INamed
    {}
}