﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Validators;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.Validators;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Validators
{
	[TestFixture]
	public class OveridableValidatorTests : BaseTest<OveridableValidator>
	{
		[Test]
		[TestCaseSource("AssertIsCompatibleOverride_GivenAListOfExistingOveridables_AddWhenCorrectOverride_TestData")]
		public void AssertIsCompatibleOverride_GivenAListOfExistingOveridables_AddWhenCorrectOverride(
			IEnumerable<ISignatured> existing, ISignatured newOverides, Action<TestDelegate> assertor)
		{
			// Arrange
			var sut = Create<OveridableValidator>();

			// Act
			TestDelegate del = () => sut.AssertIsCompatibleOverride(existing, newOverides);

			// Assert
			assertor(del);
		}
		private static IEnumerable AssertIsCompatibleOverride_GivenAListOfExistingOveridables_AddWhenCorrectOverride_TestData()
		{
			var existing = new List<ISignatured>
			{
				new Method("Name1"),
				new Method("Name2")
			};

			var doThrow = new Action<TestDelegate>(x => Assert.Throws<InvalidOperationException>(x));
			var dontThrow = new Action<TestDelegate>(Assert.DoesNotThrow);

			yield return new TestCaseData(existing.Take(0), new Method("Name1"), dontThrow)
			{
				TestName = "AssertIsCompatibleOverride: Given no existing overidables, don't throw"
			};
			yield return new TestCaseData(existing.Take(1), new Method("Name1"), doThrow)
			{
				TestName = "AssertIsCompatibleOverride: Given 1 existing overridable, and new one matches, throw"
			};
			yield return new TestCaseData(existing.Take(1), new Method("Name2"), dontThrow)
			{
				TestName = "AssertIsCompatibleOverride: Given 1 existing overridable, and new one doesnt match, don't throw"
			};
			yield return new TestCaseData(existing.Take(2), new Method("Name2"), doThrow)
			{
				TestName = "AssertIsCompatibleOverride: Given 2 existing overridable, and new one matches, throw"
			};
			yield return new TestCaseData(existing.Take(2), new Method("Name3"), dontThrow)
			{
				TestName = "AssertIsCompatibleOverride: Given 2 existing overridable, and new one doesnt match, don't throw"
			};
		}

		[Test]
		public void AssertIsCompatibleOverride_ExceptionHasTheCorrectMessage()
		{
			// Arrange
			var sut = Create<OveridableValidator>();

			// Act
			TestDelegate del = () => sut.AssertIsCompatibleOverride(new List<ISignatured>
			{
				new Method("Name")
			}, new Method("Name"));

			// Assert
			var message = Assert.Throws<InvalidOperationException>(del).Message;
			Assert.AreEqual("Cannot add this item as an existing item exists that shares the same signature.\r\n" +
			                "Consider changing the 'Name' of the item or make it an overloaded item.", message);
		}
	}
}