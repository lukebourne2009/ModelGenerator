﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.Method.Implementations;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Implementations
{
	[TestFixture]
	public class ConstructorInjectionImplementationTests : BaseTest<ConstructorInjectionMethodImplementation>
	{
		[Test]
		[TestCaseSource("Implementation_GivenAnImplementationWith_X_Inputs_MatchThemToFieldsInModelScope_TestData")]
		public string Implementation_GivenAnImplementationWith_X_Inputs_MatchThemToFieldsInModelScope(
			IList<IMethodArgument> args,
			IList<IField> fields)
		{
			// Arrange
			var sut = Create<ConstructorInjectionMethodImplementation>();
			Mock<IScope>().Method.Inputs.Returns(args);
			Mock<IScope>().Model.Type.Returns(new ClassModel("Type")
			{
				Fields = fields
			});

			// Act
			var result = sut.Implementation;

			// Assert
			return result;
		}

		private static IEnumerable Implementation_GivenAnImplementationWith_X_Inputs_MatchThemToFieldsInModelScope_TestData()
		{
			yield return new TestCaseData(new List<IMethodArgument>(), new List<IField>())
			{
				ExpectedResult = ""
			};
			yield return new TestCaseData(new List<IMethodArgument>
			{
				new MethodArgument(new ClassModel("Type"), "arg")
			}, new List<IField>
			{
				new Field(new ClassModel("Type"), "_arg")
			})
			{
				ExpectedResult = "\r\n_arg = arg;\r\n"
			};
			yield return new TestCaseData(new List<IMethodArgument>
			{
				new MethodArgument(new ClassModel("Type"), "arg1"),
				new MethodArgument(new ClassModel("Type"), "arg2")
			}, new List<IField>
			{
				new Field(new ClassModel("Type"), "_arg1"),
				new Field(new ClassModel("Type"), "_arg2")
			})
			{
				ExpectedResult = "\r\n_arg1 = arg1;\r\n_arg2 = arg2;\r\n"
			};
		}

		[Test]
		public void Implementation_GivenInputThatIsNotAvailableAsAField_ThrowException()
		{
			// Arrange
			var sut = Create<ConstructorInjectionMethodImplementation>();
			Mock<IScope>().Model.Type.Returns(new ClassModel("Model"));
			Mock<IScope>().Method.Inputs.Returns(new List<IMethodArgument>
			{
				new MethodArgument(new ClassModel("Type"), "arg")
			});

			// Act
			TestDelegate del = () => { var result = sut.Implementation; };

			// Assert
			Assert.Throws<InvalidOperationException>(del);
		}

		[Test]
		public void Implementation_GivenTheObjectInScopeIsNotAClassModel_ThrowException()
		{
			// Arrange
			var sut = Create<ConstructorInjectionMethodImplementation>();
			Mock<IScope>().Model.Type.Returns(new InterfaceModel("Model"));
			Mock<IScope>().Method.Inputs.Returns(new List<IMethodArgument>
			{
				new MethodArgument(new ClassModel("Type"), "arg")
			});

			// Act
			TestDelegate del = () => { var result = sut.Implementation; };

			// Assert
			Assert.Throws<InvalidOperationException>(del);
		}
	}
}