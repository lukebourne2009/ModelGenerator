﻿using System;
using System.Collections.Generic;
using System.Text;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Helpers;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Helpers
{
	public class ObjectTemplateBuilder : IObjectTemplateBuilder
	{
		private readonly IGlobalObjectManager _manager;
		private readonly IObjectCreator _classCreator;
		private readonly IObjectCreator _interfaceCreator;

		public ObjectTemplateBuilder(IGlobalObjectManager manager,
			IObjectCreator classCreator,
			IObjectCreator interfaceCreator)
		{
			_manager = manager;
			_classCreator = classCreator;
			_interfaceCreator = interfaceCreator;
		}

		public IObjectModel BuildModelMapper(string modelName)
		{
			if (string.IsNullOrEmpty(modelName)) throw new ArgumentNullException("modelName", "Value cannot be null or empty.");

			var mapperModel = _manager.GetObject(string.Format("{0}ModelMapper", modelName));
			if (mapperModel != null)
				return mapperModel;

			mapperModel = new ClassModel(modelName + "ModelMapper");
			_manager.RegisterObject(mapperModel);
			var entity = modelName + "Entity";
			var model = modelName + "Model";
			var viewModel = modelName + "ViewModel";

			var iModelMapper = BuildIModelMapper();

			var implementation = new ImplementationDesignModel(iModelMapper);

			implementation.GenericArguments.Add("TEntity", new GenericArgumentDesignModel(entity));
			implementation.GenericArguments.Add("TModel", new GenericArgumentDesignModel(model));
			implementation.GenericArguments.Add("TViewModel", new GenericArgumentDesignModel(viewModel));

			var declaredModel = new DeclaredDesignModel(mapperModel.Name);

			implementation.MethodImplementations.Add("MapToViewModel.TModel",
				BuildMapperCreateImplementation(declaredModel, "MapToViewModel.TModel", viewModel, modelName));
			implementation.MethodImplementations.Add("MapToViewModel.TEntity",
				BuildMapperCreateImplementation(declaredModel, "MapToViewModel.TEntity", viewModel, modelName));
			implementation.MethodImplementations.Add("MapToModel.TEntity",
				BuildMapperCreateImplementation(declaredModel, "MapToModel.TEntity", model, modelName));
			implementation.MethodImplementations.Add("MapToModel.TViewModel",
				BuildMapperCreateImplementation(declaredModel, "MapToModel.TViewModel", model, modelName));
			implementation.MethodImplementations.Add("MapToEntity.TModel",
				BuildMapperCreateImplementation(declaredModel, "MapToEntity.TModel", entity, modelName));
			implementation.MethodImplementations.Add("MapToEntity.TViewModel",
				BuildMapperCreateImplementation(declaredModel, "MapToEntity.TViewModel", entity, modelName));


			mapperModel = _classCreator.Implement(mapperModel, implementation);
			return mapperModel;
		}
		public IObjectModel BuildIFactory(int nInputs = 0)
		{
			var iMapperModel = _manager.GetObject(ConstructGenericTemplate(nInputs, 1, "IFactory"));
			if (iMapperModel != null)
				return iMapperModel;

			var model = new InterfaceModel("IFactory");

			_interfaceCreator.SetGeneric(model, "TObject");

			var method = new MethodDesignModel("Create")
			{
				Output = new MethodOutputDesignModel("TObject")
			};

			for (int i = 0; i < nInputs; i++)
			{
				string genericType = "TSource" + (i + 1);
				_interfaceCreator.SetGeneric(model, genericType);
				method.Inputs.Add(new MethodArgumentDesignModel("source" + (i + 1), genericType));
			}

			_interfaceCreator.SetMethod(model, method);

			_manager.RegisterObject(model);
			return model;
		}
		public IObjectModel BuildIMapper(int nInputs = 0)
		{
			var iMapperModel = _manager.GetObject(ConstructGenericTemplate(nInputs, 1, "IMapper"));
			if (iMapperModel != null)
				return iMapperModel;

			iMapperModel = new InterfaceModel("IMapper");
			_interfaceCreator.SetGeneric(iMapperModel, "TTarget");

			var method = new MethodDesignModel("Map");

			for (int i = 0; i < nInputs; i++)
			{
				_interfaceCreator.SetGeneric(iMapperModel, "TSource" + (i + 1));
				method.Inputs.Add(new MethodArgumentDesignModel("source" + (i + 1), "TSource" + (i + 1)));
			}

			method.Output = new MethodOutputDesignModel("TTarget");
			_interfaceCreator.SetMethod(iMapperModel, method);

			_manager.RegisterObject(iMapperModel);

			return iMapperModel;
		}
		public IObjectModel BuildIModelMapper()
		{
			var iModelMapper = _manager.GetObject("IModelMapper<0,1,2>");
			if (iModelMapper != null)
				return iModelMapper;

			InterfaceModel model = new InterfaceModel("IModelMapper");
			_interfaceCreator.SetGeneric(model, "TEntity");
			_interfaceCreator.SetGeneric(model, "TModel");
			_interfaceCreator.SetGeneric(model, "TViewModel");
			CreateIMapperMethod(model, "MapToEntity", "TModel", "TEntity");
			CreateIMapperMethod(model, "MapToEntity", "TViewModel", "TEntity");
			CreateIMapperMethod(model, "MapToModel", "TEntity", "TModel");
			CreateIMapperMethod(model, "MapToModel", "TViewModel", "TModel");
			CreateIMapperMethod(model, "MapToViewModel", "TEntity", "TViewModel");
			CreateIMapperMethod(model, "MapToViewModel", "TModel", "TViewModel");

			_manager.RegisterObject(model);
			return model;
		}
		private IObjectModel CreateIMapperMethod(IObjectModel model, string name, string input, string output)
		{
			var methodDesignModel = new MethodDesignModel(name)
			{
				Inputs = new List<IMethodArgumentDesignModel>
				{
					new MethodArgumentDesignModel("source", input)
				},
				Output = new MethodOutputDesignModel
				{
					Type = output
				}
			};
			return _interfaceCreator.SetMethod(model, methodDesignModel);
		}
		private static string ConstructGenericTemplate(int nInputs, int outputs, string type)
		{
			StringBuilder builder = new StringBuilder(type);
			builder.Append('<');
			for (int i = 0; i < nInputs + outputs; i++)
			{
				builder.Append(i);
				if (i < nInputs)
					builder.Append(',');
			}
			builder.Append('>');
			return builder.ToString();
		}

		private static MapperCreateMethodImplementationDesignModel BuildMapperCreateImplementation(
			IDeclaredDesignModel modelScope, string methodSig, string returnObject,
			string modelSig)
		{
			return new MapperCreateMethodImplementationDesignModel(
				new ScopeDesignModel(modelScope, methodSig),
				new DeclaredDesignModel(returnObject), modelSig);
		}

	}
}