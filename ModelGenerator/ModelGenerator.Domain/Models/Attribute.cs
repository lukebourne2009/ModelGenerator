﻿using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Models
{
    public class Attribute : IAttribute
    {
        public Attribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}