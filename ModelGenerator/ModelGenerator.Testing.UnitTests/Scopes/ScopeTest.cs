﻿using System;
using System.Collections;
using System.Linq;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using NUnit.Framework;
using ModelGenerator.Domain;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Testing.UnitTests.Scopes
{
	[TestFixture]
	public class ScopeTest
	{
		[Test]
		public void GetVariables_GivenAClassWithXFields_ReturnItInScope(
			[Values(0,1,2)] int fields)
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			for (int i = 0; i < fields; i++)
			{
				model.Fields.Add(new Field(new ClassModel("int"), "name"));				
			}
			var type = new DeclaredType(model);
			var sut = new Scope(type);

			// Act
			var result = sut.GetVariables();

			// Assert
			Assert.AreEqual(fields, result.Count);
			for (int i = 0; i < fields; i++)
			{
				Assert.AreEqual(model.Fields[i], result[i]);				
			}
		}		
		
		[Test]
		public void GetVariables_GivenAFieldWithAGeneric_PassItsConcreteGenericArgument()
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			model.Generics.Add(new Generic("TGen"));
			model.Fields.Add(new Field(model.Generics[0], "name"));
			var type = new DeclaredType(model);
			type.GenericArguments.Add(model.Generics[0], new DeclaredType(new ClassModel("int")));
			var sut = new Scope(type);

			// Act
			var result = sut.GetVariables();

			// Assert
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(model.Fields[0], result[0]);
		}

		[Test]
		public void GetGenericArgument_GivenNoGenericArgument_ThrowException()
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			var type = new DeclaredType(model);
			var sut = new Scope(type);

			// Act
			TestDelegate del = () => sut.GetGenericArgument("Generic");

			// Assert
			Assert.Throws<InvalidOperationException>(del);
		}

		[Test]
		public void GetGenericArgument_GivenAMatchingGenericArgument_ButDeclaredArgumentProvided_ReturnNull()
		{
			// Arrange
			var model = new ClassModel("Type");
			model.Generics.Add(new Generic("Generic"));
			var type = new DeclaredType(model);
			var sut = new Scope(type);

			// Act
			TestDelegate del = () => sut.GetGenericArgument("Generic");

			// Assert
			Assert.Throws<InvalidOperationException>(del);
		}		
		
		[Test]
		public void GetGenericArgument_GivenAMatchingGenericArgument_AndGenericArgumentSupplie_ReturnType()
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			model.Generics.Add(new Generic("Generic"));
			var type = new DeclaredType(model);
			type.GenericArguments.Add(model.Generics[0],new DeclaredType(new ClassModel("GenArg")));
			var sut = new Scope(type);

			// Act
			var result = sut.GetGenericArgument("Generic");

			// Assert
			Assert.AreEqual(type.GenericArguments.ElementAt(0).Value, result);
		}

		[Test]
		public void GetMethod_GivenNoMatchingMethod_returnNull()
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			var type = new DeclaredType(model);
			IScope sut = new Scope(type);

			// Act
			var result = sut.GetMethod("Method");

			// Assert
			Assert.IsNull(result);
		}

		[Test]
		[TestCaseSource("GetMethod_GivenAMatchingMethod_ReturnMethod_TestData",new object[]{"model"})]
		public void GetMethod_GivenAMatchingMethod_ReturnMethod(IMethod method, string methodSig)
		{
			// Arrange
			ClassModel model = new ClassModel("Type");
			model.Methods.Add(method);
			var type = new DeclaredType(model);
			IScope sut = new Scope(type);

			// Act
			var result = sut.GetMethod(methodSig);

			// Assert
			Assert.AreEqual(method, result);
		}

		[Test]
		[TestCaseSource("GetMethod_GivenAMatchingMethod_ReturnMethod_TestData",new object[]{"implementation"})]
		public void GetMethod_GivenAMatchingMethodInImplementation_ReturnMethod(IMethod method, string methodSig)
		{
			// Arrange
			var model = new ClassModel("Type");
			model.Implementations.Add(new Implementation(new DeclaredType(new ClassModel("OtherType")
			{
				Methods = {method}
			})));
			var type = new DeclaredType(model);
			var sut = new Scope(type);

			// Act
			var result = sut.GetMethod(methodSig);

			// Assert
			Assert.AreEqual(method, result);
		}

		private static IEnumerable GetMethod_GivenAMatchingMethod_ReturnMethod_TestData(string modelOrImplementation)
		{
			yield return new TestCaseData(new Method("Method"), "Method")
			{
				TestName = string.Format("GetMethod: Given a Method exists in {0}, return it", modelOrImplementation)
			};
			yield return new TestCaseData(new Method("Method")
			{
				Inputs = {new MethodArgument(new ClassModel("int"), "arg")}
			}, "Method.int")
			{
				TestName = string.Format("GetMethod: Given a Method with an input exists in {0}, return it", modelOrImplementation)
			};
			yield return new TestCaseData(new Method("Method")
			{
				Inputs =
				{
					new MethodArgument(new ClassModel("int"), "arg"),
					new MethodArgument(new ClassModel("string"), "arg2")
				}
			}, "Method.int.string")
			{
				TestName = string.Format("GetMethod: Given a Method with 2 inputs exists in {0}, return it", modelOrImplementation)
			};
		}

	}
}