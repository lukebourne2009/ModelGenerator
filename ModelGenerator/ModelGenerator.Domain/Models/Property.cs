﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Domain.ClassModels
{
	public class Property : IProperty
    {
        public Property(string propertyName, ITypeModel type)
        {
            Name = propertyName;
	        DeclaredType = new DeclaredType(type);
	        Attributes = new List<IAttribute>();
        }

		public Property(string propertyName, IDeclaredType type)
	    {
			Name = propertyName;
		    DeclaredType = type;
		    Attributes = new List<IAttribute>();
	    }

	    public IDeclaredType DeclaredType { get; set; }
		public string Name { get; set; }
	    public IList<IAttribute> Attributes { get; set; }
	    public IPropertyMapping PropertyMapping { get; set; }
	    public string Signature
	    {
		    get { return Name; }
	    }
    }
}