﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.DesignModels
{
	public class DeclaredDesignModel : TypeSignatured<DeclaredDesignModel>, IDeclaredDesignModel
	{
		public DeclaredDesignModel(string type)
			: base(x => x.Type, x => x.GenericArguments.Select(y => y.Value))
		{
			Type = type; 
			GenericArguments = new Dictionary<string, IDeclaredDesignModel>();
		}

		public string Type { get; set; }
		public IDictionary<string, IDeclaredDesignModel> GenericArguments { get; set; }
	}
}