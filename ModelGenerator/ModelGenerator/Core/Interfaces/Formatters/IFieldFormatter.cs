﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Formatters
{
	public interface IFieldFormatter
	{
		string BuildFields(IList<IField> fields);
	}
}