﻿using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.DesignModels.MethodImplementations
{
    public class MapperCreateMethodImplementationDesignModel : IMethodImplementationDesignModel
    {
		public MapperCreateMethodImplementationDesignModel(IScopeDesignModel scope, IDeclaredDesignModel newModel, 
			string conceptModel)
		{
			Scope = scope;
            NewObject = newModel;
	        ConceptModelSignature = conceptModel;
        }

		public IDeclaredDesignModel NewObject { get; set; }
		public string ConceptModelSignature { get; set; }
	    public string MethodSignature { get; private set; }
	    public IScopeDesignModel Scope { get; private set; }
	    public IFunction Method { get; set; }
    }
}