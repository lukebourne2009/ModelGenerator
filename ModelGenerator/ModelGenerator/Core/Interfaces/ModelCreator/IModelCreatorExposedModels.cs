﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.ModelCreator
{
	public interface IModelCreatorExposedModels : IModelCreator
	{
		IModel Model { get; }
	}
}