﻿using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Helpers;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.ObjectCreator;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests
{
	[TestFixture]
	public class ViewModelPropertyMapperContextTests : BaseTest<ViewModelPropertyMapperContext>
	{

		ViewModelPropertyMapperContext CreateViewModelPropertyMapperContext(string propertyName = null,
			IModelCreatorExposedModels creator = null,
			IObjectCreator classCreator = null,
			IObjectCreator interfaceCreator = null,
			IObjectTemplateBuilder templateBuilder = null,
			IGlobalObjectManager manager = null,
			IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> propMapper = null)
		{
			return new ViewModelPropertyMapperContext(propertyName ?? "Prop",
				creator ?? Substitute.For<IModelCreatorExposedModels>(),
				classCreator ?? Substitute.For<IObjectCreator>(),
				interfaceCreator ?? Substitute.For<IObjectCreator>(),
				templateBuilder ?? Substitute.For<IObjectTemplateBuilder>(),
				manager ?? Substitute.For<IGlobalObjectManager>(),
				propMapper ?? Substitute.For<IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope>>());
		}

		#region From Model

		[Test]
		public void FromModel_SetsTheSourceAsTheModelObject()
		{
			// Arrange
			var sut = Create<ViewModelPropertyMapperContext>();

			// Act
			sut.FromModel();

			// Assert
			Assert.AreEqual(sut.Source.Type, Mock<IModelCreatorExposedModels>().Model.Model);
		}
		[Test]
		public void FromModel_ReturnsSameContext()
		{
			// Arrange
			var sut = CreateViewModelPropertyMapperContext();

			// Act
			var result = sut.FromModel();

			// Assert
			Assert.AreEqual(sut, result);
		}

		#endregion

		#region Using Properties

		[Test]
		public void UsingProperties_ReturnTheOriginalModelCreator()
		{
			// Arrange
			IModelCreatorExposedModels modelCreator = Substitute.For<IModelCreatorExposedModels>();
			modelCreator.Model.Returns(new ConceptModel("Model"));
			var sut = CreateViewModelPropertyMapperContext(creator: modelCreator);

			// Act
			var result = sut.FromModel().UsingProperties();

			// Assert
			Assert.AreEqual(modelCreator, result);
		}

		[Test]
		[TestCase(new string[0], 0)]
		[TestCase(new []{"dfh","dfh"}, 1)]
		[TestCase(new []{"ghk","lh","hj"}, 2)]
		public void UsingProperties_TargetPropertiesPropertyMappingIsMappedFromTheMapper(string[] usings, int nProps)
		{
			// Arrange
			IModelCreatorExposedModels modelCreator = Substitute.For<IModelCreatorExposedModels>();
			modelCreator.Model.Returns(new ConceptModel("Model"));
			IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> mapper = Substitute.For<IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope>>();
			var sut = CreateViewModelPropertyMapperContext(propMapper: mapper, creator: modelCreator);

			// Act
			sut.FromModel().UsingProperties(usings);

			// Assert
			mapper.Map(Arg.Is<IPropertyMappingDesignModel>(x => x.UsingProperties.Count == nProps), Arg.Any<IScope>()).Received(1);
        }

		#endregion
	}
}