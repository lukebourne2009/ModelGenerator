﻿using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
    public interface IMethodImplementationDesignModel
    {
		string MethodSignature { get; }
	    IScopeDesignModel Scope { get; }
	    IFunction Method { get; set; }
    }
}