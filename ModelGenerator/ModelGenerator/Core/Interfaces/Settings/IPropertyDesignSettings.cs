﻿namespace ModelGenerator.Core.Interfaces.Settings
{
	public interface IPropertyDesignSettings
	{
		int? Order { get; set; }
		bool Required { get; set; }
	}
}