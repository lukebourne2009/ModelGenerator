public interface IModelMapper<TEntity, TModel, TViewModel>
{
	TEntity MapToEntity(TModel source);
	TEntity MapToEntity(TViewModel source);
	TModel MapToModel(TEntity source);
	TModel MapToModel(TViewModel source);
	TViewModel MapToViewModel(TEntity source);
	TViewModel MapToViewModel(TModel source);
	
}
