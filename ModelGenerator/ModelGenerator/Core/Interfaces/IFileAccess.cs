﻿namespace ModelGenerator.Core.Interfaces
{
    public interface IFileAccess
    {
        void Write(string str);
    }
}