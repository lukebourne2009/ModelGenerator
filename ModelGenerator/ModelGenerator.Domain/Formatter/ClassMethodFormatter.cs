﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain.Models.Method;

namespace ModelGenerator.Domain.Formatter
{
    public class ClassMethodFormatter : IMethodFormatter
    {
        public string BuildMethods(IEnumerable<IMethod> methods, IList<IImplementation> implementations)
        {
            if (methods         == null) throw new ArgumentNullException("methods");
            if (implementations == null) throw new ArgumentNullException("implementations");

            var builder = new StringBuilder();

            foreach (var method in methods)
            {
                BuildMethod(builder, method, method.Implementation);
            }

			foreach (var implementation in implementations)
            {
	            foreach (var method in (implementation.DeclaredType.Type as IObjectModel).Methods)
	            {
		            if (!implementation.MethodImplementations.ContainsKey(method))
						BuildMethod(builder, method, new NotImplementedImplementation());
					else
						BuildMethod(builder, method, implementation.MethodImplementations[method]);
				}
			}

            return builder.ToString();
        }

	    private static void BuildMethod(StringBuilder builder, IMethod method, IMethodImplementation implementation)
        {
	        string arguments = CreateArguments(method);

            builder.AppendLine(string.Format(
                "public {0} {1}({2})\r\n{{\r\n{3}\r\n}}", method.Output.Type.Signature,
                method.Name, arguments, implementation.Implementation));
        }

			private static string CreateArguments(IMethod method)
			{
				var inputs = method.Inputs.ToList();
				var arguments = string.Join(", ", inputs
					.Select(x => string.Format("{0} {1}", x.DeclaredType.Signature, x.Name)));
				return arguments;
			}

    }
}