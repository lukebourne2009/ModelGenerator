﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Models
{
	// TODO: this can be a ITypeModel as the scope can resolve and generics
	public interface IVariable : INamed, ISignatured
	{
		IDeclaredType DeclaredType { get; set; }
	}
}