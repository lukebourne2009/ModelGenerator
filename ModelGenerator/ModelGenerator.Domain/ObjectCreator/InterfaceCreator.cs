﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;

namespace ModelGenerator.Domain.ObjectCreator
{
    public class InterfaceCreator : IObjectCreator
    {
        private readonly IFileCreator _fileCreator;
        private readonly IFormatter _braceFormatter;
        private readonly IGlobalObjectManager _objectManager;
		private readonly IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> _methodArgumentMapper;
		private readonly IMapper<IMethodOutputDesignModel, IMethodOutput, IScope> _methodOutputMapper;

        public InterfaceCreator(IFileCreator fileCreator, IFormatter braceFormatter,
            IGlobalObjectManager objectManager,
			IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> methodArgumentMapper,
			IMapper<IMethodOutputDesignModel, IMethodOutput, IScope> methodOutputMapper)
        {
            if (fileCreator == null) throw new ArgumentNullException("fileCreator");
            _fileCreator = fileCreator;
            _braceFormatter = braceFormatter;
            _objectManager = objectManager;
            _methodArgumentMapper = methodArgumentMapper;
            _methodOutputMapper = methodOutputMapper;
        }

        public IObjectModel CreateObject(IObjectModel modelContext)
        {
            if (modelContext == null) throw new ArgumentNullException("modelContext");
            if (modelContext.FilePath == null) throw new ArgumentNullException("FilePath");

            _objectManager.RegisterObject(modelContext);

            using (Stream fileStream =
                _fileCreator.CreateFile(
                    String.Format("{0}\\{1}.cs", modelContext.FilePath.LocalPath, modelContext.Name)))
            {
                StringBuilder build = new StringBuilder();
                BuildObjectDecleration(modelContext, build);
                build.AppendLine("{");

                BuildMethods(modelContext, build);

                build.AppendLine();
                build.AppendLine("}");

                string formatted = _braceFormatter.Format(build.ToString());
                byte[] textBytes = Encoding.ASCII.GetBytes(formatted);
                fileStream.Write(textBytes, 0, textBytes.Length);
            }

            return modelContext;
        }

	    public IObjectModel SetField(IObjectModel classModel, IFieldDesignModel fieldDesignModel)
	    {
		    throw new NotImplementedException();
	    }

	    public IObjectModel SetProperty(IObjectModel modelContext, IPropertyDesignModel model)
        {
            throw new NotImplementedException();
        }

        public IObjectModel SetMethod(IObjectModel modelContext, IMethodDesignModel model)
        {
            var distinctSet = model.Inputs.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            if (distinctSet.Count() != model.Inputs.Count)
                throw new ArgumentException("There were some duplicate names for the arguments");

	        var method = new Method(model.Name);

			var scope = new Scope(new DeclaredType(modelContext), method);
	        foreach (var generic in modelContext.Generics)
	        {
		        scope.Model.GenericArguments.Add(generic, new DeclaredType(generic));
	        }

			method.Inputs = _methodArgumentMapper.Map(model.Inputs, scope).ToList();
			method.Output = _methodOutputMapper.Map(model.Output, scope);

	        modelContext.Methods.Add(method);
            return modelContext;
        }

        public IObjectModel SetGeneric(IObjectModel model, string genericName)
        {
            if (model.Generics.All(x => x.Name != genericName))
                model.Generics.Add(new Generic(genericName));
            else
                throw new ArgumentException("Cannot Create 2 Generic Types with the same TypeName");

            return model;
        }

        public IObjectModel Implement(IObjectModel model, IImplementationDesignModel implementation)
        {
            throw new NotImplementedException();
        }

	    public IObjectModel SetConstructor(IObjectModel classModel, IConstructorDesignModel constructorDesignModel)
	    {
		    throw new NotImplementedException();
	    }

	    private void BuildMethods(IObjectModel modelContext, StringBuilder build)
        {
            foreach (IMethod method in modelContext.Methods)
            {
                var arguments = string.Join(", ", method.Inputs
                    .Select(x => string.Format("{0} {1}", x.DeclaredType.Type.Name, x.Name)));
                build.AppendLine(string.Format("{0} {1}({2});", method.Output.Type.Name, method.Name, arguments));
            }
        }

        private static void BuildObjectDecleration(IObjectModel mymodel, StringBuilder build)
        {
            var generics = string.Join(", ", mymodel.Generics.Select(x => x.Name));
            generics = string.IsNullOrEmpty(generics) ? "" : string.Format("<{0}>", generics);
            build.AppendLine(string.Format("public interface {0}{1}", mymodel.Name, generics));
        }
    }
};