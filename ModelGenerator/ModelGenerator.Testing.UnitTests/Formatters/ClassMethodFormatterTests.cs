﻿using System;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Testing.BehaviourTests;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
    [TestFixture]
    public class ClassMethodFormatterTests : BaseTest<ClassMethodFormatter>
    {
        [Test]
        public void BuildMethods_GivenNoModels_NoMethodIsFormatted()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
            var methods = new List<IMethod>();

            // Act
            var result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("", result);
        }
        [Test]
        public void BuildMethods_GivenAMethodModel_MethodIsFormatted()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Implementation = new NotImplementedImplementation(),
	                Output = new MethodOutput(new ClassModel("void"))
				}
            };

            // Act
			var result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("public void MyMethod()\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }
        [Test]
        public void BuildMethods_GivenAMethodModelWith1Argument_MethodIsFormatted()
        {
            // Arrange
            var sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Inputs = new List<IMethodArgument>
                    {
                        new MethodArgument(new ClassModel("int"),"arg1")
                    },
                    Implementation = new NotImplementedImplementation(),
					Output = new MethodOutput(new ClassModel("void"))
                }
            };

            // Act
			var result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("public void MyMethod(int arg1)\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }
        [Test]
        public void BuildMethods_GivenAMethodModelWith2Arguments_MethodIsFormatted()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Inputs = new List<IMethodArgument>
                    {
                        new MethodArgument(new ClassModel("int"),"arg1"),
                        new MethodArgument(new ClassModel("int"),"arg2")
                    },
                    Implementation = new NotImplementedImplementation(),
	                Output = new MethodOutput(new ClassModel("void"))
				}
            };

            // Act
			var result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("public void MyMethod(int arg1, int arg2)\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }        
        [Test]
        public void BuildMethods_GivenAMethodModelWithAnOutput_MethodIsFormatted()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Output = new MethodOutput(new ClassModel("int")),
                    Implementation = new NotImplementedImplementation()
				}
            };

            // Act
			var result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("public int MyMethod()\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }
        [Test]
        public void BuildMethods_Given2MethodModels_MethodsAreFormatted()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Output = new MethodOutput(new ClassModel("int")),
                    Implementation = new NotImplementedImplementation()
				},
                new Method("MyOtherMethod")
                {
                    Output = new MethodOutput(new ClassModel("int")),
                    Implementation = new NotImplementedImplementation()
				}
            };

            // Act
			string result = sut.BuildMethods(methods, new List<IImplementation>());

            // Assert
            Assert.AreEqual("public int MyMethod()\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n" +
                            "public int MyOtherMethod()\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }        
        [Test]
        public void BuildMethods_GivenAnInterfaceIsImplemented_UseImplementationFromImplementationModel()
        {
            // Arrange
            var sut = Create<ClassMethodFormatter>();
			var implementationMethods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    // TODO make it so that implementable methods eg. interface or abstract cannot be given an implementation
                    Implementation = new NotImplementedImplementation(),
					Inputs = new List<IMethodArgument>{new MethodArgument(new ClassModel("Type"), "source" )},
	                Output = new MethodOutput(new ClassModel("void"))
				}
            };
	        var implementation = new Implementation(new DeclaredType(new InterfaceModel("IType")
	        {
		        Methods = {implementationMethods[0]}
	        }))
	        {
		        MethodImplementations = new Dictionary<IMethod, IMethodImplementation>
		        {
			        {implementationMethods[0], new MapperCreateMethodImplementation(new DeclaredType(new ClassModel("model")), new ConceptModel("Model"), 
						new Scope(new DeclaredType(new ClassModel("Type")),implementationMethods[0]))}
		        }
	        };

            // Act
			string result = sut.BuildMethods(new List<IMethod>(), new[] { implementation });

            // Assert
            Assert.AreEqual("public void MyMethod(Type source)\r\n" +
                            "{\r\n" +
                            "return new model();\r\n" +
                            "}\r\n", result);
        }        
		[Test]
        public void BuildMethods_GivenValidOverloadedMethod_BuildsCorrectly()
        {
            // Arrange
            IMethodFormatter sut = Create<ClassMethodFormatter>();
			var methods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Implementation = new NotImplementedImplementation(),
	                Output = new MethodOutput(new ClassModel("void"))
				}
            };
            var implementationMethods = new List<IMethod>
            {
                new Method("MyMethod")
                {
                    Inputs = new List<IMethodArgument>
                    {
                        new MethodArgument(new ClassModel("int"), "arg1")
                    },
                    Implementation = new NotImplementedImplementation(),
	                Output = new MethodOutput(new ClassModel("void"))
				}
            };
	        var implementation = new Implementation(new DeclaredType(new InterfaceModel("IType")
	        {
		        Methods = {implementationMethods[0]}
	        }))
	        {
		        MethodImplementations = new Dictionary<IMethod, IMethodImplementation>
		        {
			        {implementationMethods[0], new NotImplementedImplementation()}
		        }
	        };

            // Act
			string result = sut.BuildMethods(methods, new[] { implementation });

            // Assert
            Assert.AreEqual("public void MyMethod()\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n" +
                            "public void MyMethod(int arg1)\r\n" +
                            "{\r\n" +
                            "throw new NotImplementedException();\r\n" +
                            "}\r\n", result);
        }

    }
}