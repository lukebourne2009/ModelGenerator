﻿using System;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;

namespace ModelGenerator.Common.Mappers
{
	public class ScopeMapper : BaseMapper<IScopeDesignModel, IScope>
	{
		private readonly IGlobalObjectManager _manager;
		private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _typeMapper;

		public ScopeMapper(IGlobalObjectManager manager,
			IMapper<IDeclaredDesignModel, IDeclaredType, IScope> typeMapper)
		{
			_manager = manager;
			_typeMapper = typeMapper;
		}

		public override IScope Map(IScopeDesignModel source)
		{
			if (source == null) throw new ArgumentNullException("source");

			var model = _typeMapper.Map(source.Model, new NoScope());
			var method = _manager.GetMethod(source.Method);

			return new Scope(model, method);
		}
	}
}