﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Helpers
{
	public interface IObjectTemplateBuilder
	{
		IObjectModel BuildModelMapper(string modelName);
		IObjectModel BuildIFactory(int nInputs = 0);
		IObjectModel BuildIMapper(int nInputs);
		IObjectModel BuildIModelMapper();
	}
}