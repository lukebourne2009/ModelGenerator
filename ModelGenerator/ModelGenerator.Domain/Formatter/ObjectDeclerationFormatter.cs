﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Formatter
{
    public class ObjectDeclerationFormatter : IObjectDeclerationFormatter
    {
        public string BuildDecleration(string objectName, IList<IImplementation> implementations)
        {
	        if (objectName == null) throw new ArgumentNullException("objectName");
	        if (implementations == null) throw new ArgumentNullException("implementations");

	        var builder = new StringBuilder();
            builder.AppendFormat("public class {0}", objectName);

            var implements = implementations.Select(x => x.DeclaredType.Signature);

            if (implementations.Count > 0)
                builder.AppendFormat(" : {0}", string.Join(", ", implements));

            return builder.AppendLine().ToString();
        }
    }
}