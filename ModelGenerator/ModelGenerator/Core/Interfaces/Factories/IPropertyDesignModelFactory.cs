﻿using Factory.Core;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Settings;

namespace ModelGenerator.Core.Interfaces.Factories
{
    public interface IPropertyDesignModelFactory : IFactory<IPropertyDesignModel, string, string, IPropertyDesignSettings>
    {}
}