﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.ClassModels
{
    public interface ITypeModel : INamed, ISignatured
    {
		IList<IGeneric> Generics { get; set; }
    }
}