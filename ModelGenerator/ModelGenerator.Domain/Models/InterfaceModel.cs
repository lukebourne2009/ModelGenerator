﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;

namespace ModelGenerator.Domain.Models
{
    public class InterfaceModel : IObjectModel
    {
	    public InterfaceModel(string name)
			: this(name, null)
	    {}
		public InterfaceModel(string name, Uri uri)
        {
            Name = name;
            FilePath = uri;
            Generics = new List<IGeneric>();
            Properties = new List<IProperty>();
            Methods = new List<IMethod>();
            Implementations = new List<IImplementation>();
        }

        public string Name { get; set; }

	    public string Signature
	    {
		    get
		    {
			    var builder = new StringBuilder(Name);
			    if (Generics.Any())
				    builder.AppendFormat("<{0}>", string.Join(", ", Generics.Select(x => x.Signature)));
			    return builder.ToString();
		    }
	    }

	    public Uri FilePath { get; set; }

	    public IList<IField> Fields { get; set; }
	    public IList<IConstructor> Constructors { get; set; }
	    public IList<IProperty> Properties { get; set; }
        public IList<IMethod> Methods { get; set; }
        public IList<IGeneric> Generics { get; set; }
	    public IList<IPropertyMapping> PropertyMappings { get; set; }
	    public IList<IImplementation> Implementations { get; set; }
    }
}