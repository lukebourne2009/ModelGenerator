﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Signatures;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Signatured
{
	[TestFixture]
	public class TypeSignaturedTests : BaseTest<TypeSignaturedTests.TestTypeSignatured>
	{
		public class TestTypeSignatured : TypeSignatured<ITypeDesignModel>
		{
			public TestTypeSignatured(Func<ITypeDesignModel, string> nameSelector, 
				Func<ITypeDesignModel, IEnumerable<ISignatured>> genericSelector) 
				: base(nameSelector, genericSelector)
			{
			}
		}

		[Test]
		public void Signature_DoesNotReturnNull()
		{
			// Arrange
			var sut = new TestTypeSignatured(x => "Name", x => new List<ISignatured>());

			// Act
			var result = sut.Signature;

			// Assert
			Assert.IsNotNull(result);
		}

		[Test]
		[TestCaseSource("Signature_GivenTheFollowingType_ReturnCorrectSignature_TestData")]
		public string Signature_GivenTheFollowingType_ReturnCorrectSignature(ITypeDesignModel type)
		{
			// Arrange
			var sut = new TestTypeSignatured(x => type.Name, x => type.GenericArguments);

			// Act
			var result = sut.Signature;

			// Assert
			return result;
		}

		public static IEnumerable Signature_GivenTheFollowingType_ReturnCorrectSignature_TestData()
		{
			ITypeDesignModel type = Substitute.For<ITypeDesignModel>();
			type.Name.Returns("Type");
			yield return new TestCaseData(type)
			{
				ExpectedResult = "Type",
				TestName = "Signature: Given no generics, return only type name"
			};

			type = Substitute.For<ITypeDesignModel>();
			type.Name.Returns("Type");
			var signatured1 = Substitute.For<IGenericArgumentDesignModel>();
			signatured1.Signature.Returns("Gen1");
			type.GenericArguments.Returns(new List<IGenericArgumentDesignModel>
			{
				signatured1
			});
			yield return new TestCaseData(type)
			{
				ExpectedResult = "Type<Gen1>",
				TestName = "Signature: Given 1 generics, return type name with generic in chevrons"
			};

			type = Substitute.For<ITypeDesignModel>();
			type.Name.Returns("Type");
			var signatured2 = Substitute.For<IGenericArgumentDesignModel>();
			signatured2.Signature.Returns("Gen2");
			type.GenericArguments.Returns(new List<IGenericArgumentDesignModel>
			{
				signatured1,
				signatured2
			});
			yield return new TestCaseData(type)
			{
				ExpectedResult = "Type<Gen1, Gen2>",
				TestName = "Signature: Given 2 generics, return type name with generics in chevrons, comma seperated"
			};
		}
	}
}