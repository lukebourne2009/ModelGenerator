﻿using Mapper.Core;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Models;
using System;
using IDeclaredTypeMapper
	= Mapper.Core.IMapper<ModelGenerator.Core.Interfaces.DesignModels.IDeclaredDesignModel, ModelGenerator.Core.Interfaces.Models.IDeclaredType, ModelGenerator.Core.Interfaces.Scopes.IScope>;

using IImplementationDictionaryMapper
	= Mapper.Core.IMapper<System.Collections.Generic.IDictionary<string, ModelGenerator.Core.Interfaces.DesignModels.IMethodImplementationDesignModel>, System.Collections.Generic.IDictionary<ModelGenerator.Core.Interfaces.Models.IMethod, ModelGenerator.Core.Interfaces.Models.Method.IMethodImplementation>, ModelGenerator.Core.Interfaces.Scopes.IScope>;

// TODO this implementationMapper might not be needed
namespace ModelGenerator.Common
{
	public class ImplementationMapper : BaseMapper<IImplementationDesignModel, IImplementation, IScope>
	{
		private readonly IImplementationDictionaryMapper _implementationMapper;
		private readonly IDeclaredTypeMapper _declaredTypeMapper;

		public ImplementationMapper(IImplementationDictionaryMapper implementationMapper,
			IDeclaredTypeMapper declaredTypeMapper)
		{
			_implementationMapper = implementationMapper;
			_declaredTypeMapper = declaredTypeMapper;
		}

		public override IImplementation Map(IImplementationDesignModel source, IScope modelScope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (modelScope == null) throw new ArgumentNullException("modelScope");

			return new Implementation(_declaredTypeMapper.Map(source, modelScope));
		}
	}
}