﻿namespace ModelGenerator.Core.Interfaces.ClassModels
{
	public interface INamed
	{
		string Name { get; set; }
	}
}