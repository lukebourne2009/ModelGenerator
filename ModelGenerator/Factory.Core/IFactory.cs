﻿namespace Factory.Core
{
    public interface IFactory<out TOut>
    {
        TOut Create();
    }
    public interface IFactory<out TOut, in TIn1>
    {
        TOut Create(TIn1 in1);
    }
    public interface IFactory<out TOut, in TIn1, in TIn2>
    {
        TOut Create(TIn1 in1, TIn2 in2);
    }
    public interface IFactory<out TOut, in TIn1, in TIn2, in TIn3>
    {
        TOut Create(TIn1 name, TIn2 type, TIn3 settings);
    }
}