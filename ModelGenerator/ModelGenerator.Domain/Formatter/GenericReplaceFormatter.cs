﻿using System;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Formatters;

namespace ModelGenerator.Domain.Formatter
{
    public class GenericReplaceFormatter : IGenericFormatter
    {
        public string Format(string inputString, IObjectModel modelInScope)
        {
            if (inputString == null) throw new ArgumentNullException("inputString");
            if (modelInScope == null) throw new ArgumentNullException("modelInScope");

            var builder = new StringBuilder(inputString);
            var generics = modelInScope.Implementations.SelectMany(x => x.DeclaredType.GenericArguments);

            foreach (var generic in generics)
            {
                builder.Replace(generic.Key.Name, generic.Value.Signature);
            }
            return builder.ToString();
        }
    }
}