﻿using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models.Method.Implementations;

namespace ModelGenerator.Domain.Models.Method
{
	public class MapperCreateMethodImplementation : MethodImplementation
	{
		private readonly IModel _conceptModel;
		private readonly IFormatter _camelCaseFormatter;
		private readonly IScope _scope;

		public MapperCreateMethodImplementation(IDeclaredType returnModel, IModel conceptModel, IScope scope)
			: base(scope.Method)
		{
			_conceptModel = conceptModel;
			ReturnModel = returnModel;
			_camelCaseFormatter = new CamelCaseFormatter();
			_scope = scope;
		}

		public IDeclaredType ReturnModel { get; private set; }

		public override string Implementation
		{
			get
			{
				var input = Method.Inputs[0].DeclaredType;
				if (Method.Inputs[0].DeclaredType.Type is IGeneric)
					input = _scope.GetGenericArgument((IGeneric) Method.Inputs[0].DeclaredType.Type);

				if (SkipsAMapper(ReturnModel.Type, input.Type))
					return BuildCascadeMap();

				return string.Format("return new {0}(){1};", ReturnModel.Signature, BuildPropertyAssignments());
			}
		}

		private string BuildCascadeMap()
		{
			return string.Format("var {0}Model = MapToModel({1});\r\n" +
			       "var {0}{2} = MapTo{2}({0}Model);\r\n" +
				   "return {0}{2};", _camelCaseFormatter.Format(_conceptModel.Name), Method.Inputs[0].Name, ReturnModel.Type.Name.Remove(0, _conceptModel.Name.Length));
		}

		private string BuildPropertyAssignments()
		{
			var model = ReturnModel.Type as IObjectModel;

			if (model.Properties.Count == 0)
				return "";

			StringBuilder builder = new StringBuilder();
			builder.AppendLine();
			builder.AppendLine("{");
			for (int i = 0; i < model.Properties.Count; i++)
			{
				builder.AppendFormat("{0}{1}\r\n", FormatMapper(model.Properties[i]),
					i + 1 == model.Properties.Count ? "" : ",");
			}
			builder.Append("}");
			return builder.ToString();
		}

		private string FormatMapper(IProperty property)
		{
			var model = ReturnModel.Type as IObjectModel;

			if (property.PropertyMapping == null)
				return string.Format("{0} = source.{0}", property.Name);

			string arguments = string.Join(", ", property.PropertyMapping.UsingProperties.Select(x => "source." + x.Name));
			string camelCase = _camelCaseFormatter.Format(model.Name);
			string method = property.PropertyMapping.UsingProperties.Any() ? "Mapper.Map" : "Factory.Create";
			return string.Format("{0} = _{1}{0}{2}({3})", property.Name, camelCase, method, arguments);
		}

		private bool SkipsAMapper(ITypeModel returnModel, ITypeModel inputModel)
		{
			return (returnModel == _conceptModel.ViewModel && inputModel == _conceptModel.Entity)
				|| (returnModel == _conceptModel.Entity && inputModel == _conceptModel.ViewModel);
		}
	}
}