﻿using System;
using System.Collections.Generic;
using System.Text;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Formatter
{
	public class FieldFormatter : IFieldFormatter
	{
		public string BuildFields(IList<IField> fields)
		{
			if (fields == null) throw new ArgumentNullException("fields");

			var builder = new StringBuilder();

			foreach (var field in fields)
			{
				builder.AppendFormat("private{0} {1} {2};\r\n", (field.Readonly ? " readonly" : ""),
					field.DeclaredType.Signature, field.Name);
			}
			if (builder.Length > 0)
				builder.AppendLine();

			return builder.ToString();
		}
	}
}