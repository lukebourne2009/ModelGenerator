﻿using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Domain.Formatter;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
    [TestFixture]
    public class BraceFormatterTests : BaseTest<BraceFormatter>
    {
        [Test]
        [TestCase("BlahString\r\n\r\nOtherString\r\n", ExpectedResult = "BlahString\r\n\r\nOtherString\r\n", 
            TestName = "Given a string with no braces, return unchanged")]
        [TestCase("", ExpectedResult = "", 
            TestName = "Given an empty string, return unchanged")]
        [TestCase("{ joe bloggs }\r\n", ExpectedResult = "{ joe bloggs }\r\n", 
            TestName = "Given a brace on the same line as its own closing brace, don't add tab")]
        [TestCase("{\r\n{ joe bloggs }\r\n}", ExpectedResult = "{\r\n\t{ joe bloggs }\r\n}\r\n", 
            TestName = "Given a brace on the same line within an enclosing brace, only tab on the enclosing brace")]
        [TestCase("{\r\nBobs {\r\njoe bloggs\r\n}\r\n}", ExpectedResult = "{\r\n\tBobs {\r\n\t\tjoe bloggs\r\n\t}\r\n}\r\n", 
            TestName = "Given the opening brace is not on its own line, still correctly tab")]
        [TestCase("{\r\nBobs\r\n {\r\njoe bloggs }\r\n}", ExpectedResult = "{\r\n\tBobs\r\n\t {\r\n\t\tjoe bloggs }\r\n}\r\n", 
            TestName = "Given the closing brace is not on its own line, still correctly tab")]
        public string Format_GivenInput_ExpectOutcomeOf(string input)
        {
            // Arrange
	        var sut = Create<BraceFormatter>();

            // Act
            var result = sut.Format(input);

            // Assert
            return result;
        }

        [Test]
        public void Format_GivenASingleBrace_NoChange()
        {
            // Arrange
            IFormatter sut = Create<BraceFormatter>();

			// Act
			var result = sut.Format("{\r\n}\r\n");

            // Assert
            Assert.AreEqual("{\r\n}\r\n", result);
        }
        [Test]
        public void Format_GivenATwoBracesNextToEachOther_NoChange()
        {
            // Arrange
            IFormatter sut = Create<BraceFormatter>();

			// Act
			var result = sut.Format("{\r\n}\r\n{\r\n}\r\n");

            // Assert
            Assert.AreEqual("{\r\n}\r\n{\r\n}\r\n", result);
        }
        [Test]
        public void Format_GivenNestedBraces_Add1LevelOfTab()
        {
            // Arrange
            IFormatter sut = Create<BraceFormatter>();

			// Act
			var result = sut.Format("{\r\n{\r\n}\r\n}\r\n");

            // Assert
            Assert.AreEqual("{\r\n" +
                            "\t{\r\n" +
                            "\t}\r\n" +
                            "}\r\n", result);
        }
        [Test]
        public void Format_GivenNestedBracesWithStuffInside_Add1LevelOfTab()
        {
            // Arrange
            IFormatter sut = Create<BraceFormatter>();

			// Act
			var result = sut.Format("{\r\n{\r\npublic doing something\r\n}\r\n}\r\n");

            // Assert
            Assert.AreEqual("{\r\n" +
                            "\t{\r\n" +
                            "\t\tpublic doing something\r\n" +
                            "\t}\r\n" +
                            "}\r\n", result);
        }
        [Test]
        public void Format_GivenBracesStartingAndEndingOnSameLine_ThenTheyAreNotTouched()
        {
            // Arrange
            IFormatter sut = Create<BraceFormatter>();

			// Act
			var result = sut.Format("{\r\n{ public doing something }\r\n}\r\n");

            // Assert
            Assert.AreEqual("{\r\n" +
                            "\t{ public doing something }\r\n" +
                            "}\r\n", result);
        }
    }
}