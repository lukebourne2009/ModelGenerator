﻿using ModelGenerator.Core.Interfaces.ModelCreator;

namespace ModelGenerator.Core.Interfaces
{
    public interface IPropertyMapperContext
    {
        IModelCreator UsingProperties(params string[] properties);
    }
}