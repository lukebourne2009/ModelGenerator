﻿using System.Collections.Generic;

namespace ModelGenerator.Core.Interfaces.Models.Mapping
{
	public interface IPropertyMapping
	{
        IDeclaredType SourceModel { get; set; }
		IList<IProperty> UsingProperties { get; set; }
	}
}