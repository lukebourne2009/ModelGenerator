﻿using ModelGenerator.Core.Enums;
using ModelGenerator.Core.Interfaces.Settings;

namespace ModelGenerator.Domain.Settings
{
    public class GlobalSettings : IGlobalSettings
    {
        public EntityIdType DefaultEntityIdType { get; set; }
    }
}