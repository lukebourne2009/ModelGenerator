﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Core.Interfaces.Validators;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;

namespace ModelGenerator.Domain.ObjectCreator
{
    public class ClassCreator : IObjectCreator
    {
		#region private fields

		private readonly IFileCreator _fileCreator;
        private readonly IGlobalObjectManager _objectManager;
	    private readonly IFieldFormatter _fieldFormatter;
	    private readonly IConstructorFormattor _constructorFormatter;
	    private readonly IMethodFormatter _methodFormatter;
        private readonly IPropertyFormatter _propertyFormatter;
        private readonly IObjectDeclerationFormatter _declerationFormatter;
        private readonly IFormatter _braceFormatter;
        private readonly IGenericFormatter _genericFormatter;
	    private readonly IOveridableValidator _overidableValidator;
	    private readonly IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> _methodArgumentMapper;
		private readonly IMapper<IMethodOutputDesignModel, IMethodOutput, IScope> _methodOutputMapper;
	    private readonly IMapper<IImplementationDesignModel, IImplementation, IScope> _implementationMapper;
	    private readonly IMapper<IConstructorDesignModel, IConstructor, IScope> _constructorMapper;
	    private readonly IMapper<IPropertyDesignModel, IProperty, IScope> _propertyMapper;
		private readonly IMapper<IFieldDesignModel, IField, IScope> _fieldMapper;
	    private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredTypeMapper;
	    private readonly IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope> _methodImplementationDictionaryMapper;
	    private readonly IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> _methodImplementationMapper;

	    #endregion

	    public ClassCreator(IFileCreator fileCreator, IGlobalObjectManager objectManager, IFieldFormatter fieldFormatter,
		    IConstructorFormattor constructorFormatter, IMethodFormatter methodFormatter,
		    IPropertyFormatter propertyFormatter, IObjectDeclerationFormatter declerationFormatter, IFormatter braceFormatter,
		    IGenericFormatter genericFormatter, IOveridableValidator overidableValidator,
		    IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> methodArgumentMapper,
		    IMapper<IMethodOutputDesignModel, IMethodOutput, IScope> methodOutputMapper,
		    IMapper<IImplementationDesignModel, IImplementation, IScope> implementationMapper,
		    IMapper<IConstructorDesignModel, IConstructor, IScope> constructorMapper,
		    IMapper<IPropertyDesignModel, IProperty, IScope> propertyMapper,
		    IMapper<IFieldDesignModel, IField, IScope> fieldMapper,
		    IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredTypeMapper,
		    IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope> methodImplementationDictionaryMapper,
		    IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> methodImplementationMapper)
	    {
		    _fileCreator = fileCreator;
		    _objectManager = objectManager;
		    _fieldFormatter = fieldFormatter;
		    _constructorFormatter = constructorFormatter;
		    _methodFormatter = methodFormatter;
		    _propertyFormatter = propertyFormatter;
		    _declerationFormatter = declerationFormatter;
		    _braceFormatter = braceFormatter;
		    _genericFormatter = genericFormatter;
		    _overidableValidator = overidableValidator;
		    _methodArgumentMapper = methodArgumentMapper;
		    _methodOutputMapper = methodOutputMapper;
		    _implementationMapper = implementationMapper;
		    _constructorMapper = constructorMapper;
		    _propertyMapper = propertyMapper;
		    _fieldMapper = fieldMapper;
		    _declaredTypeMapper = declaredTypeMapper;
		    _methodImplementationDictionaryMapper = methodImplementationDictionaryMapper;
		    _methodImplementationMapper = methodImplementationMapper;
	    }

        public IObjectModel CreateObject(IObjectModel model)
        {
	        if (model == null) throw new ArgumentNullException("model");

	        _objectManager.RegisterObject(model);

            using (Stream fileStream =
                _fileCreator.CreateFile(string.Format("{0}\\{1}.cs", model.FilePath.LocalPath, model.Name)))
            {
                StringBuilder build = new StringBuilder();
                build.Append(_declerationFormatter.BuildDecleration(model.Name, model.Implementations));
                build.AppendLine("{");

	            build.Append(_fieldFormatter.BuildFields(model.Fields));
	            build.Append(_constructorFormatter.BuildConstructors(model.Constructors));
                build.Append(_propertyFormatter.BuildProperties(model.Properties, model.Implementations
                    .SelectMany(x => ((IObjectModel)x.DeclaredType.Type).Properties)));
	            build.Append(_methodFormatter.BuildMethods(model.Methods, model.Implementations));

                build.AppendLine();
                build.AppendLine("}");

                string formatted = _braceFormatter.Format(_genericFormatter.Format(build.ToString(), model));

                byte[] textBytes = Encoding.ASCII.GetBytes(formatted);
                fileStream.Write(textBytes, 0, textBytes.Length);
            }

            return model;
        }
	    public IObjectModel SetField(IObjectModel model, IFieldDesignModel fieldDesignModel)
	    {
		    if (model            == null) throw new ArgumentNullException("model");
		    if (fieldDesignModel == null) throw new ArgumentNullException("fieldDesignModel");
		    model.Fields.Add(SetRequest(model, model.Fields, fieldDesignModel, _fieldMapper));
		    return model;
	    }

	    public IObjectModel SetProperty(IObjectModel model, IPropertyDesignModel propModel)
        {
	        if (model     == null) throw new ArgumentNullException("model");
	        if (propModel == null) throw new ArgumentNullException("propModel");

			model.Properties.Insert(propModel.Settings.Order ?? model.Properties.Count, 
				SetRequest(model, model.Properties, propModel, _propertyMapper));
            return model;
        }

        public IObjectModel SetMethod(IObjectModel model, IMethodDesignModel methodModel)
        {
	        if (model       == null) throw new ArgumentNullException("model");
	        if (methodModel == null) throw new ArgumentNullException("methodModel");

	        _overidableValidator.AssertIsCompatibleOverride(model.Methods, methodModel);

	        var method    = new Method(methodModel.Name);
			var scope     = new Scope(new DeclaredType(model), method);
			method.Inputs = _methodArgumentMapper.Map(methodModel.Inputs, scope).ToList();
			method.Output = _methodOutputMapper.Map(methodModel.Output, scope);

	        model.Methods.Add(method);
            return model;
        }

	    public IObjectModel SetGeneric(IObjectModel model, string genericName)
	    {
		    if (model       == null) throw new ArgumentNullException("model");
		    if (genericName == null) throw new ArgumentNullException("genericName");

		    throw new NotImplementedException();
	    }

        public IObjectModel Implement(IObjectModel model, IImplementationDesignModel implementDesign)
        {
	        if (model           == null) throw new ArgumentNullException("model");
	        if (implementDesign == null) throw new ArgumentNullException("implementDesign");

			_overidableValidator.AssertIsCompatibleOverride(
				model.Implementations.Select(x => x.DeclaredType), implementDesign);

			var scope     = new Scope(new DeclaredType(model));
	        var implementation = _implementationMapper.Map(implementDesign, scope);
	        model.Implementations.Add(implementation);

	        implementation.MethodImplementations =
		        _methodImplementationDictionaryMapper.Map(implementDesign.MethodImplementations, scope);

			return model;
        }

	    public IObjectModel SetConstructor(IObjectModel model, IConstructorDesignModel constructorDesignModel)
	    {
		    if (model                  == null) throw new ArgumentNullException("model");
		    if (constructorDesignModel == null) throw new ArgumentNullException("constructorDesignModel");

		    var constructor = SetRequest(model, model.Constructors, constructorDesignModel, _constructorMapper);
		    model.Constructors.Add(constructor);

			var scope = new Scope(new DeclaredType(model), constructor);
		    constructor.Implementation = _methodImplementationMapper.Map(constructorDesignModel.Implementation, scope);

			return model;
	    }


	    private TOut SetRequest<TOut, TIn>(IObjectModel model, IList<TOut> existingModels,
		    TIn designModel, IMapper<TIn, TOut, IScope> mapper)
		    where TOut : class, ISignatured
		    where TIn : class, ISignatured
	    {
		    _overidableValidator.AssertIsCompatibleOverride(existingModels, designModel);

		    return mapper.Map(designModel, new Scope(new DeclaredType(model)));
	    }
	}
}