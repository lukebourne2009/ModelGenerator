﻿namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface IFieldDesignModel : IVariableDesignModel
	{
		bool Readonly { get; set; }
	}
}