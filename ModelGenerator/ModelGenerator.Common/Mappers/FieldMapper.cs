﻿using System;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Common.Mappers
{
	public class FieldMapper : BaseMapper<IFieldDesignModel, IField, IScope>
	{
		private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredTypeMapper;

		public FieldMapper(IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredTypeMapper)
		{
			_declaredTypeMapper = declaredTypeMapper;
		}

		public override IField Map(IFieldDesignModel source, IScope modelScope)
		{
		    if (modelScope == null) throw new ArgumentNullException("modelScope");
			if (source == null) throw new ArgumentNullException("source");

		    var declaredType = _declaredTypeMapper.Map(source, modelScope);
            var field = new Field(declaredType, source.Name)
			{
                Readonly = source.Readonly
			};

		    return field;
		}
	}
}