﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
    public interface IMethodDesignModel : INamed, ISignatured
    {
        IList<IMethodArgumentDesignModel> Inputs { get; set; }
        IMethodOutputDesignModel Output { get; set; }
        IMethodImplementationDesignModel MethodImplementation { get; set; }
    }
}