﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Settings;
using ModelGenerator.Core.Settings;

namespace ModelGenerator.Domain.DesignModels
{
    public class PropertyDesignModel : DeclaredDesignModel, IPropertyDesignModel
    {
        public PropertyDesignModel(string name, string type)
			: base(type)
        {
            Name = name;
            Attributes = new List<IAttributeDesignModel>();
            Settings = PropertyDesignSettings.Default;
        }
	    public string Name { get; set; }
        public IList<IAttributeDesignModel> Attributes { get; set; }
        public IPropertyDesignSettings Settings { get; set; }
    }
}