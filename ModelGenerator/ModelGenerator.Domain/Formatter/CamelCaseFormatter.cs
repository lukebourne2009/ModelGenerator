﻿using System;
using ModelGenerator.Core.Interfaces.Formatters;

namespace ModelGenerator.Domain.Formatter
{
	public class CamelCaseFormatter : IFormatter
	{
		public string Format(string str)
		{
			if (str == null) throw new ArgumentNullException("str");
			if (str == string.Empty) throw new ArgumentException("Value cannot be empty.", "str");

			return char.ToLowerInvariant(str[0]) + str.Substring(1);
		}
	}
}