﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.Models
{
    public class InterfaceModel : TypeSignatured<InterfaceModel>, IObjectModel
    {
	    public InterfaceModel(string name)
			: this(name, null)
	    {}
		public InterfaceModel(string name, Uri uri)
			:base(x => x.Name, x => x.Generics)
        {
            Name = name;
            FilePath = uri;
            Generics = new List<IGeneric>();
            Properties = new List<IProperty>();
            Methods = new List<IMethod>();
            Implementations = new List<IImplementation>();
        }

        public string Name { get; set; }
	    public Uri FilePath { get; set; }
	    public IList<IField> Fields { get; set; }
	    public IList<IConstructor> Constructors { get; set; }
	    public IList<IProperty> Properties { get; set; }
        public IList<IMethod> Methods { get; set; }
        public IList<IGeneric> Generics { get; set; }
	    public IList<IImplementation> Implementations { get; set; }
    }
}