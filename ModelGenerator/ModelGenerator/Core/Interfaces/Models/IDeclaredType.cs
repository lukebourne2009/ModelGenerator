﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Models
{
	public interface IDeclaredType : ISignatured
	{
		ITypeModel Type { get; set; }
		IDictionary<IGeneric, IDeclaredType> GenericArguments { get; set; }
	}
}