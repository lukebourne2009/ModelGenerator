﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Models
{
	public class DeclaredTypeTests : BaseTest<DeclaredType>
	{
		[Test]
		public void Signature_GivenATypeWithNoGenerics_TypeNameIsReturned()
		{
			// Arrange 
			var sut = Create<DeclaredType>();
			Mock<ITypeModel>().Name.Returns("Type");

			// Act
			var result = sut.Signature;

			// Assert
			Assert.AreEqual("Type", result);
		}

		[Test]
		[TestCaseSource("Signature_GivenATypeWithSomeGenerics_AndMatchingGenericArguments_ReturnDeclaredName_TestData")]
		public string Signature_GivenATypeWithSomeGenerics_AndMatchingGenericArguments_ReturnDeclaredName(
			List<IGeneric> generics,
			IDictionary<IGeneric, IDeclaredType> genArgs)
		{           
			// Arrange 
			var sut = Create<DeclaredType>();
			Mock<ITypeModel>().Name.Returns("Type");
			Mock<ITypeModel>().Generics.Returns(generics);
			sut.GenericArguments = genArgs;

			// Act
			var result = sut.Signature;

			// Assert
			return result;
		}
		private static IEnumerable Signature_GivenATypeWithSomeGenerics_AndMatchingGenericArguments_ReturnDeclaredName_TestData()
		{
			var generic = new Generic("TGen");
			yield return new TestCaseData(new List<IGeneric>
			{
				generic
			},new Dictionary<IGeneric, IDeclaredType>
			{
				{generic, new GenericArgument(new ClassModel("Obj")) }
			})
			{
				ExpectedResult = "Type<Obj>"
			};

			var generic2 = new Generic("TGen");
			yield return new TestCaseData(new List<IGeneric>
			{
				generic, generic2
			},new Dictionary<IGeneric, IDeclaredType>
			{
				{generic, new GenericArgument(new ClassModel("Obj1")) },
				{generic2, new GenericArgument(new ClassModel("Obj2")) }
			})
			{
				ExpectedResult = "Type<Obj1, Obj2>"
			};
		}

		[Test]
		public void Signature_GivenATypeWithSomeGenerics_AndAMissingGenericArgument_ThrowException()
		{
			// Arrange 
			var sut = Create<DeclaredType>();
			Mock<ITypeModel>().Name.Returns("Type");
			Mock<ITypeModel>().Generics.Returns(new List<IGeneric>
			{
				new Generic("TGen")
			});

			// Act
			TestDelegate del = () =>
			{
				var result = sut.Signature;
			};

			// Assert
			Assert.Throws<InvalidOperationException>(del);
		}
	}
}