﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Core.Interfaces.Models
{
    public interface IImplementation
    {
	    IDeclaredType DeclaredType { get; set; }
		IDictionary<IMethod, IMethodImplementation> MethodImplementations { get; set; }
    }
}