﻿using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.Models.Method.Implementations
{
	public abstract class MethodImplementation : IMethodImplementation
	{
		protected MethodImplementation(IFunction method)
		{
			Method = method;
		}

		public IFunction Method { get; private set; }

		public abstract string Implementation { get; }
	}
}