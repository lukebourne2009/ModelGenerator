﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models.Mapping;

namespace ModelGenerator.Core.Interfaces.Models
{
    public interface IProperty : IVariable
    {
        IList<IAttribute> Attributes { get; set; }
	    IPropertyMapping PropertyMapping { get; set; }
    }
}