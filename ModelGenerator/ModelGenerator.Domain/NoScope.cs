﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;

namespace ModelGenerator.Domain
{
	public class NoScope : IScope
	{
		public IDeclaredType Model { get; private set; }
		public IMethod Method { get; private set; }
		public IList<IVariable> GetVariables()
		{
			return new List<IVariable>();
		}
		public IGeneric GetGeneric(string name)
		{
			return null;
		}
		public IDeclaredType GetGenericArgument(string genericType)
		{
			return null;
		}
		public IDeclaredType GetGenericArgument(IGeneric genericType)
		{
			return null;
		}
		public IMethod GetMethod(string methodSig)
		{
			return null;
		}
	}
}