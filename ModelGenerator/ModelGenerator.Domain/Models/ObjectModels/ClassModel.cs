﻿using System;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.Models.ObjectModels
{
    public class ClassModel : TypeSignatured<ITypeModel>, IObjectModel
    {
        public ClassModel(string name)
            : this(name, null)
        {}

        public ClassModel(string name, Uri uri)
			:base(x => x.Name, x => x.Generics)
        {
            Name = name;
            FilePath = uri;
            Properties = new List<IProperty>();
            Methods = new List<IMethod>();
            Generics = new List<IGeneric>();
            Implementations = new List<IImplementation>();
			Constructors = new List<IConstructor>();
			Fields = new List<IField>();
        }

        public string Name { get; set; }
	    public Uri FilePath { get; set; }
	    public IList<IField> Fields { get; set; }
	    public IList<IConstructor> Constructors { get; set; }
	    public IList<IProperty> Properties { get; set; }
        public IList<IMethod> Methods { get; set; }
        public IList<IGeneric> Generics { get; set; }
	    public IList<IImplementation> Implementations { get; set; }
    }
}