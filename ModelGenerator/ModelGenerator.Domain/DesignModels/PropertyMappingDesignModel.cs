﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.DesignModels
{
	public class PropertyMappingDesignModel : IPropertyMappingDesignModel
	{
		public PropertyMappingDesignModel(string targetProperty, IDeclaredDesignModel sourceDesign)
		{
			TargetProperty = targetProperty;
			SourceDesign = sourceDesign;
			UsingProperties = new List<string>();
		}
		public PropertyMappingDesignModel(string targetProperty, IDeclaredType source)
		{
			TargetProperty = targetProperty;
			Source = source;
			UsingProperties = new List<string>();
		}

		public IDeclaredType Source { get; set; }
		public IDeclaredDesignModel SourceDesign { get; set; }
		public string TargetProperty { get; set; }
		public IList<string> UsingProperties { get; set; }
	}
}