﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels
{
    public class ImplementationDesignModel : DeclaredDesignModel, IImplementationDesignModel
    {
        public ImplementationDesignModel(string interfaceName)
            : base(interfaceName)
        {
			MethodImplementations = new Dictionary<string, IMethodImplementationDesignModel>();
        }
        public ImplementationDesignModel(IObjectModel model)
            : this(model.Name)
        {
            Model = model;
        }

        public IObjectModel Model { get; set; }
        public Dictionary<string, IMethodImplementationDesignModel> MethodImplementations { get; set; }
    }
}