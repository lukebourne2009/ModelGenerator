﻿using System;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Factories;
using ModelGenerator.Core.Interfaces.Settings;
using ModelGenerator.Domain.DesignModels;

namespace ModelGenerator.Common.Factories
{
    public class PropertyDesignModelFactory : IPropertyDesignModelFactory
    {
        public IPropertyDesignModel Create(string name, string type, IPropertyDesignSettings settings)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (type == null) throw new ArgumentNullException("type");
            if (settings == null) throw new ArgumentNullException("settings");

            PropertyDesignModel propertyDesignModel = new PropertyDesignModel(name, type);
            if(settings.Required)
                propertyDesignModel.Attributes.Add(new AttributeDesignModel("Required"));
            return propertyDesignModel;
        }
    }
}