﻿namespace ModelGenerator.Core.Enums
{
    public enum ObjectType
    {
        None,
        ViewModel,
        Model,
        Entity
    }
}