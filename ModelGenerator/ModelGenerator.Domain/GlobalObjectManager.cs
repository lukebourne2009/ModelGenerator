﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Exceptions;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Domain
{
    public class GlobalObjectManager : IGlobalObjectManager
    {
        public GlobalObjectManager()
        {
            ObjectModels = new Dictionary<string, IObjectModel>();
            FileModels = new Dictionary<string, IObjectModel>();
			Models = new Dictionary<string, IModel>();

			ObjectModels.Add("int", new ClassModel("int"));
			ObjectModels.Add("string", new ClassModel("string"));
        }

		private IDictionary<string, IModel> Models { get; set; }
        private IDictionary<string, IObjectModel> ObjectModels { get; set; }
        private IDictionary<string, IObjectModel> FileModels { get; set; }

        public void RegisterObject(IObjectModel model)
        {
	        if (model == null) throw new ArgumentNullException("model");

	        var builder = new StringBuilder(model.Name);
	        if (model.Generics.Any())
	        {
		        builder.Append('<');
		        for (int i = 0; i < model.Generics.Count; i++)
		        {
			        builder.Append(i);
			        if (i + 1 < model.Generics.Count)
				        builder.Append(",");
		        }
		        builder.Append('>');
	        }
	        var objectSignature = builder.ToString();

	        if (ObjectModels.ContainsKey(objectSignature))
            {
                if(ObjectModels[objectSignature] != model)
                    throw new ArgumentException("You cannot register a new model with the same signature");
            }
            else
            {
                ObjectModels.Add(objectSignature, model);
            }
        }
        public IObjectModel GetObject(string name)
        {
	        if (name == null) throw new ArgumentNullException("name");

	        StringBuilder builder = new StringBuilder(name);

	        var generics = new Regex("<(.*)>").Match(name).Groups;
	        if (generics.Count > 1)
	        {
		        builder.Replace(generics[0].Value, "");

		        var rawGenerics = Regex.Replace(generics[1].Value, "<(.*)>", "").Replace(" ","");
		        var nGenerics = rawGenerics.Split(',').Length;

		        builder.Append('<');
		        for (int i = 0; i < nGenerics; i++)
		        {
			        builder.Append(i);
			        if (i + 1 < nGenerics)
				        builder.Append(',');
		        }
		        builder.Append('>');
			}
	        name = builder.ToString();

	        if (!ObjectModels.ContainsKey(name)) return null;
            return ObjectModels[name];
        }
	    public IProperty TryGetProperty(string prop)
	    {
		    if (prop == null) throw new ArgumentNullException("prop");

		    var sigParts = ValidatePropertySignature(prop);
		    IObjectModel model = GetObject(sigParts[0]);
		    if (model == null) throw new ArgumentException(string.Format("Model: {0} does not yet exist",sigParts[0]));

		    IProperty property = model.Properties.FirstOrDefault(x => x.Name == sigParts[1]);

		    return property;
	    }
	    public IProperty GetProperty(string prop)
	    {
		    if (prop == null) throw new ArgumentNullException("prop");

		    IProperty property;
		    if ((property = TryGetProperty(prop)) == null)
			    throw new PropertyNotFoundException(prop);

		    return property;
		}
	    public IMethod TryGetMethod(string methodSig)
	    {
		    if (methodSig == null) throw new ArgumentNullException("methodSig");

		    var sigParts = ValidatePropertySignature(methodSig);
		    IObjectModel model = GetObject(sigParts[0]);

		    if (model == null) throw new ArgumentException(string.Format("Model: {0} does not yet exist", sigParts[0]));

			var method = model.Methods.Concat(model.Implementations.SelectMany(x => (x.DeclaredType.Type as IObjectModel).Methods))
				.Concat(model.Constructors)
				.FirstOrDefault(x => x.Signature == methodSig.Substring(methodSig.IndexOf(".", StringComparison.Ordinal) + 1));

			if(method == null)
				return model.Constructors.FirstOrDefault(x => x.Signature == methodSig.Substring(methodSig.IndexOf(".", StringComparison.Ordinal) + 1));

		    return method;
	    }

	    public IMethod GetMethod(string methodSig)
	    {
		    if (methodSig == null) throw new ArgumentNullException("methodSig");

		    IMethod method;
		    if ((method = TryGetMethod(methodSig)) == null)
			    throw new MethodNotFoundException(methodSig);

		    return method;
	    }

	    public IModel GetModel(string modelSig)
	    {
		    var model = TryGetModel(modelSig);

		    if (model == null)
			    throw new ModelNotFoundException(modelSig);

		    return model;
	    }

	    private string[] ValidatePropertySignature(string input)
	    {
		    var strings = input.Split('.');
		    if(strings.Length < 2)
				throw new FormatException(string.Format("Malformed property Signature format: {0}", input));

		    return strings;
	    }

	    public IModel TryGetModel(string modelName)
	    {
		    if (modelName == null) throw new ArgumentNullException("modelName");

		    if (!Regex.IsMatch(modelName, "^\\w+$"))
			    throw new FormatException(string.Format(
				    "the string you searched for: '{0}' did not fit the required regex pattern of: ^\\w+$", modelName));

		    if (Models.ContainsKey(modelName))
			    return Models[modelName];

		    return null;
	    }

	    public void RegisterModel(IModel model)
	    {
		    if (model == null) throw new ArgumentNullException("model");

		    if (Models.ContainsKey(model.Name) && Models[model.Name] != model)
			    throw new ArgumentException("You cannot register a new model with the same signature");

		    Models.Add(model.Name, model);
	    }
    }
}