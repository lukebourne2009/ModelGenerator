﻿using System;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models.Mapping;

namespace ModelGenerator.Common.Mappers
{
	public class PropertyMappingMapper : BaseMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope>
	{
		private readonly IGlobalObjectManager _manager;
		private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredMapper;

		public PropertyMappingMapper(IGlobalObjectManager manager,
			IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredMapper)
		{
			_manager = manager;
			_declaredMapper = declaredMapper;
		}
		
		public override IPropertyMapping Map(IPropertyMappingDesignModel source, IScope scope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (scope == null) throw new ArgumentNullException("scope");

			PropertyMapping propertyMapping = new PropertyMapping(source.Source ?? _declaredMapper.Map(source.SourceDesign, scope));

			foreach (string propertySignature in source.UsingProperties)
			{
				IProperty property = _manager.GetProperty(propertySignature);
				propertyMapping.UsingProperties.Add(property);
			}

			return propertyMapping;
		}
	}
}