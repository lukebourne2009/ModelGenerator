﻿using System;
using System.Linq;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Helpers;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Mapping;

namespace ModelGenerator.Domain.ObjectCreator
{
	public class ViewModelPropertyMapperContext : IViewModelPropertyMapperContext
	{
		private readonly string _targetProperty;
		private readonly IModelCreatorExposedModels _modelCreator;
		private readonly IObjectCreator _classCreator;
		private readonly IObjectCreator _interfaceCreator;
		private readonly IObjectTemplateBuilder _templateBuilder;
		private readonly IGlobalObjectManager _manager;
		private readonly IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> _mappingMapper;

		public ViewModelPropertyMapperContext(
			string targetProperty,
			IModelCreatorExposedModels modelCreator,
			IObjectCreator classCreator,
			IObjectCreator interfaceCreator,
			IObjectTemplateBuilder templateBuilder,
			IGlobalObjectManager manager,
			IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> mappingMapper)
		{
			_targetProperty = targetProperty;
			_modelCreator = modelCreator;
			_classCreator = classCreator;
			_interfaceCreator = interfaceCreator;
			_templateBuilder = templateBuilder;
			_manager = manager;
			_mappingMapper = mappingMapper;
		}

		public IDeclaredType Source { get; private set; }
		public string TargetProperty { get { return _targetProperty; } }

		public IModelCreator UsingProperties(params string[] properties)
		{
			if (properties == null) throw new ArgumentNullException("properties");

			var modelMapperModel = _templateBuilder.BuildModelMapper(_modelCreator.ModelName);
			var iMapper          = _templateBuilder.BuildIMapper(properties.Length);

			var targetProp = _manager.GetProperty(BuildPropertySignature(TargetProperty));
			var inputs     = properties.Select(x => _manager.GetProperty(BuildPropertySignature(x))).ToList();

			var constructor = new ConstructorDesignModel(modelMapperModel.Name);
			var argName = char.ToLower(_modelCreator.Model.ViewModel.Name[0]) + _modelCreator.Model.ViewModel.Name.Substring(1) +
			              targetProp.Name + "Mapper";
			constructor.Inputs.Add(new MethodArgumentDesignModel(argName, iMapper.Name));
			constructor.Inputs[0].GenericArguments.Add("TTarget", 
				new GenericArgumentDesignModel(targetProp.DeclaredType.Signature));
			for (int i = 0; i < properties.Length; i++)
			{
				constructor.Inputs[0].GenericArguments.Add("TSource" + (i + 1), 
					new GenericArgumentDesignModel(inputs[i].DeclaredType.Signature));
			}
			constructor.Implementation = new ConstructorInjectionMethodImplementationDesignModel(
				new ScopeDesignModel(new DeclaredDesignModel(modelMapperModel.Name), modelMapperModel.Name), 
				modelMapperModel.Name);

			var field = new FieldDesignModel(iMapper.Name, "_" + argName)
			{
				GenericArguments = constructor.Inputs[0].GenericArguments,
				Readonly = true
			};

			_classCreator.SetField(modelMapperModel, field);
			_classCreator.SetConstructor(modelMapperModel, constructor);

			targetProp.PropertyMapping = new PropertyMapping(Source)
			{
				UsingProperties = inputs
			};

			return _modelCreator;
        }

        public IPropertyMapperContext FromModel()
		{
			Source = new DeclaredType(_modelCreator.Model.Model);
			return this;
		}

		private string BuildPropertySignature(string targetProperty)
		{
			return string.Format("{0}.{1}", _modelCreator.Model.ViewModel.Signature, targetProperty);
		}
	}
}