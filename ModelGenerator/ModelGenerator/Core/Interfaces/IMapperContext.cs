﻿namespace ModelGenerator.Core.Interfaces
{
    public interface IViewModelPropertyMapperContext : IPropertyMapperContext
    {
        IPropertyMapperContext FromModel();
    }
}