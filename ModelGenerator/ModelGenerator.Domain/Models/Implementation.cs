﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Domain.ClassModels
{
    public class Implementation : IImplementation
    {
	    public Implementation()
	    {
		    MethodImplementations = new Dictionary<IMethod, IMethodImplementation>();
		}
		public Implementation(IDeclaredType declaredType)
	    {
		    DeclaredType = declaredType;
		    MethodImplementations = new Dictionary<IMethod, IMethodImplementation>();
	    }

	    public IDeclaredType DeclaredType { get; set; }
	    public IDictionary<IMethod, IMethodImplementation> MethodImplementations { get; set; }
    }
}