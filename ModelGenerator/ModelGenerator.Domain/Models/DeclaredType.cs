﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.Models
{
	public class DeclaredType : IDeclaredType
	{
		public DeclaredType(ITypeModel type)
		{
			Type = type;
			GenericArguments = new Dictionary<IGeneric, IDeclaredType>();
		}

		public DeclaredType(IDeclaredType declaredType)
		{
			Type = declaredType.Type;
			GenericArguments = declaredType.GenericArguments;
		}

		public ITypeModel Type { get; set; }
		public IDictionary<IGeneric, IDeclaredType> GenericArguments { get; set; }
		public virtual string Signature
		{
			get
			{
				var builder = new StringBuilder(Type.Name);

			    if (Type.Generics.Any())
			    {
					if(GenericArguments.Count != Type.Generics.Count)
						throw new InvalidOperationException("All Generics must contain a generic argument. The argument provided can be another generic.");

			        builder.Append('<');
			        builder.Append(string.Join(", ", Type.Generics.Select(x => GenericArguments[x].Signature)));
			        builder.Append('>');
			    }
			    return builder.ToString();
			}
		}
    }
}