﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.Models.Method
{
    public class MethodOutput : DeclaredType, IMethodOutput
    {
        public MethodOutput(ITypeModel model) 
			: base(model)
        {}        
		public MethodOutput(IDeclaredType declaredModel)
			: base(declaredModel)
        {}
    }
}