﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Validators;

namespace ModelGenerator.Domain.Validators
{
	public class OveridableValidator : IOveridableValidator
	{
		public void AssertIsCompatibleOverride(IEnumerable<ISignatured> overides, ISignatured overide)
		{
			if (overides == null) throw new ArgumentNullException("overides");
			if (overide == null) throw new ArgumentNullException("overide");

			if(overides.Any(x => x.Signature == overide.Signature))
				throw new InvalidOperationException(
@"Cannot add this item as an existing item exists that shares the same signature.
Consider changing the 'Name' of the item or make it an overloaded item.");
		}
	}
}