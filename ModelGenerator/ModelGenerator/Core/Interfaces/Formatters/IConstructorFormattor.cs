﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Formatters
{
	public interface IConstructorFormattor
	{
		string BuildConstructors(IList<IConstructor> constructors);
	}
}