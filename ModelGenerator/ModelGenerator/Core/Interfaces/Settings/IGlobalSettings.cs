﻿using ModelGenerator.Core.Enums;

namespace ModelGenerator.Core.Interfaces.Settings
{
    public interface IGlobalSettings
    {
        EntityIdType DefaultEntityIdType { get; set; }
    }
}