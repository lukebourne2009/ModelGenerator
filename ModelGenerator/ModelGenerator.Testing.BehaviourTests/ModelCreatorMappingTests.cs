﻿using ModelGenerator.Common;
using ModelGenerator.Common.Factories;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Domain;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.ObjectCreator;
using ModelGenerator.Domain.Settings;
using ModelGenerator.Domain.Validators;
using ModelGenerator.Helpers;
using NUnit.Framework;

namespace ModelGenerator.Testing.BehaviourTests
{
    [TestFixture]
    public class ModelCreatorMappingTests
    {
        [Test]
        public void Build_Creates3CSharpFilesWithTheCorrectNamesAtLocationProvided()
        {
            // Arrange
            string objectName = "MyModel";
            IModelCreator creator = CreateModelCreator();

            // Act
            creator.Start(objectName)
				.CreateProperty("Prop", "int")
                .CreateProperty("Prop2", "int")
                    .AndMapToViewModelProperty("Prop")
                    .FromModel()
                    .UsingProperties("Prop", "Prop2")
                .Build(TestHelper.TestDirectory);

            // Assert
            TestHelper.AssertFileMatchesTemplate(TestHelper.TestDirectory + "\\Common\\Mappers\\MyModelModelMapper.cs",
				concreteTemplate);
        }

        private static ModelCreator CreateModelCreator()
        {
	        var man = new GlobalObjectManager();

	        var typeMapper = new DeclaredTypeMapper(man);

	        var methodImplementationMapper = new MethodImplementationMapper(man, new ScopeMapper(man, typeMapper), typeMapper);
	        var implementationDictionaryMapper = new MethodImplementationDictionaryMapper(man,
		        methodImplementationMapper);

	        var classCreator = new ClassCreator(new FileCreator(), man,
		        new FieldFormatter(),
		        new ConstructorFormatter(),
		        new ClassMethodFormatter(), new ClassPropertyFormatter(),
		        new ObjectDeclerationFormatter(),
		        new BraceFormatter(), new GenericReplaceFormatter(), new OveridableValidator(),
		        new MethodArgumentMapper(new DeclaredTypeMapper(man)),
		        new MethodOutputMapper(typeMapper),
		        new ImplementationMapper(implementationDictionaryMapper, typeMapper),
		        new ConstructorMapper(new MethodArgumentMapper(new DeclaredTypeMapper(man)),
			        new MethodImplementationMapper(man,
				        new ScopeMapper(man, typeMapper), typeMapper)),
		        new PropertyMapper(typeMapper),
		        new FieldMapper(typeMapper),
		        typeMapper,
		        implementationDictionaryMapper, methodImplementationMapper);

	        var interfaceCreator = new InterfaceCreator(new FileCreator(), new BraceFormatter(), man,
		        new MethodArgumentMapper(new DeclaredTypeMapper(man)), new MethodOutputMapper(typeMapper));

	        return new ModelCreator(classCreator,
		        interfaceCreator,
		        new ObjectTemplateBuilder(man, classCreator, interfaceCreator),
		        new PropertyDesignModelFactory(),
		        new GlobalSettings(), man, new PropertyMappingMapper(man, typeMapper));
        }

	    private string concreteTemplate =
			@"public class MyModelModelMapper : IModelMapper<MyModelEntity, MyModelModel, MyModelViewModel>
{
	private readonly IMapper<int, int, int> _myModelViewModelPropMapper;
	
	public MyModelModelMapper(IMapper<int, int, int> myModelViewModelPropMapper)
	{
		_myModelViewModelPropMapper = myModelViewModelPropMapper;
	}
	
	public MyModelEntity MapToEntity(MyModelModel source)
	{
		return new MyModelEntity()
		{
			Prop = source.Prop,
			Prop2 = source.Prop2
		};
	}
	public MyModelEntity MapToEntity(MyModelViewModel source)
	{
		var myModelModel = MapToModel(source);
		var myModelEntity = MapToEntity(myModelModel);
		return myModelEntity;
	}
	public MyModelModel MapToModel(MyModelEntity source)
	{
		return new MyModelModel()
		{
			Prop = source.Prop,
			Prop2 = source.Prop2
		};
	}
	public MyModelModel MapToModel(MyModelViewModel source)
	{
		return new MyModelModel()
		{
			Prop = source.Prop,
			Prop2 = source.Prop2
		};
	}
	public MyModelViewModel MapToViewModel(MyModelEntity source)
	{
		var myModelModel = MapToModel(source);
		var myModelViewModel = MapToViewModel(myModelModel);
		return myModelViewModel;
	}
	public MyModelViewModel MapToViewModel(MyModelModel source)
	{
		return new MyModelViewModel()
		{
			Prop = _myModelViewModelPropMapper.Map(source.Prop, source.Prop2),
			Prop2 = source.Prop2
		};
	}
	
}
";
	}
}