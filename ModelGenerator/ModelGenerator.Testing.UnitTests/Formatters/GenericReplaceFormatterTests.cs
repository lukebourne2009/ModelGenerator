﻿using System;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
    [TestFixture]
    public class GenericReplaceFormatterTests : BaseTest<GenericReplaceFormatter>
    {
        [Test]
        public void Format_GivenAGenericArgument_ThenReplaceTheCorrectTypes()
        {
            // Arrange
            var sut = Create<GenericReplaceFormatter>();
            var implementation = new Implementation(new DeclaredType(new ClassModel("Implement")));
            implementation.DeclaredType.GenericArguments = new Dictionary<IGeneric, IDeclaredType>
            {
                {new Generic("TType"), new GenericArgument(new ClassModel("Object"))}
            };
            var objectModel = new ClassModel("Model", new Uri("C:\\dev"));
            objectModel.Implementations = new List<IImplementation>
            {
                implementation
            };

            // Act
            string result = sut.Format("public class Thing {TType}", objectModel);

            // Assert
            Assert.AreEqual("public class Thing {Object}", result);
        }
    }
}