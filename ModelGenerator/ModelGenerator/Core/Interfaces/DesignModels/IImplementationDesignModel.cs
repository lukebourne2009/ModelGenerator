﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
    public interface IImplementationDesignModel : IDeclaredDesignModel
    {
		// TODO Ideally this should not be here
        IObjectModel Model { get; set; }
        Dictionary<string, IMethodImplementationDesignModel> MethodImplementations { get; set; }
    }
}