﻿using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels.MethodImplementations
{
    public class MethodOutputDesignModel : DeclaredDesignModel, IMethodOutputDesignModel
    {
        public MethodOutputDesignModel()
			: base("void")
        {}

        public MethodOutputDesignModel(string type)
			: base(type)
        {}
    }
}