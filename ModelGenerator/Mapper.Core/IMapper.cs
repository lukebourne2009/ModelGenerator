﻿using System.Collections.Generic;

namespace Mapper.Core
{
    public interface IMapper<in TSource, out TTarget>
    {
        TTarget Map(TSource source);
        IEnumerable<TTarget> Map(IEnumerable<TSource> sources);
    }
    public interface IMapper<in TSource, out TTarget, in TArg1>
    {
        TTarget Map(TSource source, TArg1 arg1);
		IEnumerable<TTarget> Map(IEnumerable<TSource> sources, TArg1 arg);
    }
}