﻿using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Models.Method.Implementations;

namespace ModelGenerator.Domain.Models.Method
{
    public class NotImplementedImplementation : MethodImplementation
    {
	    public NotImplementedImplementation()
			: base(null)
	    {}

	    public override string Implementation { get { return "throw new NotImplementedException();"; } }
    }
}