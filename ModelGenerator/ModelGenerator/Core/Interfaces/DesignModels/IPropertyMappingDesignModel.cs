﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface IPropertyMappingDesignModel
	{
		IDeclaredType Source { get; set; }
		IDeclaredDesignModel SourceDesign { get; set; }
		string TargetProperty { get; set; }
		IList<string> UsingProperties { get; set; }
	}
}