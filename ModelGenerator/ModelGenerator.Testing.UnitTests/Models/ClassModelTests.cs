﻿using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NUnit.Framework;

namespace ModelGenerator.Testing.UnitTests.Models
{
	[TestFixture]
	public class ClassModelTests
	{
		[Test]
		public void FullName_GivenAName_ReturnsNameOnly()
		{
			// Arrange
			var sut = new ClassModel("Name");

			// Act
			var result = sut.Signature;

			// Assert
			Assert.AreEqual("Name", result);
		}

		[Test]
		[TestCaseSource("FullName_GivenANameAndGenerics_ReturnsNameWithFormattedGenerics_TestData")]
		public string FullName_GivenANameAndGenerics_ReturnsNameWithFormattedGenerics(List<IGeneric> generics)
		{
			// Arrange
			var sut = new ClassModel("Name")
			{
				Generics = generics
			};

			// Act
			var result = sut.Signature;

			// Assert
			return result;
		}

		private static IEnumerable FullName_GivenANameAndGenerics_ReturnsNameWithFormattedGenerics_TestData()
		{
			yield return new TestCaseData(new List<IGeneric>
			{
				new Generic("Type1")
			}).Returns("Name<Type1>");
			yield return new TestCaseData(new List<IGeneric>
			{
				new Generic("Type1"),
				new Generic("Type2")
			}).Returns("Name<Type1, Type2>");
			yield return new TestCaseData(new List<IGeneric>
			{
				new Generic("Type1"),
				new Generic("Type2"),
				new Generic("Type3")
			}).Returns("Name<Type1, Type2, Type3>");
		}
	}
}