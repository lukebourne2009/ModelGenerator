﻿using System.Collections.Generic;
using System.Linq;

namespace Mapper.Core
{
    public abstract class BaseMapper<TSource, TTarget> : IMapper<TSource, TTarget>
    {
	    public abstract TTarget Map(TSource source);
	    public virtual IEnumerable<TTarget> Map(IEnumerable<TSource> source)
        {
            return source.Select(Map);
        }
    }
    public abstract class BaseMapper<TSource, TTarget, TArg> : IMapper<TSource, TTarget, TArg>
    {
        public abstract TTarget Map(TSource source, TArg arg1);
	    public virtual IEnumerable<TTarget> Map(IEnumerable<TSource> source, TArg arg1)
        {
            return source.Select(x => Map(x, arg1));
        }
    }
}