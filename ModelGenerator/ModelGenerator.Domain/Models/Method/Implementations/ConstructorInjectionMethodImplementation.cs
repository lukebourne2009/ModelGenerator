﻿using System;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Domain.Models.Method.Implementations
{
    public class ConstructorInjectionMethodImplementation : MethodImplementation
	{
		private readonly IFormatter _camelCaseFormatter = new CamelCaseFormatter();
        public ConstructorInjectionMethodImplementation(IScope scope)
			: base(scope.Method)
        {
	        Scope = scope;
        }

		public IScope Scope { get; private set; }

	    public override string Implementation
        {
            get
            {
				if(!(Scope.Model.Type is ClassModel))
					throw new InvalidOperationException();

	            var arguments = Scope.Method.Inputs;
				if (!arguments.Any()) return string.Empty;

				var builder = new StringBuilder("\r\n");

	            foreach (var methodArgument in arguments)
				{ 
		            IField field;
		            if((field = (Scope.Model.Type as IObjectModel).Fields
						.FirstOrDefault(x => x.Name.Substring(1) == methodArgument.Name)) == null)
						throw new InvalidOperationException();

		            builder.AppendFormat("{0} = {1};\r\n", field.Name, 
						_camelCaseFormatter.Format(methodArgument.Name));

	            }
				return builder.ToString();
			}
        }
    }
}