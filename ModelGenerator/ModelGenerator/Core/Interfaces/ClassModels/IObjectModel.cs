﻿using System;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.ClassModels
{
    public interface IObjectModel : ITypeModel
    {
        Uri FilePath { get; set; }
	    IList<IField> Fields { get; set; }
		IList<IConstructor> Constructors { get; set; }
        IList<IProperty> Properties { get; set; }
        IList<IMethod> Methods { get; set; }
        IList<IImplementation> Implementations { get; set; }
    }
}