﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.DesignModels
{
	public class TypeDesignModel : TypeSignatured<ITypeDesignModel>, ITypeDesignModel
	{
		public TypeDesignModel(string name)
			: base(x => x.Name, x => x.GenericArguments)
		{
			Name = name;
		}

		public string Name { get; set; }
		public IList<IGenericArgumentDesignModel> GenericArguments { get; set; }
	}
}