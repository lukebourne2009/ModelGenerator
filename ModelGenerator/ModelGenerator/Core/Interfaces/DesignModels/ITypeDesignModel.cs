﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface ITypeDesignModel : INamed, ISignatured
	{
		IList<IGenericArgumentDesignModel> GenericArguments { get; set; }
	}
}