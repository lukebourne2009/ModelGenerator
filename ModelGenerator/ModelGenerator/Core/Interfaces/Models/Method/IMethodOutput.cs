﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Models.Method
{
    public interface IMethodOutput : IDeclaredType
    {}
}