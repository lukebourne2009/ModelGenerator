﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Signatures
{
	public abstract class TypeSignatured<TType> : ISignatured 
		where TType : class
	{
		private readonly Func<TType, string> _nameSelector;
		private readonly Func<TType, IEnumerable<ISignatured>> _genericSelector;

		protected TypeSignatured(Func<TType,string> nameSelector, 
			Func<TType, IEnumerable<ISignatured>> genericSelector)
		{
			_nameSelector = nameSelector;
			_genericSelector = genericSelector;
		}
		public virtual string Signature
		{
			get
			{
				StringBuilder builder = new StringBuilder(_nameSelector(this as TType));
				var generics = _genericSelector(this as TType).ToList();
				if (generics.Any())
					builder.AppendFormat("<{0}>", string.Join(", ", generics.Select(x => x.Signature)));
				return builder.ToString();
			}
		}
	}
}