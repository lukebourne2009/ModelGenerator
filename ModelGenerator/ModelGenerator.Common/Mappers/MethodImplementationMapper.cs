﻿using System;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Exceptions;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.Method.Implementations;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Common.Mappers
{
	public class MethodImplementationMapper : BaseMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope>
	{
		private readonly IGlobalObjectManager _manager;
		private readonly IMapper<IScopeDesignModel, IScope> _scopeMapper;
		private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredTypeMapper;
		private readonly Dictionary<Type, Func<IMethodImplementationDesignModel, IScope, IMethodImplementation>> _dictionary;

		public MethodImplementationMapper(IGlobalObjectManager manager, 
			IMapper<IScopeDesignModel, IScope> scopeMapper,
			IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredTypeMapper)
		{
			_manager = manager;
			_scopeMapper = scopeMapper;
			_declaredTypeMapper = declaredTypeMapper;
			_dictionary = new Dictionary<Type, Func<IMethodImplementationDesignModel, IScope, IMethodImplementation>>
			{
				{typeof(MapperCreateMethodImplementationDesignModel), CreateMapperCreateMethodImplementation},
				{typeof(ConstructorInjectionMethodImplementationDesignModel), CreateConstructorInjectionMethodImplementation }
			};
		}

		private IMethodImplementation CreateConstructorInjectionMethodImplementation(IMethodImplementationDesignModel source, IScope scope)
		{
			return new ConstructorInjectionMethodImplementation(scope);
		}

		private IMethodImplementation CreateMapperCreateMethodImplementation(IMethodImplementationDesignModel source, IScope scope)
		{
			var mapperCreate = (source as MapperCreateMethodImplementationDesignModel);
			var returnObject = _declaredTypeMapper.Map(mapperCreate.NewObject, scope);
			var conceptModel = _manager.GetModel(mapperCreate.ConceptModelSignature);

			return new MapperCreateMethodImplementation(returnObject, conceptModel, scope);
		}


		public override IMethodImplementation Map(IMethodImplementationDesignModel source, IScope scope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (scope == null) throw new ArgumentNullException("scope");

			return _dictionary[source.GetType()](source, scope);
		}
	}
}