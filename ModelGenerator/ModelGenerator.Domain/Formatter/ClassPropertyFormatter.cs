﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Formatter
{
    public class ClassPropertyFormatter : IPropertyFormatter
    {
        public string BuildProperties(IEnumerable<IProperty> properties, IEnumerable<IProperty> implementationProperties)
        {
            if (properties               == null) throw new ArgumentNullException("properties");
            if (implementationProperties == null) throw new ArgumentNullException("implementationProperties");
            
            var builder = new StringBuilder();
            foreach (var property in properties)
            {
                BuildProperty(builder, property);
            }

            foreach (var implementationProperty in implementationProperties)
            {
                BuildProperty(builder, implementationProperty);
            }

            return builder.ToString();
        }

        private static void BuildProperty(StringBuilder builder, IProperty prop)
        {
            foreach (IAttribute attribute in prop.Attributes)
            {
                builder.AppendFormat("[{0}]\r\n", attribute.Name);
            }
            builder.AppendFormat("public {0} {1} {{ get; set; }}\r\n",
                prop.DeclaredType.Signature, prop.Name);
        }
    }
}