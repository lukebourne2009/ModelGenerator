﻿using System;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;

namespace ModelGenerator.Common.Mappers
{
	public class MethodImplementationDictionaryMapper : BaseMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope>
    {
	    private readonly IGlobalObjectManager _manager;
	    private readonly IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> _implementationMapper;

		public MethodImplementationDictionaryMapper(IGlobalObjectManager manager, 
			IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> implementationMapper)
		{
			_manager = manager;
			_implementationMapper = implementationMapper;
		}

		public override IDictionary<IMethod, IMethodImplementation> Map(IDictionary<string, IMethodImplementationDesignModel> source, IScope scope)
        {
	        if (source == null) throw new ArgumentNullException("source");
	        if (scope == null) throw new ArgumentNullException("scope");

	        var dictionary = new Dictionary<IMethod, IMethodImplementation>();

	        foreach (var designModel in source)
	        {
		        var method = scope.GetMethod(designModel.Key);
		        var methodScope = new Scope(scope.Model, method);
				dictionary.Add(method, _implementationMapper.Map(designModel.Value, methodScope));
	        }

	        return dictionary;
        }
    }
}