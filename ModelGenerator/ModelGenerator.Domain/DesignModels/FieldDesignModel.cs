﻿using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels
{
	public class FieldDesignModel : DeclaredDesignModel, IFieldDesignModel
	{
		public FieldDesignModel(string type, string name)
			: base(type)
		{
			Name = name;
			Readonly = false;
		}
		public string Name { get; set; }
		public bool Readonly { get; set; }
	}
}