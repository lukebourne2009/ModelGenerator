﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Core.Interfaces.Models
{
	/* TODO: using a ITypeModel here is incorrect as Name is refering to the type here and it should be refering to the property name.
	 * This would be more suited for some kind of IVariable type deal
	 */
    public interface IMethod : ITypeModel, IFunction
    {}
}