﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Core.Interfaces.Validators;
using ModelGenerator.Core.Settings;
using ModelGenerator.Domain;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.ObjectCreator;
using ModelGenerator.Domain.Validators;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;
using TestHelper = ModelGenerator.Testing.BehaviourTests.TestHelper;

namespace ModelGenerator.Testing.UnitTests
{
    internal class TestStream : Stream
    {
        public static string[] Result = new string[1];

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            Result[0] = Encoding.ASCII.GetString(buffer);
        }

        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanWrite
        {
            get { throw new NotImplementedException(); }
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position { get; set; }
    }

    [TestFixture]
    public class ClassCreatorTests : BaseTest<ClassCreator>
    {
        #region CreateModels

        [Test]
        public void CreateObject_CallsCreateFileOnFileCreator()
        {
            // Arrange
	        Mock<IFileCreator>().CreateFile("C:\\Dev\\MyModelViewModel.cs").Returns(new TestStream());

            // Act
            Sut.CreateObject(new ClassModel("MyModelViewModel", new Uri("C:\\Dev")));

			// Assert
	        Mock<IFileCreator>().Received(1).CreateFile("C:\\Dev\\MyModelViewModel.cs");
        }
        [Test]
        public void CreateObject_StreamsToCreatedFile()
        {
			// Arrange
			Mock<IFileCreator>().CreateFile("C:\\Dev\\MyModelViewModel.cs").Returns(new TestStream());
	        Mock<IFormatter>().Format(Arg.Any<string>()).Returns("Formatted");

			// Act
	        Sut.CreateObject(new ClassModel("MyModelViewModel", new Uri("C:\\Dev")));

            // Assert
            Assert.AreEqual("Formatted", TestStream.Result[0]);
        }

	    [Test]
	    public void CreateObject_FormattersArePlacedInTheCorrectPlace()
	    {
			// Arrange
		    var model = new ClassModel("MyModelViewModel", new Uri("C:\\Dev"));
		    Mock<IFileCreator>().CreateFile("C:\\Dev\\MyModelViewModel.cs").Returns(new TestStream());
		    Mock<IObjectDeclerationFormatter>().BuildDecleration("MyModelViewModel", model.Implementations).Returns("BuildDecleration");
		    Mock<IFieldFormatter>().BuildFields(model.Fields).Returns("BuildFields");
		    Mock<IConstructorFormattor>().BuildConstructors(model.Constructors).Returns("BuildConstructors");
		    Mock<IPropertyFormatter>().BuildProperties(model.Properties, Arg.Any<IEnumerable<IProperty>>()).Returns("BuildProperties");
		    Mock<IMethodFormatter>().BuildMethods(model.Methods, model.Implementations).Returns("BuildMethods");

			// Act
		    Sut.CreateObject(model);

		    // Assert
		    Mock<IGenericFormatter>().Received(1).Format("BuildDecleration" +
		                                     "{\r\n" +
		                                     "BuildFields" +
		                                     "BuildConstructors" +
		                                     "BuildProperties" +
		                                     "BuildMethods\r\n" +
		                                                 "}\r\n", model);
	    }

	    [Test]
	    public void CreateObject_RegistersObjectAgainstGlobalObjectManager()
	    {
		    // Arrange
		    Mock<IFileCreator>().CreateFile(Arg.Any<string>()).Returns(new TestStream());
		    var model = new ClassModel("MyModelViewModel", new Uri("C:\\Dev"));

			// Act
		    Sut.CreateObject(model);

		    // Assert
			Mock<IGlobalObjectManager>().Received(1).RegisterObject(model);
	    }

		#endregion

		#region SetField

		[Test]
		public void SetField_GivenValidDesignModel_FieldIsSet()
		{
			// Arrange
			var creator = Create<ClassCreator>();

			// Act
			var model = creator.SetField(new ClassModel("Type"), new FieldDesignModel("Type", "arg"));

			// Assert
			Assert.IsNotNull(model.Fields[0]);
		}

	    [Test]
	    public void SetField_CallsMapperWithDesignModelAndReturnsModel()
	    {
		    // Arrange
		    var creator = Create<ClassCreator>();
		    var designModel = new FieldDesignModel("int", "arg");
		    var field = new Field(new ClassModel("int"), "arg");
		    ClassModel classModel = new ClassModel("Type");
			Mock<IMapper<IFieldDesignModel, IField, IScope>>()
				.Map(designModel, Arg.Is<IScope>(x => x.Model.Type == classModel)).Returns(field);
		
			// Act
		    var model = creator.SetField(classModel, designModel);

			// Assert
			Assert.AreEqual(field, model.Fields[0]);
	    }

		#endregion

		#region Set Constructor

		[Test]
	    public void SetConstructor_GivenValidDesignModel_ConstructorIsSet()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();

		    // Act
		    var prop = sut.SetConstructor(new ClassModel("MyModelViewModel"), new ConstructorDesignModel("Mapper"));

		    // Assert
		    Assert.IsNotNull(prop.Constructors[0]);
	    }		
		[Test]
	    public void SetConstructor_GivenValidDesignModel_ModelIsRunThroughConstructorMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var designModel = new ConstructorDesignModel("Mapper");
		    var model = new ClassModel("MyModelViewModel");
		    var constructor = new Constructor("Ctor");
		    Mock<IMapper<IConstructorDesignModel, IConstructor, IScope>>()
			    .Map(designModel, Arg.Is<Scope>(x => x.Model.Type == model)).Returns(constructor);

		    // Act
		    var prop = sut.SetConstructor(model, designModel);

		    // Assert
			Assert.AreEqual(constructor, prop.Constructors[0]);
	    }
	    [Test]
	    public void SetConstructor_ValidationIsRunThroughValidator()
	    {
		    // Arrange
			var sut = Create<ClassCreator>();
		    var model = new ClassModel("MyModelViewModel");
		    var designModel = new ConstructorDesignModel("Mapper");

			// Act
			sut.SetConstructor(model, designModel);

			// Assert
			Mock<IOveridableValidator>().Received(1).AssertIsCompatibleOverride(model.Constructors, designModel);
        }

		#endregion

		#region Set Property

		[Test]
        public void SetProperty_GivenAPropertyIsSet_ThePropertyIsAddedToTheList()
        {
            // Arrange
	        var sut = Create<ClassCreator>();
	        var propModel = new PropertyDesignModel("Prop", "PropType");
	        var model = new ClassModel("Blogs");
	        var property = new Property("Property", new ClassModel("Type"));
	        Mock<IMapper<IPropertyDesignModel, IProperty, IScope>>()
				.Map(propModel, Arg.Is<Scope>(x => x.Model.Type == model)).Returns(property);

			// Act
			var prop = sut.SetProperty(model, propModel);

            // Assert
			Assert.AreEqual(property, prop.Properties[0]);
        }

	    [Test]
		public void SetProperty_ValidationIsRunThroughValidator()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("MyModelViewModel");
		    var designModel = new PropertyDesignModel("Mapper", "Type");

		    // Act
		    sut.SetProperty(model, designModel);

		    // Assert
		    Mock<IOveridableValidator>().Received(1).AssertIsCompatibleOverride(model.Properties, designModel);
	    }

	    [Test]
	    public void SetProperty_GivenAnOrderIsSet_InsertIntoListAtTheCorrectPlace()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var propModel = new PropertyDesignModel("Prop", "PropType")
		    {
				Settings = new PropertyDesignSettings
				{
					Order = 2
				}
		    };
		    Mock<IObjectModel>().Properties.Returns(new List<IProperty>
		    {
			    new Property("Prop1", new ClassModel("Type")),
			    new Property("Prop2", new ClassModel("Type")),
			    new Property("Prop3", new ClassModel("Type"))
		    });
		    Mock<IMapper<IPropertyDesignModel, IProperty, IScope>>()
			    .Map(propModel, Arg.Is<Scope>(x => x.Model.Type == Mock<IObjectModel>())).Returns(Mock<IProperty>());

		    // Act
		    var prop = sut.SetProperty(Mock<IObjectModel>(), propModel);

		    // Assert
			Assert.AreEqual(Mock<IProperty>(), prop.Properties[2]);
	    }

		#endregion

		#region Implement

		[Test]
		public void Implement_ValidationIsRunThroughValidator()
		{
			// Arrange
			var sut = Create<ClassCreator>();
			var model = new ClassModel("MyModelViewModel");
			var designModel = new ImplementationDesignModel("IType");

			// Act
			sut.Implement(model, designModel);

			// Assert
			Mock<IOveridableValidator>().Received(1)
				.AssertIsCompatibleOverride(
					Arg.Is<IEnumerable<ISignatured>>(x => x.SequenceEqual(model.Implementations.Select(y => y.DeclaredType))),
					designModel);
		}
	    [Test]
	    public void Implement_AddsImplementationWithResultOfImplementationMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("MyModelViewModel");
		    var designModel = new ImplementationDesignModel("IType");
		    var implementation = new Implementation(new DeclaredType(new ClassModel("Model")));
			
		    Mock<IMapper<IImplementationDesignModel, IImplementation, IScope>>()
			    .Map(designModel, Arg.Is<Scope>(x => x.Model.Type == model))
			    .Returns(implementation);

			// Act
			sut.Implement(model, designModel);

		    // Assert
		    Assert.AreEqual(implementation, model.Implementations[0]);
	    }
		#endregion

		#region Set Method

	    [Test]
	    public void SetMethod_ValidationIsRunThroughValidator()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("MyModelViewModel");
		    var designModel = new MethodDesignModel("Name");

		    // Act
		    sut.SetMethod(model, designModel);

		    // Assert
		    Mock<IOveridableValidator>().Received(1).AssertIsCompatibleOverride(model.Methods, designModel);
	    }
		[Test]
        public void SetMethod_GivenAValidMethod_AddsItToTheList()
        {
            // Arrange
	        IObjectCreator sut = Create<ClassCreator>();
            var model = new ClassModel("MyModel", new Uri("C:\\dev"));

            // Act
            sut.SetMethod(model, new MethodDesignModel("MyMethod"));

            // Assert
            Assert.AreEqual(1, model.Methods.Count);
        }
		[Test]
	    public void SetMethod_CorrectScopeIsUsedForArgumentMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("Model");

			// Act
			sut.SetMethod(model, Mock<IMethodDesignModel>());

			// Assert
		    Mock<IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope>>().Received(1)
			    .Map(Mock<IMethodDesignModel>().Inputs, 
				Arg.Is<Scope>(x => x.Model.Type == model && x.Method == model.Methods[0]));
	    }
		[Test]
		public void SetMethod_CorrectScopeIsUsedForOutputMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("Model");

		    // Act
		    sut.SetMethod(model, Mock<IMethodDesignModel>());

		    // Assert
		    Mock<IMapper<IMethodOutputDesignModel, IMethodOutput, IScope>>().Received(1)
			    .Map(Mock<IMethodDesignModel>().Output,
				    Arg.Is<Scope>(x => x.Model.Type == model && x.Method == model.Methods[0]));
		}
		[Test]
	    public void SetMethod_InputsAreSetUsingArgumentMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("Model");

		    // Act
		    var result = sut.SetMethod(model, Mock<IMethodDesignModel>());

		    // Assert
		    var expected = Mock<IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope>>().Map(
				Mock<IMethodDesignModel>().Inputs, Arg.Is<Scope>(x => x.Model.Type == model && x.Method == model.Methods[0]));

			Assert.AreEqual(expected, result.Methods[0].Inputs);
		}
		[Test]
		public void SetMethod_OutputIsSetUsingOutputMapper()
	    {
		    // Arrange
		    var sut = Create<ClassCreator>();
		    var model = new ClassModel("Model");
		    var methodOutput = new MethodOutput(new ClassModel("Return"));
			Mock<IMapper<IMethodOutputDesignModel, IMethodOutput, IScope>>().Map(
				Mock<IMethodDesignModel>().Output, Arg.Is<Scope>(x => x.Model.Type == model)).Returns(methodOutput);

			// Act
			var result = sut.SetMethod(model, Mock<IMethodDesignModel>());

		    // Assert
		    Assert.AreEqual(methodOutput, result.Methods[0].Output);
		}
        #endregion
    }
}