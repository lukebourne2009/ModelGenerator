﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Formatter
{
	public class ConstructorFormatter : IConstructorFormattor
	{
	    public string BuildConstructors(IList<IConstructor> constructors)
		{
		    if (constructors == null) throw new ArgumentNullException("constructors");

		    var builder = new StringBuilder();

		    foreach (var constructor in constructors)
		    {
		        builder.AppendFormat("public {0}(", constructor.Name);

		        var arguments = constructor.Inputs.Select(x => string.Format("{0} {1}", x.DeclaredType.Signature, x.Name));
		        builder.Append(string.Join(", ", arguments));

		        builder.Append(")\r\n{");
		        builder.Append(constructor.Implementation.Implementation);
		        builder.Append("}\r\n\r\n");
		    }

		    return builder.ToString();
		}
	}
}