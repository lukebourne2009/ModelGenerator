﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;

namespace ModelGenerator.Domain.Models.Mapping
{
	public class PropertyMapping : IPropertyMapping
	{
		public PropertyMapping(IDeclaredType sourceModel)
		{
			SourceModel = sourceModel;
			UsingProperties = new List<IProperty>();
		}

		public IDeclaredType SourceModel { get; set; }
		public IList<IProperty> UsingProperties { get; set; }
	}
}