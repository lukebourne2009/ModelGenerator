﻿using System;
using System.IO;
using ModelGenerator.Core.Interfaces.ModelCreator;

namespace ModelGenerator.Domain
{
    public class FileCreator : IFileCreator
    {
        public Stream CreateFile(string fileName)
        {
            Uri uri = new Uri(new Uri(fileName), ".");
            Directory.CreateDirectory(uri.LocalPath);
            return File.Create(fileName);
        }
    }
}