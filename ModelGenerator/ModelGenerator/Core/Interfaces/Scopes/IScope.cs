﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Scopes
{
	public interface IScope
	{
		IDeclaredType Model { get; }
		IMethod Method { get; }
		IList<IVariable> GetVariables();
		IGeneric GetGeneric(string name);
		IDeclaredType GetGenericArgument(string genericType);
		IDeclaredType GetGenericArgument(IGeneric genericType);
		IMethod GetMethod(string methodSig);
	}
}