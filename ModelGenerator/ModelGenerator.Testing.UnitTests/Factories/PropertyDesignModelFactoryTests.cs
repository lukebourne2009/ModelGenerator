﻿using System;
using System.Linq;
using ModelGenerator.Common.Factories;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Factories;
using ModelGenerator.Core.Settings;
using NUnit.Framework;

namespace ModelGenerator.Testing.UnitTests.Factories
{
    [TestFixture]
    public class PropertyDesignModelFactoryTests
    {
        [Test, Pairwise]
        public void Create_GivenNullInput_ThrowException(
            [Values("", null)] string name, 
            [Values("", null)] string type, 
            [Values] bool settings)
        {
            // Arrange
            IPropertyDesignModelFactory sut = new PropertyDesignModelFactory();

            // Act
            TestDelegate del = () => sut.Create(name, type, settings ? new PropertyDesignSettings() : null);

            // Assert
            Assert.Throws<ArgumentNullException>(del);
        }

        [Test]
        public void Create_CreatePropertyValuesCorrectly(
            [Values("Name", "Different")] string name, 
            [Values("Type", "Thing")] string type)
        {
            // Arrange
            IPropertyDesignModelFactory sut = new PropertyDesignModelFactory();

            // Act
            IPropertyDesignModel result = sut.Create(name, type, new PropertyDesignSettings());

            // Assert
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(type, result.Type);
        }

        [Test]
        public void Create_GivenRequiredSet_MapsAttributeCorrectly()
        {
            // Arrange
            IPropertyDesignModelFactory sut = new PropertyDesignModelFactory();

            // Act
            PropertyDesignSettings propertyDesignSettings = new PropertyDesignSettings
            {
                Required = true
            };
            IPropertyDesignModel result = sut.Create("name", "type", propertyDesignSettings);

            // Assert
            Assert.IsTrue(result.Attributes.Any(x => x.Name == "Required"));
        }        
        [Test]
        public void Create_GivenRequiredNotSet_DoesntSetAttribute()
        {
            // Arrange
            IPropertyDesignModelFactory sut = new PropertyDesignModelFactory();

            // Act
            PropertyDesignSettings propertyDesignSettings = new PropertyDesignSettings();
            IPropertyDesignModel result = sut.Create("name", "type", propertyDesignSettings);

            // Assert
            Assert.IsTrue(!result.Attributes.Any());
        }

    }
}