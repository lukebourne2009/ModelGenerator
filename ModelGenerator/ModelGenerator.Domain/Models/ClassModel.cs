﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Enums;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;

namespace ModelGenerator.Domain.ClassModels
{
    public class ClassModel : IObjectModel
    {
        public ClassModel()
            : this("")
        {}
        public ClassModel(string name)
            : this(name, null)
        {}

        public ClassModel(string name, Uri uri)
        {
            Name = name;
            FilePath = uri;
            Properties = new List<IProperty>();
            Methods = new List<IMethod>();
            Generics = new List<IGeneric>();
            Implementations = new List<IImplementation>();
			Constructors = new List<IConstructor>();
			Fields = new List<IField>();
			PropertyMappings = new List<IPropertyMapping>();
        }

        public string Name { get; set; }

	    public string Signature
	    {
		    get
		    {
			    var builder = new StringBuilder(Name);
			    if (Generics.Any())
				    builder.AppendFormat("<{0}>", string.Join(", ", Generics.Select(x => x.Signature)));
				return builder.ToString();
		    }
	    }

	    public Uri FilePath { get; set; }

	    public IList<IField> Fields { get; set; }
	    public IList<IConstructor> Constructors { get; set; }
	    public IList<IProperty> Properties { get; set; }
        public IList<IMethod> Methods { get; set; }
        public IList<IGeneric> Generics { get; set; }
	    public IList<IPropertyMapping> PropertyMappings { get; set; }
	    public IList<IImplementation> Implementations { get; set; }
    }
}