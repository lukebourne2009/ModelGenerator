﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Signatures
{
	public abstract class MethodSignatured<TType> : TypeSignatured<TType>
		where TType : class
	{
		private readonly Func<TType, IEnumerable<ISignatured>> _inputSelector;

		protected MethodSignatured(Func<TType, string> nameSelector,
			Func<TType, IEnumerable<ISignatured>> genericSelector,
			Func<TType, IEnumerable<ISignatured>> inputSelector)
			: base(nameSelector, genericSelector)
		{
			_inputSelector = inputSelector;
		}
		public override string Signature
		{
			get
			{
				var builder = new StringBuilder(base.Signature);
				foreach (var input in _inputSelector(this as TType))
				{
					builder.Append('.');
					builder.Append(input.Signature);
				}
				return builder.ToString();
			}
		}
	}
}