﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class DeclaredTypeMapperTests : BaseTest<DeclaredTypeMapper>
	{ 
		[Test]
		public void Map_GivenObjectIsAnObjectModel_SearchInGlobalObjectManager()
		{
			// Arrange
			var sut = Create<DeclaredTypeMapper>();

			// Act
			var result = sut.Map(new DeclaredDesignModel("Model"), Mock<IScope>());

			// Assert
			Assert.AreEqual(Mock<IGlobalObjectManager>().GetObject("Model"), result.Type);
		}		
		[Test]
		public void Map_GivenObjectIsAGeneric_SearchInScopeForGenericArgument()
		{
			// Arrange
			Mock<IGlobalObjectManager>().GetObject("TModel").Returns((IObjectModel)null);
			//var sut = Create<DeclaredTypeMapper>();

			// Act
			var result = Sut.Map(new DeclaredDesignModel("TModel"), Mock<IScope>());

			// Assert
			Assert.AreEqual(Mock<IScope>().GetGenericArgument("TModel"), result);
		}
		public void Map_GivenObjectHasGenerics_AndArgumentsAreNotSet_DoNotSetArguments()
		{
			// Arrange
			Mock<IGlobalObjectManager>().GetObject("TModel").Generics.Returns(new List<IGeneric>
			{
				new Generic("TGen")
			});
			var sut = new DeclaredTypeMapper(Mock<IGlobalObjectManager>());

			// Act
			var result = sut.Map(new DeclaredDesignModel("TModel"), Mock<IScope>());

			// Assert
			Assert.AreEqual(0, result.GenericArguments.Count);
		}

		[Test]
		[TestCaseSource("Map_GivenObjectHasGenerics_AndArgumentsAreSet_UseMapToProduceArguments_TestData", new object[]{1})]
		[TestCaseSource("Map_GivenObjectHasGenerics_AndArgumentsAreSet_UseMapToProduceArguments_TestData", new object[]{2})]
		[TestCaseSource("Map_GivenObjectHasGenerics_AndArgumentsAreSet_UseMapToProduceArguments_TestData", new object[]{3})]
		public void Map_GivenObjectHasGenerics_AndArgumentsAreSet_UseMapToProduceArguments(
			IDictionary<string, IDeclaredDesignModel> dictionary,
			List<IGeneric> generics,
			Action<IDeclaredType> asserter)
		{
			// Arrange
			var sut                         = new DeclaredTypeMapper(Mock<IGlobalObjectManager>());
			DeclaredDesignModel designModel = new DeclaredDesignModel("Model")
			{
				GenericArguments            = dictionary
			};
			Mock<IGlobalObjectManager>().GetObject(designModel.Signature).Returns(new ClassModel("Model")
			{
				Generics = generics
			});
			generics.Select(x => Mock<IGlobalObjectManager>().GetObject(x.Signature).Returns((IObjectModel) null));

			// Act
			var result = sut.Map(designModel, Mock<IScope>());

			// Assert
			asserter(result);
		}
		private static IEnumerable Map_GivenObjectHasGenerics_AndArgumentsAreSet_UseMapToProduceArguments_TestData(int nGenerics)
		{
			var generics = new List<IGeneric>();
			for (int i = 0; i < nGenerics; i++)
			{
				generics.Add(new Generic("TGen" + i));
			}
			var dictionary = new Dictionary<string, IDeclaredDesignModel>();
			for (int i = 0; i < nGenerics; i++)
			{
				dictionary.Add("TGen" + i, new DeclaredDesignModel("Concrete" + i));				
			}

			yield return new TestCaseData(dictionary, generics,
				new Action<IDeclaredType>(x => Assert.AreEqual(nGenerics, x.GenericArguments.Count)))
			{
				TestName = string.Format("Map: Given {0} Generic, and {0} argument(s) are set, " +
				                         "result will have {0} argument(s)", nGenerics)
			};

			for (int i = 0; i < nGenerics; i++)
			{
				int i1 = i;
				yield return new TestCaseData(dictionary, generics,
					new Action<IDeclaredType>(x => Assert.IsTrue(x.GenericArguments.ContainsKey(generics[i1]))))
				{
					TestName = string.Format("Map: Given {0} Generic, and {0} argument(s) are set, " +
					                         "item {1} is keyed by the IGeneric", nGenerics,i1)
				};
				yield return new TestCaseData(dictionary, generics,
					new Action<IDeclaredType>(x => Assert.AreEqual(Mock<IGlobalObjectManager>().GetObject("Concrete" + i1), 
						x.GenericArguments[generics[i1]].Type)))
				{
					TestName = string.Format("Map: Given {0} Generic, and {0} argument(s) are set, " +
					                         "item {1} contains the correct argument", nGenerics, i1)

				};
			}
		}

		[Test]
		[TestCaseSource("Map_GivenGenericArgumentIsAGenericOfObjectInScope_ReturnThatGenericAsArgument_TestData")]
		public void Map_GivenGenericArgumentIsAGenericOfObjectInScope_ReturnThatGenericAsArgument(Action<IDeclaredType, IObjectModel, IScope> assertor)
		{
			// Arrange
			var sut = Create<DeclaredTypeMapper>();
			var interfaceModel = new InterfaceModel("Interface")
			{
				Generics = {new Generic("TInterfaceGen")}
			};
			Mock<IGlobalObjectManager>().GetObject("Interface<TClassGen>").Returns(interfaceModel);
			Mock<IGlobalObjectManager>().GetObject("TClassGen").Returns((IObjectModel) null);

			// Act
			var result = sut.Map(new DeclaredDesignModel("Interface")
			{
				GenericArguments = {{"TInterfaceGen", new GenericArgumentDesignModel("TClassGen")}}
			}, Mock<IScope>());
			
			// Assert
			assertor(result, interfaceModel, Mock<IScope>());
		}

		private static IEnumerable Map_GivenGenericArgumentIsAGenericOfObjectInScope_ReturnThatGenericAsArgument_TestData()
		{
			yield return new TestCaseData(new Action<IDeclaredType, IObjectModel, IScope>((x,y, scope) =>
				Assert.IsTrue(x.GenericArguments.ContainsKey(y.Generics[0]))))
			{
				TestName = "Map: Given a generic argument which takes as its argument another generic thats in scope, Result contains this generic"
			};
			yield return new TestCaseData(new Action<IDeclaredType, IObjectModel, IScope>((x,y, scope) =>
				Assert.AreEqual(scope.GetGenericArgument("TClassGen"), x.GenericArguments[y.Generics[0]])))
			{
				TestName = "Map: Given a generic argument which takes as its argument another generic thats in scope, Generic arg is the arg in scope"
			};
		}

	}
}