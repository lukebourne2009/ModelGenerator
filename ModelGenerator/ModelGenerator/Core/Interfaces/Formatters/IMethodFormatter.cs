﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Formatters
{
    public interface IMethodFormatter
    {
		string BuildMethods(IEnumerable<IMethod> methods, IList<IImplementation> implementations);
    }
}