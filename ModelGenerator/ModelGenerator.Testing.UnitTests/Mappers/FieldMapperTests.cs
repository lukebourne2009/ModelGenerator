﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;
using TestHelper = ModelGenerator.Testing.BehaviourTests.TestHelper;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class FieldMapperTests : BaseTest<FieldMapper>
	{
		[Test]
		public void Map_GivenValidDesignModel_ReturnNonNull()
		{
			// Arrange
			var sut = Create<FieldMapper>();

			// Act
			var result = sut.Map(new FieldDesignModel("type", "name"), new NoScope());

			// Assert
			Assert.IsNotNull(result);
		}

		[Test]
		[TestCaseSource("Map_GivenValidDesignModel_ReturnCorrectType_TestData")]
		public void Map_GivenValidDesignModel_ReturnCorrectType(DeclaredType model, FieldDesignModel field, Action<IField> assertor)
		{
			Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>()
				.Map(Arg.Is<IDeclaredDesignModel>(x => x.Type == "type"),Arg.Any<IScope>()).Returns(model);

			// Act
			var result = Sut.Map(field, Mock<IScope>());

			// Assert
			assertor(result);
		}

		private static IEnumerable Map_GivenValidDesignModel_ReturnCorrectType_TestData()
		{
			var model = new DeclaredType(new ClassModel("type"));
			var field = new FieldDesignModel("type", "name");;

			yield return new TestCaseData(model, field, new Action<IField>(x => Assert.AreEqual(model.Type, x.DeclaredType.Type)));
			yield return new TestCaseData(model, field, new Action<IField>(x => Assert.AreEqual(model.GenericArguments, x.DeclaredType.GenericArguments)));
			yield return new TestCaseData(model, field, new Action<IField>(x => Assert.AreEqual("name", x.Name)));
			yield return new TestCaseData(model, field, new Action<IField>(x => Assert.IsFalse(x.Readonly)));

			field = new FieldDesignModel("type","name");
			field.Readonly = true;
			yield return new TestCaseData(model, field, new Action<IField>(x => Assert.IsTrue(x.Readonly)));
		}
	}
}