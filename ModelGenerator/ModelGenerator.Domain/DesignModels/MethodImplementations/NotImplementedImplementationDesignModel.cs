﻿using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.DesignModels.MethodImplementations
{
	public class NotImplementedImplementationDesignModel : IMethodImplementationDesignModel
	{
		public string MethodSignature { get; private set; }
		public IFunction Method { get; set; }
		public IScopeDesignModel Scope { get; set; }
	}
}