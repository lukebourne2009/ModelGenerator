﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Core.Interfaces.ModelCreator
{
    public interface IObjectCreator
    {
        IObjectModel CreateObject(IObjectModel modelContext);
	    IObjectModel SetField(IObjectModel classModel, IFieldDesignModel fieldDesignModel);
		IObjectModel SetProperty(IObjectModel modelContext, IPropertyDesignModel model);
        IObjectModel SetMethod(IObjectModel modelContext, IMethodDesignModel model);
        IObjectModel SetGeneric(IObjectModel model, string genericName);
        IObjectModel Implement(IObjectModel model, IImplementationDesignModel implementation);
	    IObjectModel SetConstructor(IObjectModel classModel, IConstructorDesignModel constructorDesignModel);
    }
}