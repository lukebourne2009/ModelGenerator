﻿using ModelGenerator.Core.Interfaces.Settings;

namespace ModelGenerator.Core.Settings
{
    public class PropertyDesignSettings : IPropertyDesignSettings
    {
        private static PropertyDesignSettings _default;
        
        public static PropertyDesignSettings Default
        {
            get { return _default ?? (_default = 
                new PropertyDesignSettings
                {
                    Order = null
                }); 
            }
            set { _default = value; }
        }
        public int? Order { get; set; }
        public bool Required { get; set; }
    }
}