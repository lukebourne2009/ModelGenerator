﻿namespace ModelGenerator.Core.Interfaces.Models
{
	public interface IField : IVariable
	{
		bool Readonly { get; set; }
	}
}