﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Formatters
{
    public interface IObjectDeclerationFormatter
    {
		// TODO: This should just simply accept an IObject Model. This way could lead to problems later.
        string BuildDecleration(string objectName, IList<IImplementation> implementations);
    }
}