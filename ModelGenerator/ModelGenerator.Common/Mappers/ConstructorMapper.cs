﻿using System;
using System.Linq;
using Mapper.Core;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Common.Mappers
{
	public class ConstructorMapper : BaseMapper<IConstructorDesignModel, IConstructor, IScope>
	{
		private readonly IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> _methodInputMapper;
		private readonly IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> _methodImplementationMapper;

		public ConstructorMapper(IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope> methodInputMapper,
			IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope> methodImplementationMapper)
		{
			_methodInputMapper          = methodInputMapper;
			_methodImplementationMapper = methodImplementationMapper;
		}

		public override IConstructor Map(IConstructorDesignModel source, IScope scope)
		{
		    if (source == null) throw new ArgumentNullException("source");
		    if (scope  == null) throw new ArgumentNullException("scope");

			var constructor = new Constructor(source.Name)
			{
				Inputs = _methodInputMapper.Map(source.Inputs, scope).ToList()
			};

			//constructor.Implementation   = _methodImplementationMapper.Map(source.Implementation);
			return constructor;
		}
	}
}