﻿using System;

namespace ModelGenerator.Domain.Exceptions
{
	public class MethodNotFoundException : Exception
	{
		public MethodNotFoundException(string methodSig)
			:base(string.Format("Method with signature: '{0}' could not be found. Have you set it and registered the object?\r\n" +
			                    "Have you made sure the signature is in format: [ObjectSignature]:[MethodSignature]",methodSig))
		{}
	}
}