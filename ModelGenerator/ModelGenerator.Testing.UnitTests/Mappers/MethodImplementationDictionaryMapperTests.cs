﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
    [TestFixture]
    public class MethodImplementationDictionaryMapperTests : BaseTest<MethodImplementationDictionaryMapper>
    {
		[Test]
	    public void Map_AddImplementationWithKeyAsResultFromScope()
	    {
		    // Arrange
		    var sut = Create<MethodImplementationDictionaryMapper>();
			var dictionary = new Dictionary<string, IMethodImplementationDesignModel>
			{
				{"Method", Mock<IMethodImplementationDesignModel>() }
			};

		    // Act
		    var result = sut.Map(dictionary, Mock<IScope>());

		    // Assert
		    var expected = Mock<IScope>().GetMethod("Method");
		    var actual = result.Keys.ElementAt(0);
			Assert.AreEqual(expected, actual);
	    }

		[Test]
	    public void Map_AddImplementationWithValueAsResultFromImplementationMapper()
	    {
		    // Arrange
		    var sut = Create<MethodImplementationDictionaryMapper>();
			var dictionary = new Dictionary<string, IMethodImplementationDesignModel>
			{
				{"Method", Mock<IMethodImplementationDesignModel>() }
			};
		    Mock<IMapper<IMethodImplementationDesignModel, IMethodImplementation, IScope>>()
			    .Map(Mock<IMethodImplementationDesignModel>(), Arg.Is<IScope>
				    (x => x.Model == Mock<IScope>().Model && x.Method == Mock<IScope>().GetMethod("Method")))
					.Returns(Mock<IMethodImplementation>());

			// Act
			var result = sut.Map(dictionary, Mock<IScope>());

		    // Assert
		    var expected = Mock<IMethodImplementation>();
			var actual = result[Mock<IScope>().GetMethod("Method")];
			Assert.AreEqual(expected, actual);
	    }
    }
}