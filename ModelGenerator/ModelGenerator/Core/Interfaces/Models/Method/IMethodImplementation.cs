﻿namespace ModelGenerator.Core.Interfaces.Models.Method
{
    public interface IMethodImplementation
    {
		// TODO: this could potentially be replaced by passing a func into the actual implementation constructor, or the object itself
		IFunction Method { get; }
        string Implementation { get; }
    }
}