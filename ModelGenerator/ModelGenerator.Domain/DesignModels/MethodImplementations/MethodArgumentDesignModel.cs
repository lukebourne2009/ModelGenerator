﻿using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels
{
    public class MethodArgumentDesignModel : DeclaredDesignModel, IMethodArgumentDesignModel
    {
        public MethodArgumentDesignModel(string name, string type)
			: base(type)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}