﻿using System;
using System.Collections.Generic;
using System.IO;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.ObjectCreator;
using ModelGenerator.Domain.Validators;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests
{
    [TestFixture]
    public class GlobalObjectManagerTests : BaseTest<GlobalObjectManager>
    {
		#region Register Object

		[Test]
        public void RegisterObject_WhenAModelIsDesignedItIsAddedToTheGlobalObjectManager()
        {
            // Arrange
            IGlobalObjectManager sut = Create<GlobalObjectManager>();

			// Act
			sut.RegisterObject(new ClassModel("Name", new Uri("C:\\Dev")));

            // Assert
            Assert.IsNotNull(sut.GetObject("Name"));
        }
        [Test]
        public void RegisterObject_ThrowExceptionOnSameObjectName()
        {   
            // Arrange
            IGlobalObjectManager sut = Create<GlobalObjectManager>();

			// Act
			sut.RegisterObject(new ClassModel("Name", new Uri("C:\\Dev")));
            TestDelegate del = () => sut.RegisterObject(new ClassModel("Name", new Uri("C:\\Other")));

            // Assert
            Assert.Throws<ArgumentException>(del);
        }
        [Test]
        public void RegisterObject_ThrowExceptionOnSameFileName()
        {
            // Arrange
            IGlobalObjectManager sut = Create<GlobalObjectManager>();

			// Act
			sut.RegisterObject(new ClassModel("Name", new Uri("C:\\Dev")));
            TestDelegate del = () => sut.RegisterObject(new ClassModel("Name", new Uri("C:\\Dev")));

            // Assert
            Assert.Throws<ArgumentException>(del);
		}

	    [Test]
		[Sequential]
	    public void RegisterObject_GivenAnObjectWithGenerics_GenerateSignature(
			[Values(0,1,2)] int nGenerics,
			[Values("IModel", "IModel<0>", "IModel<0, 1>")] string expectedSig)
	    {
		    // Arrange
			var sut = Create<GlobalObjectManager>();

			var model = new InterfaceModel("IModel");
		    for (int i = 0; i < nGenerics; i++)
		    {
			    model.Generics.Add(new Generic("GenType" + i));
		    }

			// Act
			sut.RegisterObject(model);

			// Assert
			Assert.AreEqual(model, sut.GetObject(expectedSig));
	    }

		#endregion

		#region Get Property

		[Test]
	    public void GetProperty_ThrowExceptionWhenModelDoesntExist()
	    {
		    // Arrange
		    IGlobalObjectManager sut = Create<GlobalObjectManager>();

		    // Act
			TestDelegate del = () => sut.TryGetProperty("Model.Prop");

		    // Assert
		    Assert.Throws<ArgumentException>(del);
	    }
	    [Test]
	    public void GetProperty_ReturnNullWhenPropertyDoesntExist()
	    {
		    // Arrange
		    IGlobalObjectManager sut = Create<GlobalObjectManager>();
			sut.RegisterObject(new ClassModel("Model"));

		    // Act
		    var result = sut.TryGetProperty("Model.Prop");

		    // Assert
		    Assert.IsNull(result);
	    }
	    [Test]
	    public void GetProperty_ThrowExceptionOnIncorrectSignatureSyntax()
	    {
		    // Arrange
		    IGlobalObjectManager sut = Create<GlobalObjectManager>();
			sut.RegisterObject(new ClassModel("Model"));

		    // Act
		    TestDelegate result = () => sut.TryGetProperty("Model");

		    // Assert
		    Assert.Throws<FormatException>(result);
		}

		#endregion

	    [Test]
		[Sequential]
	    public void GetObject_GivenAStringWithGenerics_ReturnGenericMatchingType(
			[Values(0, 1, 2)] int nGenerics,
		    [Values("IModel", "IModel<TArg>", "IModel<TOther, TWhat>")] string name)
	    {
			// Arrange
		    var sut = Create<GlobalObjectManager>();
			var expected = new InterfaceModel("IModel");
		    for (int i = 0; i < nGenerics; i++)
		    {
			    expected.Generics.Add(new Generic("TGenType" + i));
		    }
			sut.RegisterObject(expected);

		    // Act
		    var actual = sut.GetObject(name);

		    // Assert
		    Assert.AreEqual(expected, actual);
		}

	    #region GetModel

	    [Test]
	    public void GetModel_GivenANameThatIsntASingleWord_ThrowFormatException()
	    {
		    // Arrange
		    var sut = Create<GlobalObjectManager>();

			// Act
			TestDelegate del = () => sut.TryGetModel("two words");

		    // Assert
		    Assert.Throws<FormatException>(del);
	    }

	    [Test]
	    public void GetModel_GivenAModelDoesntExistReturnNull()
	    {
			// Arrange
		    var sut = Create<GlobalObjectManager>();

			// Act
			var result = sut.TryGetModel("NonExistentModel");

			// Assert
			Assert.IsNull(result);
	    }

	    [Test]
	    public void GetModel_GivenAModelHasBeenRegistered_ReturnModel()
	    {
		    // Arrange
		    var sut = Create<GlobalObjectManager>();
			sut.RegisterModel(new ConceptModel("ExistentModel"));

		    // Act
		    var result = sut.TryGetModel("ExistentModel");

		    // Assert
		    Assert.IsNotNull(result);
		}

	    #endregion

	    #region RegisterModel

	    [Test]
		public void RegisterModel_GivenAModel_ModelIsStored()
	    {
		    // Arrange
		    var sut = Create<GlobalObjectManager>();
			ConceptModel model = new ConceptModel("Model");

		    // Act
		    sut.RegisterModel(model);

		    // Assert
		    Assert.AreEqual(model, sut.TryGetModel("Model"));
	    }

	    #endregion
    }
}