﻿using System.Collections.Generic;
using System.Text;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.Models
{
	public class Constructor : MethodSignatured<Constructor>, IConstructor
	{
	    public Constructor(string name)
			: base(x => x.Name, x => x.Generics, x => x.Inputs)
	    {
		    Name = name;
			Inputs = new List<IMethodArgument>();
			Generics = new List<IGeneric>();
			Implementation = new NotImplementedImplementation();
		}

		public string Name { get; set; }
		public IList<IGeneric> Generics { get; set; }
		public IList<IMethodArgument> Inputs { get; set; }
		public IMethodOutput Output { get; set; }
		public IMethodImplementation Implementation { get; set; }
	}
}