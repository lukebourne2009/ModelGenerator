﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class ConstructorMapperTests : BaseTest<ConstructorMapper>
	{
		[Test]
		public void Map_UsesArgumentMapperToMapInputs()
		{
			// Arrange
			var mapper       = Mock<IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope>>();
			var sut = Create<ConstructorMapper>();
			var model        = new Scope(new DeclaredType(new ClassModel("Type")));
			var designModels = new List<IMethodArgumentDesignModel> { new MethodArgumentDesignModel("Blobl", "Type") };
			mapper.Map(designModels, model).Returns(new List<IMethodArgument>());

			// Act
			var result = sut.Map(new ConstructorDesignModel("Mapper"), model);

			// Assert
			Assert.AreEqual(mapper.Map(designModels, model), result.Inputs);
		}
	}
}