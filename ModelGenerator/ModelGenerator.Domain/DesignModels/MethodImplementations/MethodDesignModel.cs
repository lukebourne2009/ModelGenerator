﻿using System.Collections.Generic;
using System.Text;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.DesignModels
{
	public class MethodDesignModel : MethodSignatured<MethodDesignModel>, IMethodDesignModel
    {
        public MethodDesignModel(string name)
			:base(x => x.Name, x => new List<ISignatured>(), x => x.Inputs)
        {
            Name = name;
            Inputs = new List<IMethodArgumentDesignModel>();
            Output = new MethodOutputDesignModel();
        }

        public string Name { get; set; }
        public IList<IMethodArgumentDesignModel> Inputs { get; set; }
        public IMethodOutputDesignModel Output { get; set; }
        public IMethodImplementationDesignModel MethodImplementation { get; set; }
    }
}