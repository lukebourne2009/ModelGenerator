﻿using System;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
    public class MethodOutputMapperTests : BaseTest<MethodOutputMapper>
    {
        [Test]
        public void Map_InjectTheDeclaredMapperResult()
        {
            // Arrange
	        Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>()
				.Map(Arg.Any<IDeclaredDesignModel>(), Mock<IScope>())
		        .Returns(new DeclaredType(new ClassModel("Model")));

            // Act
			var result = Sut.Map(new MethodOutputDesignModel("Model"), Mock<IScope>());

            // Assert
            Assert.AreEqual("Model", result.Signature);
        }
    }
}