﻿using System.Collections.Generic;
using System.Text;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.DesignModels
{
	public class ConstructorDesignModel : MethodSignatured<ConstructorDesignModel>, IConstructorDesignModel
	{
		public ConstructorDesignModel(string name)
			: base(x => x.Name, x => new ISignatured[0], x => x.Inputs)
		{
			Name = name;
			Inputs = new List<IMethodArgumentDesignModel>();
		}

		public string Name { get; set; }
		public IList<IMethodArgumentDesignModel> Inputs { get; private set; }
		public IMethodImplementationDesignModel Implementation { get; set; }
	}
}