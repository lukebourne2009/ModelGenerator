﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Domain.ObjectCreator;
using NSubstitute;
using NUnit.Framework;

namespace ModelGenerator.Testing.UnitTests
{
    [TestFixture]
    public class InterfaceCreatorTests
    {
        [Test]
        public void Constructor_ThrowOnNullReference()
        {
            // Arrange
            TestDelegate del = () => new InterfaceCreator(null,null, null,null,null);

            // Act

            // Assert
            Assert.Throws<ArgumentNullException>(del);
        }
        [Test]
        public void CreateObject_ThrowOnNullReference()
        {
            // Arrange
            IObjectCreator sut = CreateInterfaceCreator(Substitute.ForPartsOf<Stream>());

            // Act
            TestDelegate del = () => sut.CreateObject(null);

            // Assert
            Assert.Throws<ArgumentNullException>(del);
        }
        [Test]
        public void CreateObject_GivenABogStandardModelReturnCorrectFormat()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream);

            // Act
            IObjectModel result = sut.CreateObject(new InterfaceModel("IMapper", new Uri("C:\\Dev")));

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper\r\n" +
                                                             "{\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }

        [Test]
        public void SetMethod_GivenSetMethodIsCalledMethodBuiltOnCallToCreateObject()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream, "void");

            // Act
            IObjectModel result = sut.SetMethod(new InterfaceModel("IMapper", new Uri("C:\\Dev")), new MethodDesignModel("Name"));
            result = sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper\r\n" +
                                                             "{\r\n" +
                                                             "void Name();\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetMethod_GivenMethodHasAnArgumentItIsAddedCorrectly()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream, "void", "int");

            // Act
            var model = new MethodDesignModel("Name");
            model.Inputs.Add(new MethodArgumentDesignModel("arg","int"));
            IObjectModel result = sut.SetMethod(new InterfaceModel("IMapper", new Uri("C:\\Dev")), model);
            result = sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper\r\n" +
                                                             "{\r\n" +
                                                             "void Name(int arg);\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetMethod_GivenMethodHasTwoArgumentsTheyAreAddedCorrectly()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream, "void", "int");

            // Act
            var model = new MethodDesignModel("Name");
            model.Inputs.Add(new MethodArgumentDesignModel("arg", "int"));
            model.Inputs.Add(new MethodArgumentDesignModel("arg2", "int"));
            IObjectModel result = sut.SetMethod(new InterfaceModel("IMapper", new Uri("C:\\Dev")), model);
            result = sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper\r\n" +
                                                             "{\r\n" +
                                                             "void Name(int arg, int arg2);\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetMethod_GivenMethodHasAReturnTypeItIsReturnedCorrectly()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream, "int");

            // Act
            var model = new MethodDesignModel("Name")
            {
                Output = new MethodOutputDesignModel { Type = "int" }
            };
            IObjectModel result = sut.SetMethod(new InterfaceModel("IMapper", new Uri("C:\\Dev")), model);
            sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper\r\n" +
                                                             "{\r\n" +
                                                             "int Name();\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetMethod_Given2ArgumentsHaveTheSameNameThrowAnException()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            InterfaceCreator sut = CreateInterfaceCreator(stream);

            // Act
            var model = new MethodDesignModel("Name");
            model.Inputs.Add(new MethodArgumentDesignModel("arg", "int"));
            model.Inputs.Add(new MethodArgumentDesignModel("arg", "int"));
            TestDelegate del = () => sut.SetMethod(new InterfaceModel("Mapper", null), model);

            // Assert
            Assert.Throws<ArgumentException>(del);
        }

        [Test]
        public void SetGeneric_GivenSetGenericIsCalledAGenericIsAddedOnCallToCreateObject()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            IObjectCreator sut = CreateInterfaceCreator(stream);

            // Act
            IObjectModel result = sut.SetGeneric(new InterfaceModel("IMapper", new Uri("C:\\Dev")), "TObject");
            result = sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper<TObject>\r\n" +
                                                             "{\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetGeneric_GivenSetGenericIsCalledTwiceTwoGenericsAreCreated()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            byte[] bytes = null;
            stream.When(x => x.Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>()))
                .Do(x => bytes = x.ArgAt<byte[]>(0));
            IObjectCreator sut = CreateInterfaceCreator(stream);

            // Act

            IObjectModel result = sut.SetGeneric(new InterfaceModel("IMapper", new Uri("C:\\Dev")), "TObject");
            result = sut.SetGeneric(result, "TEntity");
            result = sut.CreateObject(result);

            // Assert
            Assert.AreEqual(Encoding.ASCII.GetString(bytes), "public interface IMapper<TObject, TEntity>\r\n" +
                                                             "{\r\n" +
                                                             "\r\n" +
                                                             "}\r\n");
        }
        [Test]
        public void SetGeneric_GivenSetGenericIsCalledTwiceWithTheSameName_ThrowException()
        {
            // Arrange
            Stream stream = Substitute.ForPartsOf<Stream>();
            IObjectCreator sut = CreateInterfaceCreator(stream);

            // Act
            IObjectModel result = sut.SetGeneric(new InterfaceModel("Mapper", null), "TEntity");
            TestDelegate del = () => sut.SetGeneric(result, "TEntity");

            // Assert
            Assert.Throws<ArgumentException>(del);
        }


        private static InterfaceCreator CreateInterfaceCreator(Stream stream, string outputReturn = null, string arg = null)
        {
            var fileCreator = Substitute.For<IFileCreator>();
            fileCreator.CreateFile(Arg.Any<string>()).Returns(stream);
            IFormatter braceFormatter = Substitute.For<IFormatter>();
            braceFormatter.Format(Arg.Any<string>()).Returns(x => x.ArgAt<string>(0));

			var methodOutputMapper = Substitute.For<IMapper<IMethodOutputDesignModel, IMethodOutput, IScope>>();
			methodOutputMapper.Map(Arg.Any<IMethodOutputDesignModel>(), Arg.Any<IScope>()).Returns(new MethodOutput(new ClassModel(outputReturn)));

            var methodArgumentMapper = Substitute.For<IMapper<IMethodArgumentDesignModel, IMethodArgument, IScope>>();
			methodArgumentMapper.Map(Arg.Any<IEnumerable<IMethodArgumentDesignModel>>(), Arg.Any<IScope>())
                .Returns(x =>
                {
                    var list = new List<IMethodArgument>();
                    for (var i = 0; i < x.ArgAt<IEnumerable<IMethodArgumentDesignModel>>(0).Count(); i++)
                    {
                        list.Add(new MethodArgument(new ClassModel(arg), x.ArgAt<IEnumerable<IMethodArgumentDesignModel>>(0).ElementAt(i).Name));
                    }
                    return list;
                });

            var sut = new InterfaceCreator(fileCreator, braceFormatter, 
                Substitute.For<IGlobalObjectManager>(),
                methodArgumentMapper,
                methodOutputMapper);
            return sut;
        }
    }
}