﻿using System;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Testing.BehaviourTests;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
    [TestFixture]
    public class MethodArgumentMapperTests : BaseTest<MethodArgumentMapper>
    {
        [Test]
        public void Map_GivenAMethodName_SetNameCorrectly()
        {
            // Arrange
	        var sut = Create<MethodArgumentMapper>();

			// Act
			var result = sut.Map(new MethodArgumentDesignModel("arg1", "Model"), Mock<IScope>());

            // Assert
            Assert.AreEqual("arg1", result.Name);
        }
        [Test]
        public void Map_GivenDeclaredMapperReturnsModel_SetArgumentType()
        {
            // Arrange
            var declaredType = new DeclaredType(new ClassModel("Model"));
	        Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>().Map(Arg.Any<IDeclaredDesignModel>(), Arg.Any<IScope>())
                .Returns(declaredType);

            // Act
			var result = Sut.Map(new MethodArgumentDesignModel("arg1", "Model"), new NoScope());

            // Assert
            Assert.AreEqual(declaredType.Signature, result.DeclaredType.Signature);
        }
	}
}