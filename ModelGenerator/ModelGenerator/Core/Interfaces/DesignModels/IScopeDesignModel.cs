﻿namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface IScopeDesignModel
	{
		IDeclaredDesignModel Model { get; }
		string Method { get; }
	}
}