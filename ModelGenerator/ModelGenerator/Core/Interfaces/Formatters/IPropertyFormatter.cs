﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Formatters
{
    public interface IPropertyFormatter
    {
        string BuildProperties(IEnumerable<IProperty> methods, IEnumerable<IProperty> implementationProperties);
    }
}