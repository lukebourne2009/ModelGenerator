﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.ModelCreator;
using ModelGenerator.Core.Interfaces.Settings;
using ModelGenerator.Core.Interfaces.Factories;
using ModelGenerator.Core.Interfaces.Helpers;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Core.Settings;
using ModelGenerator.Domain.Models.ObjectModels;

namespace ModelGenerator.Domain.ObjectCreator
{
	public class ModelCreator : IModelCreatorExposedModels
    {
        private readonly IObjectCreator _classCreator;
        private readonly IObjectCreator _interfaceCreator;
	    private readonly IObjectTemplateBuilder _templateBuilder;
	    private readonly IPropertyDesignModelFactory _propertyModelFactory;
        private readonly IGlobalSettings _globalSettings;
        private readonly IGlobalObjectManager _globalObjectManager;
	    private readonly IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> _mappingMapper;

	    private IModel _model;
		public IModel Model { get { return _model; } }
	    public string ModelName { get { return _model.Name; } }

		public ModelCreator(IObjectCreator classCreator, 
            IObjectCreator interfaceCreator,
			IObjectTemplateBuilder templateBuilder,
            IPropertyDesignModelFactory propertyModelFactory,
            IGlobalSettings globalSettings,
            IGlobalObjectManager globalObjectManager,
	        IMapper<IPropertyMappingDesignModel, IPropertyMapping, IScope> mappingMapper)
        {
            _classCreator = classCreator;
            _interfaceCreator = interfaceCreator;
	        _templateBuilder = templateBuilder;
	        _propertyModelFactory = propertyModelFactory;
            _globalSettings = globalSettings;
            _globalObjectManager = globalObjectManager;
	        _mappingMapper = mappingMapper;
        }

	    public void Build(Uri outPathUri)
        {
            if (outPathUri == null) throw new ArgumentNullException("outPathUri");

	        Model.ViewModel.Name = ModelName + "ViewModel";
	        Model.ViewModel.FilePath = outPathUri;
			_classCreator.CreateObject(Model.ViewModel);

	        Model.Model.Name = ModelName + "Model";
	        Model.Model.FilePath = outPathUri;
			_classCreator.CreateObject(Model.Model);

	        Model.Entity.Name = ModelName + "Entity";
	        Model.Entity.FilePath = outPathUri;
			_classCreator.CreateObject(Model.Entity);

	        var iModelMapper = _templateBuilder.BuildIModelMapper();
	        iModelMapper.FilePath = new Uri(Path.Combine(outPathUri.LocalPath, "Core\\Interfaces"));
			_interfaceCreator.CreateObject(iModelMapper);

	        var modelMapper = _templateBuilder.BuildModelMapper(ModelName);
	        modelMapper.FilePath = new Uri(Path.Combine(outPathUri.LocalPath, "Common\\Mappers"));
			_classCreator.CreateObject(modelMapper);
		}
        public void Build(string outPathUri)
        {
	        if (outPathUri == null) throw new ArgumentNullException("outPathUri");
            Build(new Uri(outPathUri));
        }
        public IModelCreator CreateProperty(string propertyName, string typeName)
		{
			return CreateProperty(propertyName, typeName, PropertyDesignSettings.Default);
        }
        public IModelCreator CreateProperty(string propertyName, string typeName, IPropertyDesignSettings propertyDesignSettings)
        {
	        if (propertyDesignSettings == null) throw new ArgumentNullException("propertyDesignSettings");
	        if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("propertyName", "Value cannot be null or empty.");
	        if (string.IsNullOrEmpty(typeName)) throw new ArgumentNullException("typeName", "Value cannot be null or empty.");

	        _classCreator.SetProperty(Model.Entity, CreateEntity(propertyName, typeName, propertyDesignSettings));
			_classCreator.SetProperty(Model.ViewModel, CreateViewModel(propertyName, typeName, propertyDesignSettings));
			_classCreator.SetProperty(Model.Model, CreateModel(propertyName, typeName, propertyDesignSettings));

            return this;
        }
        public IModelCreator EditGlobalSettings(Action<IGlobalSettings> action)
        {
            if (action == null) throw new ArgumentNullException("action");

            action(_globalSettings);
            return this;
        }

        public IViewModelPropertyMapperContext AndMapToViewModelProperty(string named)
        {
	        if (named == null) throw new ArgumentNullException("named");

	        if (named == string.Empty || new Regex("^[\\d\\w]+$").Match(named).Value != named)
                throw new ArgumentException("input has to match the regex format: ^[\\d\\w]+$ ");

            return new ViewModelPropertyMapperContext(named, 
				this, 
				_classCreator, 
				_interfaceCreator, 
				_templateBuilder, 
				_globalObjectManager, 
				_mappingMapper);
        }

	    public IModelCreator Start(string modelName)
	    {
		    if (string.IsNullOrEmpty(modelName)) throw new ArgumentNullException("modelName", "Value cannot be null or empty.");

			_model = new ConceptModel(modelName);

			_globalObjectManager.RegisterObject(_model.Entity);
			_globalObjectManager.RegisterObject(_model.Model);
			_globalObjectManager.RegisterObject(_model.ViewModel);

			_globalObjectManager.RegisterModel(_model);
		    
		    return this;
	    }

	    #region Privates

        private IPropertyDesignModel CreateEntity(string propertyName, string typeName, IPropertyDesignSettings propertyDesignSettings)
        {
            return _propertyModelFactory.Create(propertyName, typeName, propertyDesignSettings);
        }
        private IPropertyDesignModel CreateModel(string propertyName, string typeName, IPropertyDesignSettings propertyDesignSettings)
        {
            var savedSetting = propertyDesignSettings.Required;
            propertyDesignSettings.Required = false;
            IPropertyDesignModel propertyDesignModel = _propertyModelFactory.Create(propertyName, typeName, propertyDesignSettings);
            propertyDesignSettings.Required = savedSetting;
            return propertyDesignModel;
        }
        private IPropertyDesignModel CreateViewModel(string propertyName, string typeName, IPropertyDesignSettings propertyDesignSettings)
        {
            return _propertyModelFactory.Create(propertyName, typeName, propertyDesignSettings);
        }
        #endregion
    }
}