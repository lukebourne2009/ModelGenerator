﻿namespace ModelGenerator.Core.Interfaces.Formatters
{
    public interface IFormatter
    {
        string Format(string classString);
    }

    public interface IFormatter<out TOut, in TIn1, in TIn2>
    {
        TOut Format(TIn1 in1, TIn2 in2);
    }
}