﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Domain.Models
{
    public class Generic : IGeneric
    {
        public Generic(string type)
        {
            Name = type;
			Generics = new List<IGeneric>();
        }
        public string Name { get; set; }
	    public string Signature
	    {
		    get { return Name; }
	    }
	    public IList<IGeneric> Generics { get; set; }
    }
}