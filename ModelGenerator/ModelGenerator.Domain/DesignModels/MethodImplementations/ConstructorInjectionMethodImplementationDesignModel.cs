﻿using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;

namespace ModelGenerator.Domain.DesignModels.MethodImplementations
{
	public class ConstructorInjectionMethodImplementationDesignModel : IMethodImplementationDesignModel
	{
		public ConstructorInjectionMethodImplementationDesignModel(IScopeDesignModel scope)
		{
			Scope = scope;
		}
		public ConstructorInjectionMethodImplementationDesignModel(IScopeDesignModel scope, string methodSig)
		{
			Scope = scope;
			MethodSignature = methodSig;
		}

		// TODO: feels like this should use either method OR scope but not both
		public string MethodSignature { get; private set; }
		public IScopeDesignModel Scope { get; private set; }
		public IFunction Method { get; set; }
	}
}