﻿using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Domain.ClassModels
{
    public class GenericArgument : DeclaredType, IGenericArgument
    {
	    public GenericArgument(ITypeModel type) 
			: base(type)
	    {}

	    public GenericArgument(IDeclaredType declaredType) 
			: base(declaredType)
	    {}
    }
}