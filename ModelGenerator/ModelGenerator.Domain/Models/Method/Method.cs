﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain.Signatures;

namespace ModelGenerator.Domain.Models.Method
{
    public class Method : MethodSignatured<Method>, IMethod
    {
        public Method(string name)
			:base(x => x.Name, x => x.Generics, x => x.Inputs.Select(y => y.DeclaredType))
        {
            Name = name;
			Generics = new List<IGeneric>();
			Inputs = new List<IMethodArgument>();
        }

        public string Name { get; set; }
	    public IList<IGeneric> Generics { get; set; }
	    public IList<IMethodArgument> Inputs { get; set; }
        public IMethodOutput Output { get; set; }
        public IMethodImplementation Implementation { get; set; }
    }
}