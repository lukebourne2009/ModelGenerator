﻿using System;
using System.Linq;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.ClassModels;
using Attribute = ModelGenerator.Domain.Models.Attribute;

namespace ModelGenerator.Common.Mappers
{
	public class PropertyMapper : BaseMapper<IPropertyDesignModel, IProperty, IScope>
	{
		private readonly IMapper<IDeclaredDesignModel, IDeclaredType, IScope> _declaredTypeMapper;

		public PropertyMapper(IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredTypeMapper)
		{
			_declaredTypeMapper = declaredTypeMapper;
		}

		public override IProperty Map(IPropertyDesignModel source, IScope scope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (scope == null) throw new ArgumentNullException("scope");

			return new Property(source.Name, _declaredTypeMapper.Map(source, scope))
			{
				Attributes = source.Attributes.Select(x => new Attribute(x.Name) as IAttribute).ToList()
			};
		}
	}
}