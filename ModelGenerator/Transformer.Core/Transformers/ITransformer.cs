﻿namespace ModelGenerator.Core.Interfaces.Factories
{
    public interface ITransformer<in TSource, out TTarget>
    {
        TTarget Transform(TSource source);
    }
}