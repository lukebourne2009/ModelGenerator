﻿using System;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Common;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Testing.BehaviourTests;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
    [TestFixture]
    public class ImplementationMapperTests : BaseTest<ImplementationMapper>
    {
        [Test]
        public void Map_GivenAModelWithAMethod_AndNoImplementation_MethodsImplementationIsThrow()
        {
            // Arrange
            Dictionary<IMethod, IMethodImplementation> methodImplementations;
            var methodImplementationsMapper = CreateMethodImplementationsMaper(out methodImplementations);

            var mapper = CreateMapper(mapper: methodImplementationsMapper);

            var interfaceModel = new InterfaceModel("MyInterface", new Uri("C:\\"));
            var implementationDesignModel = new ImplementationDesignModel(interfaceModel);

            // Act
			IImplementation result = mapper.Map(implementationDesignModel, new Scope(new DeclaredType(new ClassModel("int"))));

            // Assert
            Assert.AreEqual(methodImplementations, result.MethodImplementations);
        }

		private static IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope>
            CreateMethodImplementationsMaper(out Dictionary<IMethod, IMethodImplementation> methodImplementations)
        {
            var methodMapper = Substitute
				.For<IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope>
                >();
            methodImplementations = new Dictionary<IMethod, IMethodImplementation>();
			methodMapper.Map(Arg.Any<IDictionary<string, IMethodImplementationDesignModel>>(), Arg.Any<IScope>()).Returns(methodImplementations);
            return methodMapper;
        }
		private static ImplementationMapper CreateMapper(IMapper<IDeclaredDesignModel, IDeclaredType, IScope> declaredTypeMapper = null,
			IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope> mapper = null)
        {
			return new ImplementationMapper(mapper ?? Substitute.For<IMapper<IDictionary<string, IMethodImplementationDesignModel>, IDictionary<IMethod, IMethodImplementation>, IScope>>(),
				declaredTypeMapper ?? Substitute.For<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>());
        }
        private static InterfaceModel CreateInterfaceModel(string[] gens, string[] genArgs, 
            out ImplementationDesignModel implementationDesignModel)
        {
            var interfaceModel = new InterfaceModel("MyInterface", new Uri("C:\\"));
            foreach (string gen in gens)
            {
                interfaceModel.Generics.Add(new Generic(gen));
            }

            implementationDesignModel = new ImplementationDesignModel(interfaceModel);
            for (int i = 0; i < genArgs.Length; i++)
            {
                implementationDesignModel.GenericArguments
                    .Add(gens[i], new GenericArgumentDesignModel(genArgs[i]));
            }

            return interfaceModel;
        }
    }
}