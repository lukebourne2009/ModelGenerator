﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Core.Interfaces.Formatters
{
    public interface IGenericFormatter : IFormatter<string, string, IObjectModel>
    {}
}