﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
    [TestFixture]
    public class ObjectDeclerationFormatterTests : BaseTest<ObjectDeclerationFormatter>
    {
        [Test]
        public void BuildDecleration_GivenAClassName_ReturnBasicDecleration()
        {
            // Arrange
            IObjectDeclerationFormatter sut = Create<ObjectDeclerationFormatter>();

            // Act
            var result = sut.BuildDecleration("Model", new List<IImplementation>());

            // Assert
            Assert.AreEqual("public class Model\r\n", result);
        }        

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void BuildDecleration_GivenAnImplementation_IncludeItInDecleration(int amount)
        {
            // Arrange
            IObjectDeclerationFormatter sut = Create<ObjectDeclerationFormatter>();

            // Act
            var implementationDesignModels = new List<IImplementation>();
            for (int i = 0; i < amount; i++)
            {
                implementationDesignModels.Add(new Implementation(new DeclaredType(new InterfaceModel("IBlogs" + i, null))));
            }
            var result = sut.BuildDecleration("Model", implementationDesignModels);

            // Assert
            var implements = "IBlogs0";
            for (int i = 1; i < amount; i++)
            {
                implements += (", IBlogs" + i);
            }
            Assert.AreEqual("public class Model : " + implements + "\r\n", result);
        }

        [Test]
        public void BuildDecleration_GivenAnImplementationWithGenerics_ReturnCorrectDecleration()
        {
            // Arrange
            IObjectDeclerationFormatter sut = Create<ObjectDeclerationFormatter>();

            // Act
            InterfaceModel interfaceModel = new InterfaceModel("IModel")
            {
                Generics = new List<IGeneric>
                {
                    new Generic("Blogs")
                }
            };
            var result = sut.BuildDecleration("Model", new List<IImplementation>
            {
                new Implementation(new DeclaredType(interfaceModel)
                {
                    GenericArguments = new Dictionary<IGeneric, IDeclaredType>
                    {
                        {interfaceModel.Generics[0], new GenericArgument(new ClassModel("Bob"))}
                    }
                })
            });

            // Assert
            Assert.AreEqual("public class Model : IModel<Bob>\r\n", result);
        }

        [Test]
        public void BuildDecleration_GivenAnImplementationWith2Generics_ReturnCorrectDecleration()
        {
            // Arrange
            IObjectDeclerationFormatter sut = Create<ObjectDeclerationFormatter>();

            // Act
            InterfaceModel interfaceModel = new InterfaceModel("IModel")
            {
                Generics = new List<IGeneric>
                {
                    new Generic("Blogs"),
                    new Generic("Jobs")
                }
            };
            var result = sut.BuildDecleration("Model", new List<IImplementation>
            {
                new Implementation(new DeclaredType(interfaceModel)
                {
                    GenericArguments = new Dictionary<IGeneric, IDeclaredType>
                    {
                        {interfaceModel.Generics[0], new GenericArgument(new ClassModel("Bob"))},
                        {interfaceModel.Generics[1], new GenericArgument(new ClassModel("Bib"))}
                    }
                })
            });

            // Assert
            Assert.AreEqual("public class Model : IModel<Bob, Bib>\r\n", result);
        }

        [Test]
        public void BuildDecleration_Given2ImplementationsWith2Generics_ReturnCorrectDecleration()
        {
            // Arrange
            IObjectDeclerationFormatter sut = Create<ObjectDeclerationFormatter>();

            // Act
            InterfaceModel interfaceModel = new InterfaceModel("IModel", null)
            {
                Generics = new List<IGeneric>
                {
                    new Generic("Blogs"),
                    new Generic("Jobs")
                }
            };
            InterfaceModel interfaceModel2 = new InterfaceModel("IOtherModel", null)
            {
                Generics = new List<IGeneric>
                {
                    new Generic("Blogs"),
                    new Generic("Jobs")
                }
            };
            var result = sut.BuildDecleration("Model", new List<IImplementation>
            {
                new Implementation(new DeclaredType(interfaceModel)
                {
                    GenericArguments = new Dictionary<IGeneric, IDeclaredType>
                    {
                        {interfaceModel.Generics[0], new GenericArgument(new ClassModel("Bob"))},
                        {interfaceModel.Generics[1], new GenericArgument(new ClassModel("Bib"))}
                    }
                }),
                new Implementation(new DeclaredType(interfaceModel2)
                {
                    GenericArguments = new Dictionary<IGeneric, IDeclaredType>
                    {
                        {interfaceModel2.Generics[0], new GenericArgument(new ClassModel("Bob"))},
                        {interfaceModel2.Generics[1], new GenericArgument(new ClassModel("Bib"))}
                    }
                })
            });

            // Assert
            Assert.AreEqual("public class Model : IModel<Bob, Bib>, IOtherModel<Bob, Bib>\r\n", result);
        }        
    }
}