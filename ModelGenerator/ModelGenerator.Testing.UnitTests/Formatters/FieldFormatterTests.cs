﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Testing.BehaviourTests;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
	public class FieldFormatterTests : BaseTest<FieldFormatter>
	{
		[Test]
		[TestCaseSource("BuildFields_GivenNumberOfFields_ReturnCorrectResult_TestData")]
		public string BuildFields_GivenNumberOfFields_ReturnCorrectResult(IList<IField> fields)
		{
			// Arrange
			var sut = Create<FieldFormatter>();

			// Act
			var result = sut.BuildFields(fields);

			// Assert
			return result;
		}

		[Test(ExpectedResult = "private readonly Type arg1;\r\n\r\n")]
		public string BuildFields_AFieldWithReadonly_AddReadonlyPrefix()
		{
			// Arrange
			var sut = Create<FieldFormatter>();

			// Act
			var result = sut.BuildFields(new List<IField>
			{
				new Field(new ClassModel("Type"), "arg1")
				{
					Readonly = true
				}
			});

			// Assert
			return result;
		}

		private static IEnumerable BuildFields_GivenNumberOfFields_ReturnCorrectResult_TestData()
		{
			yield return new TestCaseData(new List<IField>())
			{
				TestName = "BuildFields: Given no fields return an empty string"
			}.Returns("");
			yield return new TestCaseData(new List<IField>
			{
				new Field(new ClassModel("int"), "arg")
			})
			{
				TestName = "BuildFields: Given 1 fields return single field"
			}.Returns("private int arg;\r\n\r\n");
			yield return new TestCaseData(new List<IField>
			{
				new Field(new ClassModel("int"), "arg"),
				new Field(new ClassModel("string"),"arg2")
			})
			{
				TestName = "BuildFields: Given 2 fields return 2 fields seperates by a line break"
			}.Returns("private int arg;\r\nprivate string arg2;\r\n\r\n");
		}

	}
}