﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Mapping;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class PropertyMappingMapperTests : BaseTest<PropertyMappingMapper>
	{
		[Test]
		public void Map_ReturnsNonNull()
		{
			// Arrange
			var sut = Create<PropertyMappingMapper>();

			// Act
			var result = sut.Map(new PropertyMappingDesignModel("blob", new DeclaredDesignModel("Source")), Mock<IScope>());

			// Assert
			Assert.IsNotNull(result);
		}

		[Test]
		public void Map_ResultOfDeclaredMapperIsSetAsSourceModel()
		{
			// Arrange
			var sut = Create<PropertyMappingMapper>();
			var design = new DeclaredDesignModel("Source");

			// Act
			var result = sut.Map(new PropertyMappingDesignModel("blob", design), Mock<IScope>());

			// Assert
			Assert.AreEqual(result.SourceModel, Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>().Map(design, Mock<IScope>()));
		}

		[Test]
		public void Map_GivenSourceIsSet_UseThatAsSourceForPropertyMapper()
		{
			// Arrange
			var sut = Create<PropertyMappingMapper>();

			// Act
			var result = sut.Map(new PropertyMappingDesignModel("blob", new DeclaredDesignModel("Source"))
			{
				Source = Mock<IDeclaredType>()
			}, Mock<IScope>());

			// Assert
			Assert.AreEqual(result.SourceModel, Mock<IDeclaredType>());
		}		
		[Test]
		public void Map_GivenSourceIsNotSet_UseSourceDesignAsParameterForDeclaredMapper()
		{
			// Arrange
			var sut = Create<PropertyMappingMapper>();
			DeclaredDesignModel designModel = new DeclaredDesignModel("Source");

			// Act
			var result = sut.Map(new PropertyMappingDesignModel("blob", designModel)
			{
				Source = null
			}, Mock<IScope>());

			// Assert
			Assert.AreEqual(
				Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>().Map(designModel, Mock<IScope>()), 
				result.SourceModel);
		}

		[Test]
		[TestCaseSource("Map_GivenAPropertyName_MapItCorrectly_TestData", new object[] { 0 })]
		[TestCaseSource("Map_GivenAPropertyName_MapItCorrectly_TestData", new object[] { 1 })]
		[TestCaseSource("Map_GivenAPropertyName_MapItCorrectly_TestData", new object[] { 2 })]
		[TestCaseSource("Map_GivenAPropertyName_MapItCorrectly_TestData", new object[] { 3 })]
		public void Map_GivenAPropertyName_MapItCorrectly(IList<string> propNames, Action<IPropertyMapping> assertion)
		{
			// Arrange
			var sut = Create<PropertyMappingMapper>();
			PropertyMappingDesignModel design = new PropertyMappingDesignModel("blob", new DeclaredDesignModel("Source"));
			design.UsingProperties = propNames;

			// Act
			IPropertyMapping result = sut.Map(design, Mock<IScope>());

			// Assert
			assertion(result);
		}

		private static IEnumerable Map_GivenAPropertyName_MapItCorrectly_TestData(int nProps)
		{
			IList<string> names = new List<string>();
			for (int i = 0; i < nProps; i++)
			{
				names.Add("Prop" + i);
			}

			yield return new TestCaseData(names, (Action<IPropertyMapping>)(x => Assert.AreEqual(x.UsingProperties.Count, nProps)));
		}
	}
}