﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using ModelGenerator.Domain.Exceptions;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.Method.Implementations;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class MethodImplementationMapperTests : BaseTest<MethodImplementationMapper>
	{
		[Test]
		public void Map_GivenAConstructorInjection_ReturnNonNull()
		{
			// Arrange
			MethodImplementationMapper sut = Create<MethodImplementationMapper>();

			// Act
			var result = sut.Map(new ConstructorInjectionMethodImplementationDesignModel(
				new ScopeDesignModel(new DeclaredDesignModel("Model"),"Method"), "Method"), Mock<IScope>());

			// Assert
			Assert.IsNotNull(result);
		}
	}
}