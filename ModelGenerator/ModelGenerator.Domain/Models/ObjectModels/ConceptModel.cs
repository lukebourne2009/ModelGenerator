﻿using ModelGenerator.Core.Interfaces.ClassModels;

namespace ModelGenerator.Domain.Models.ObjectModels
{
	public class ConceptModel : IModel
	{
		public ConceptModel(string name)
		{
			Name = name;
			Entity = new ClassModel(name + "Entity");
			Model = new ClassModel(name + "Model");
			ViewModel = new ClassModel(name + "ViewModel");
		}

		public string Name { get; private set; }
		public IObjectModel ViewModel { get; set; }
		public IObjectModel Model { get; set; }
		public IObjectModel Entity { get; set; }
	}
}