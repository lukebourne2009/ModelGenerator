﻿using System;
using System.Linq;
using Mapper.Core;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain.Models;

namespace ModelGenerator.Common.Mappers
{
	public class DeclaredTypeMapper : BaseMapper<IDeclaredDesignModel, IDeclaredType, IScope>
	{
		private readonly IGlobalObjectManager _manager;

		public DeclaredTypeMapper(IGlobalObjectManager manager)
		{
			_manager = manager;
		}

		public override IDeclaredType Map(IDeclaredDesignModel source, IScope scope)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (scope  == null) throw new ArgumentNullException("scope");

			ITypeModel model = _manager.GetObject(source.Signature);

			if(model == null)
			{
				var declaredGenArg = scope.GetGenericArgument(source.Signature);
				if (declaredGenArg == null)
					throw new InvalidOperationException(
						string.Format("No declared type could be found for generic '{0}' in scope", source.Signature));

				return declaredGenArg;
			}

			var declaredModel = new DeclaredType(model);
			foreach (var generic in model.Generics)
			{
				var genericArgument = source.GenericArguments[generic.Signature];
				declaredModel.GenericArguments.Add(generic, Map(genericArgument, scope));
			}
			return declaredModel;
		}
	}
}