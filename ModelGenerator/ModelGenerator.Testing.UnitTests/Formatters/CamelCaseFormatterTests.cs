﻿using System;
using ModelGenerator.Domain.Formatter;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
	[TestFixture]
	public class CamelCaseFormatterTests : BaseTest<CamelCaseFormatter>
	{
		[TestCase("Bob", ExpectedResult = "bob")]
		[TestCase("UPPER", ExpectedResult = "uPPER")]
		[TestCase("lower", ExpectedResult = "lower")]
		[TestCase("A", ExpectedResult = "a")]
		public string Format_GivenInputProduceCorrectOutput(string input)
		{
			// Arrange
			var sut = Create<CamelCaseFormatter>();

			// Act
			var result = sut.Format(input);

			// Assert
			return result;
		}
		[Test]
		public void Format_GivenInputOfEmptyString_ThrowArgumentException()
		{
			// Arrange
			var sut = Create<CamelCaseFormatter>();

			// Act
			TestDelegate result = () => sut.Format("");

			// Assert
			Assert.Throws<ArgumentException>(result);
		}
	}
}