﻿using System;
using System.Collections;
using Mapper.Core;
using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Core.Interfaces.Scopes;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.ObjectModels;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Mappers
{
	[TestFixture]
	public class ScopeMapperTests : BaseTest<ScopeMapper>
	{
		[Test]
		public void Map_ResultFromTypeMapperIsUsedForScopeModel()
		{
			// Arrange
			var sut = Create<ScopeMapper>();
			DeclaredDesignModel designModel = new DeclaredDesignModel("Model");
			DeclaredType declaredType = new DeclaredType(new ClassModel("Model"));
			Mock<IMapper<IDeclaredDesignModel, IDeclaredType, IScope>>()
				.Map(designModel, Arg.Any<NoScope>()).Returns(declaredType);

			// Act
			var result = sut.Map(new ScopeDesignModel(designModel, "Method"));

			// Assert
			Assert.AreEqual(declaredType, result.Model);

		}		
		[Test]
		public void Map_ResultFromManagerIsUsedForMethodScope()
		{
			// Arrange
			var sut = Create<ScopeMapper>();

			// Act
			var result = sut.Map(new ScopeDesignModel(new DeclaredDesignModel("Model"), "Method"));

			// Assert
			Assert.AreEqual(Mock<IGlobalObjectManager>().GetMethod("Model.Method"), result.Method);
		}
	}
}