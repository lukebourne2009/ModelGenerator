﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NSubstitute;
using NUnit.Framework;

namespace ModelGenerator.Testing.BehaviourTests
{
    public class TestHelper
    {
        public static string TestDirectory = ConfigurationManager.AppSettings["TestFileDirectory"];

        public static void AssertFileMatchesTemplate(string file, string template)
        {
            using (FileStream stream = File.Open(file, FileMode.Open))
            {
                byte[] text = new byte[(int)stream.Length];
                stream.Read(text, 0, (int)stream.Length);
                string fileString = Encoding.ASCII.GetString(text);
                Assert.AreEqual(template, fileString);
            }
        }

	    public static IEnumerable NullParameterChecks(Type sutType)
	    {
		    List<TestCaseData> tests = new List<TestCaseData>();
			var methods = sutType.GetMethods();

		    foreach (var method in methods)
		    {
				if(method.DeclaringType.IsAbstract)
					continue;

			    var parameterInfos = method.GetParameters();
			    for (var index = 0; index < parameterInfos.Length; index++)
			    {
				    var parameterInfo = parameterInfos[index];
				    Action test = () =>
				    {
					    var sut = CreateSystemUnderTest(method);
					    Assert.Throws<ArgumentNullException>(() =>
					    {
						    try
						    {
							    method.Invoke(sut, GenerateParameters(parameterInfos, parameterInfo));
						    }
						    catch (TargetInvocationException e)
						    {
							    if (e.InnerException != null) throw e.InnerException;
						    }
					    });
				    };
				    tests.Add(new TestCaseData(test)
				    {
					    TestName = string.Format("{2}: passing null for parameter {0} of type {1}, throws an exception.",
							index + 1, parameterInfo.ParameterType.Name, method.Name)
				    });
			    }
		    }
		    return tests;
	    }
		private static object CreateSystemUnderTest(MethodInfo method)
		{
			object[] sutParameters = {};
			var constructorInfos = method.DeclaringType.GetConstructors();

			if(constructorInfos.Any())
			{
				var sutConstructor = constructorInfos[0];
				var sutConstructorParameters = sutConstructor.GetParameters();

				sutParameters = GenerateParameters(sutConstructorParameters);
			}

			var classInstance = Activator.CreateInstance(method.DeclaringType, sutParameters);
			return classInstance;
		}
	    private static object[] GenerateParameters(ParameterInfo[] sutConstructorParameters, ParameterInfo excludedParameter = null)
	    {
		    object[] sutParameters = new object[sutConstructorParameters.Length];
		    for (int i = 0; i < sutConstructorParameters.Length; i++)
		    {
			    if (excludedParameter != null && sutConstructorParameters[i].Equals(excludedParameter))
			    {
				    sutParameters[i] = null;
			    }
			    else
			    {
					var mockParameter = Substitute.For(new[] { sutConstructorParameters[i].ParameterType }, new object[0]);
				    sutParameters[i] = mockParameter;
				}
		    }

		    return sutParameters;
	    }
	}
}