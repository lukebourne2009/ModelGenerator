﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.ClassModels;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
	public interface IConstructorDesignModel : INamed, ISignatured
	{
		IList<IMethodArgumentDesignModel> Inputs { get; }
		IMethodImplementationDesignModel Implementation { get; set; }
	}
}