﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Settings;

namespace ModelGenerator.Core.Interfaces.DesignModels
{
    public interface IPropertyDesignModel : IVariableDesignModel
    {
        IList<IAttributeDesignModel> Attributes { get; set; }
        IPropertyDesignSettings Settings { get; set; }
    }
}