﻿using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Models;

namespace ModelGenerator.Core.Interfaces.Validators
{
	public interface IOveridableValidator
	{
		void AssertIsCompatibleOverride(IEnumerable<ISignatured> overides, ISignatured overide);
	}
}