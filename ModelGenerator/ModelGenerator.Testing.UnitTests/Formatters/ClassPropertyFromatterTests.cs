﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.Formatters;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain.ClassModels;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.ObjectModels;
using ModelGenerator.Testing.BehaviourTests;
using NUnit.Framework;
using TesterHelper.Core;
using Attribute = ModelGenerator.Domain.Models.Attribute;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
    [TestFixture]
    public class ClassPropertyFromatterTests : BaseTest<ClassPropertyFormatter>
    {
        static IEnumerable BuildProperties_GivenAPropertyFormatItCorrectly_Data()
        {
            yield return new TestCaseData(new List<IProperty>
            {
                new Property("Prop", new ClassModel("Type"))
            }, new List<IProperty>()).Returns("public Type Prop { get; set; }\r\n");
            yield return new TestCaseData(new List<IProperty>
            {
                new Property("Prop", new ClassModel("Type")),
                new Property("Prop2", new ClassModel("Type"))
            }, new List<IProperty>()).Returns("public Type Prop { get; set; }\r\npublic Type Prop2 { get; set; }\r\n");
            yield return new TestCaseData(new List<IProperty>
            {
                new Property("Prop", new ClassModel("Type"))
            }, new List<IProperty>
            {
                new Property("Prop2", new ClassModel("Type"))
            }).Returns("public Type Prop { get; set; }\r\npublic Type Prop2 { get; set; }\r\n");

            var generic = new Generic("TType");
	        var classModel = new ClassModel("Type")
	        {
		        Generics = new List<IGeneric> {generic}
	        };
	        yield return new TestCaseData(new List<IProperty>
                    {
                        new Property("Prop", classModel)
                        {
	                        DeclaredType = new DeclaredType(classModel)
	                        {
		                        GenericArguments = new Dictionary<IGeneric, IDeclaredType>
		                        {
			                        {generic, new DeclaredType(new ClassModel("int"))}
		                        }
	                        }
                        }
                    },
                    new List<IProperty>())
                .Returns("public Type<int> Prop { get; set; }\r\n");
        }

        [Test]
        [TestCaseSource("BuildProperties_GivenAPropertyFormatItCorrectly_Data")]
        public string BuildProperties_GivenAPropertyFormatItCorrectly(
            IEnumerable<IProperty> properties, IEnumerable<IProperty> implementationProperties)
        {
			// Arrange
	        var sut = Create<ClassPropertyFormatter>();

            // Act
            var result = sut.BuildProperties(properties, implementationProperties);

            // Assert
            return result;
        }
        [Test]
        public void BuildProperties_GivenAnAttribute_ThenAttributeIsFormattedCorrectly()
        {
			// Arrange
	        var sut = Create<ClassPropertyFormatter>();

			// Act
			string result = sut.BuildProperties(new List<IProperty>
            {
                new Property("Prop", new ClassModel("Type"))
                {
                    Attributes = new List<IAttribute>
                    {
                        new Attribute("Name")
                    }
                }
            }, new List<IProperty>());

            // Assert
            Assert.AreEqual("[Name]\r\npublic Type Prop { get; set; }\r\n", result);
        }
    }
}