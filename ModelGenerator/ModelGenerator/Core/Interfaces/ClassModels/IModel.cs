﻿namespace ModelGenerator.Core.Interfaces.ClassModels
{
	public interface IModel
	{
		string Name { get; }
		IObjectModel ViewModel { get; set; }
		IObjectModel Model { get; set; }
		IObjectModel Entity { get; set; }
	}
}