public class MyOtherModelModelMapper : IModelMapper<MyOtherModelEntity, MyOtherModelModel, MyOtherModelViewModel>
{
	public MyOtherModelEntity MapToEntity(MyOtherModelModel source)
	{
		return new MyOtherModelEntity()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyOtherModelEntity MapToEntity(MyOtherModelViewModel source)
	{
		var myOtherModelModel = MapToModel(source);
		var myOtherModelEntity = MapToEntity(myOtherModelModel);
		return myOtherModelEntity;
	}
	public MyOtherModelModel MapToModel(MyOtherModelEntity source)
	{
		return new MyOtherModelModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyOtherModelModel MapToModel(MyOtherModelViewModel source)
	{
		return new MyOtherModelModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	public MyOtherModelViewModel MapToViewModel(MyOtherModelEntity source)
	{
		var myOtherModelModel = MapToModel(source);
		var myOtherModelViewModel = MapToViewModel(myOtherModelModel);
		return myOtherModelViewModel;
	}
	public MyOtherModelViewModel MapToViewModel(MyOtherModelModel source)
	{
		return new MyOtherModelViewModel()
		{
			PropertyEntity = source.PropertyEntity
		};
	}
	
}
