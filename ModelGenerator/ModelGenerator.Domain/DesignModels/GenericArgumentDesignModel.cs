﻿using ModelGenerator.Core.Interfaces.DesignModels;

namespace ModelGenerator.Domain.DesignModels
{
    public class GenericArgumentDesignModel : DeclaredDesignModel, IGenericArgumentDesignModel
    {
        public GenericArgumentDesignModel(string type)
			: base(type)
        {}
    }
}