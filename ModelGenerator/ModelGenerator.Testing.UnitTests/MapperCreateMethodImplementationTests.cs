﻿using ModelGenerator.Common.Mappers;
using ModelGenerator.Core.Interfaces;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models.Method;
using ModelGenerator.Domain.DesignModels.MethodImplementations;
using NSubstitute;
using NUnit.Framework;

namespace ModelGenerator.Testing.UnitTests
{
    [TestFixture]
    public class MapperCreateMethodImplementationTests
    {
        [Test]
        public void TEst()
        {
            // Arrange
            IMethodImplementationDesignModel sut = new MapperCreateMethodImplementationDesignModel("Model");

            // Act
            var result = sut.Implementation;

            // Assert
            Assert.AreEqual("return new Model();", result);
        }
    }
}