﻿using System;
using System.Collections;
using System.Collections.Generic;
using ModelGenerator.Core.Interfaces.DesignModels;
using ModelGenerator.Core.Interfaces.Models;
using ModelGenerator.Domain;
using ModelGenerator.Domain.DesignModels;
using ModelGenerator.Domain.Formatter;
using ModelGenerator.Domain.Models;
using ModelGenerator.Domain.Models.Method;
using ModelGenerator.Domain.Models.Method.Implementations;
using ModelGenerator.Domain.Models.ObjectModels;
using NUnit.Framework;
using TesterHelper.Core;

namespace ModelGenerator.Testing.UnitTests.Formatters
{
	[TestFixture]
	public class ConstructorFormatterTests : BaseTest<ConstructorFormatter>
	{
	    [Test]
        [TestCaseSource("BuildConstructors_GivenSomeConstructors_FormatCorrectly_TestData")]
	    public string BuildConstructors_GivenSomeConstructors_FormatCorrectly(List<IConstructor> constructors)
	    {
	        // Arrange
		    var sut = Create<ConstructorFormatter>();

	        // Act
	        var result = sut.BuildConstructors(constructors);

	        // Assert
	        return result;
	    }

	    private static IEnumerable BuildConstructors_GivenSomeConstructors_FormatCorrectly_TestData()
	    {
	        var constructors = new List<IConstructor>();

            yield return new TestCaseData(constructors).Returns("");

		    constructors = new List<IConstructor> {new Constructor("Model")};
			var scope = new Scope(new DeclaredType(new ClassModel("Type")), constructors[0]);
			constructors[0].Implementation = new NotImplementedImplementation();

			yield return new TestCaseData(constructors).Returns("public Model()\r\n{throw new NotImplementedException();" +
			                                                    "}\r\n\r\n");

		    constructors = new List<IConstructor> {new Constructor("Model")};
		    scope = new Scope(new DeclaredType(new ClassModel("Type")), constructors[0]);
			constructors[0].Implementation = new NotImplementedImplementation();
			constructors[0].Inputs.Add(new MethodArgument(new ClassModel("Type"), "arg"));


			yield return new TestCaseData(constructors).Returns("public Model(Type arg)\r\n" +
																"{throw new NotImplementedException();" +
	                                                            "}\r\n\r\n");

		    constructors = new List<IConstructor> { new Constructor("Model") };
			scope = new Scope(new DeclaredType(new ClassModel("Type")), constructors[0]);
			constructors[0].Implementation = new NotImplementedImplementation();
			constructors[0].Inputs.Add(new MethodArgument(new ClassModel("Type"), "arg"));
	        constructors[0].Inputs.Add(new MethodArgument(new ClassModel("Type"), "arg2"));

			yield return new TestCaseData(constructors).Returns("public Model(Type arg, Type arg2)\r\n" +
																"{throw new NotImplementedException();" +
	                                                            "}\r\n\r\n");
	    }
    }

}